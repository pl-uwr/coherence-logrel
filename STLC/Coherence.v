Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Types.
Require Import STLC.Source.Typing STLC.CoercionSemantics.
Require Import STLC.CoercionSemanticsFacts.
Require Import STLC.Source.TypingInduction.
Require Import STLC.Target.Syntax.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.Compatibility.
Require Import STLC.LogicalRelation.Coercion.
Require Import STLC.LogicalRelation.Soundness.
Require Import STLC.Target.ContextualApproximation.
Require STLC.Source.Syntax.

Local Hint Resolve trans_sub_preserves_types.

Lemma coherence_lemma {V : Set} {Γ₁ Γ₂ : env V} 
      {e : Source.Syntax.exp V} {τ₁ τ₂ : typ} :
  ∀ (D₁ : Γ₁ ⊢ e ∷ τ₁) (D₂ : Γ₂ ⊢ e ∷ τ₂),
    ⊨ rel_e_open Γ₁ Γ₂ T⟦ D₁ ⟧ T⟦ D₂ ⟧ τ₁ τ₂.
Proof.
intro D₁; generalize Γ₂ τ₂; clear Γ₂ τ₂.
induction D₁; intros D₂ Γ₂ τ₂ n; simpl.
+ apply typing_var_ind with
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - apply compat_var.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_const_ind with
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - apply compat_const.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_abs_ind with 
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - intros; apply compat_abs; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_app_ind with 
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - intros; eapply compat_app; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_fix_ind with 
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - intros; apply compat_fix; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_succ_ind with 
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - intros; apply compat_succ; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_case_ind with 
    (P := λ _ τ D, n ⊨ rel_e_open _ _ _ T⟦ D ⟧ _ τ); simpl.
  - intros; apply compat_case; [ apply IHD₁1 | apply IHD₁2 | apply IHD₁3 ].
  - intros; eapply rel_coercion_r; eauto.
+ eapply rel_coercion_l; eauto; apply IHD₁.
Qed.

Theorem coherence {V : Set} (Γ : env V) e τ :
  ∀ (D₁ D₂ : Γ ⊢ e ∷ τ), ctx_approx Γ T⟦ D₁ ⟧ T⟦ D₂ ⟧ τ.
Proof.
intros D₁ D₂; apply rel_soundness; apply coherence_lemma.
Qed.