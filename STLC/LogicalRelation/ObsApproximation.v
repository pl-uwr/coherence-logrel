Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Target.Syntax STLC.Target.Reduction.
Require Import STLC.Target.ContextualApproximation.

Definition obs_func (e₁ e₂ : exp0) (n : nat) : Prop :=
  stops_n e₁ n → stops e₂.
Lemma obs_func_monotone : ∀ e₁ e₂, monotone (obs_func e₁ e₂).
Proof.
unfold obs_func; intros e₁ e₂ n H Hs.
apply H; clear H; induction Hs.
+ constructor 1; auto.
+ econstructor 2; eassumption.
+ econstructor 3; eassumption.
Qed.

Definition I_obs (e₁ e₂ : exp0) : IProp :=
  mk_IProp (obs_func e₁ e₂) (obs_func_monotone e₁ e₂).

Notation "A ~ᵢ B" := (I_obs A B) (at level 70).

Lemma obs_beta_l (e₁ e₁' e₂ : exp0) (n : nat) :
  beta e₁ e₁' → (n ⊨ ▷(e₁' ~ᵢ e₂)) → (n ⊨ e₁ ~ᵢ e₂).
Proof.
intros Hβ H.
apply I_valid_intro; intro Hs.
destruct n.
+ inversion Hs.
  - value_normal.
  - exfalso; eapply beta_or_iota; eassumption.
+ apply I_valid_elim, I_valid_elim in H; apply H.
  inversion Hs.
  - value_normal.
  - erewrite (beta_unique e₁ e₁'); eassumption.
  - exfalso; eapply beta_or_iota; eassumption.
Qed.

Lemma obs_beta_r (e₁ e₂ e₂' : exp0) (n : nat) :
  beta e₂ e₂' → (n ⊨ e₁ ~ᵢ e₂') → (n ⊨ e₁ ~ᵢ e₂).
Proof.
intros Hβ H.
apply I_valid_intro; intro Hs.
econstructor 2; [ eassumption | ].
apply I_valid_elim in H; apply H; assumption.
Qed.

Lemma obs_iota_l (e₁ e₁' e₂ : exp0) (n : nat) :
  iota e₁ e₁' → (n ⊨ e₁' ~ᵢ e₂) → (n ⊨ e₁ ~ᵢ e₂).
Proof.
intros Hι H.
apply I_valid_intro; intro Hs.
apply I_valid_elim in H; apply H; inversion Hs.
+ value_normal.
+ exfalso; eapply beta_or_iota; eassumption.
+ erewrite (iota_unique e₁ e₁'); eassumption.
Qed.

Lemma obs_iota_r (e₁ e₂ e₂' : exp0) (n : nat) :
  iota e₂ e₂' → (n ⊨ e₁ ~ᵢ e₂') → (n ⊨ e₁ ~ᵢ e₂).
Proof.
intros Hι H.
apply I_valid_intro; intro Hs.
econstructor 3; [ eassumption | ].
apply I_valid_elim in H; apply H; assumption.
Qed.

Lemma stops_limit e : stops e → ∃ n, stops_n e n.
Proof.
induction 1.
+ exists 0; constructor 1; assumption.
+ destruct IHstops as [n ?]; exists (S n).
  econstructor 2; eassumption.
+ destruct IHstops as [n ?]; exists n.
  econstructor 3; eassumption.
Qed.

Lemma obs_limit (e₁ e₂ : exp0) : (⊨ e₁ ~ᵢ e₂) → e₁ ~ e₂.
Proof.
intros H s.
destruct (stops_limit e₁) as [ n Hs ]; [ assumption | ].
specialize (H n); apply I_valid_elim in H; apply H; assumption.
Qed.