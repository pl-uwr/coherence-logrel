Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Types.
Require Import STLC.Target.Syntax.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.ObsApproximation.

Lemma rel_v_arrow_rule {f₁ f₂ v₁ v₂ : exp0} {σ₁ σ₂ τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_v (σ₁ ↦ τ₁) (σ₂ ↦ τ₂) f₁ f₂) →
  (n ⊨ rel_v σ₁ σ₂ v₁ v₂) →
  ∀ E₁ E₂, (n ⊨ rel_k τ₁ τ₂ E₁ E₂) →
  (n ⊨ cplug E₁ (exp_app f₁ v₁) ~ᵢ cplug E₂ (exp_app f₂ v₂)).
Proof.
intros Hf Hv E₁ E₂ HE.
simpl in Hf; apply I_conj_elim2 in Hf.
ispecialize Hf v₁; ispecialize Hf v₂; ispecialize Hf; [ assumption | ].
unfold grel_e in Hf.
ispecialize Hf E₁; ispecialize Hf E₂; iapply Hf; assumption.
Qed.

Lemma rel_v_arrow_rule_e {f₁ f₂ v₁ v₂ : exp0} {σ₁ σ₂ τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_v (σ₁ ↦ τ₁) (σ₂ ↦ τ₂) f₁ f₂) →
  (n ⊨ rel_v σ₁ σ₂ v₁ v₂) →
  (n ⊨ rel_e τ₁ τ₂ (exp_app f₁ v₁) (exp_app f₂ v₂)).
Proof.
intros Hf Hv.
simpl in Hf; apply I_conj_elim2 in Hf.
ispecialize Hf v₁; ispecialize Hf v₂; ispecialize Hf; assumption.
Qed.

Lemma rel_k_rule {E₁ E₂ : cctx} {v₁ v₂ : exp0} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_k τ₁ τ₂ E₁ E₂) →
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) →
  (n ⊨ cplug E₁ v₁ ~ᵢ cplug E₂ v₂).
Proof.
intros HE Hv.
unfold rel_k in HE; unfold grel_k in HE.
apply I_conj_elim2 in HE.
ispecialize HE v₁; ispecialize HE v₂; iapply HE; assumption.
Qed.

Lemma rel_e_rule {e₁ e₂ : exp0} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_e τ₁ τ₂ e₁ e₂) →
  ∀ E₁ E₂, (n ⊨ rel_k τ₁ τ₂ E₁ E₂) →
  (n ⊨ cplug E₁ e₁ ~ᵢ cplug E₂ e₂).
Proof.
intros He E₁ E₂ HE.
unfold rel_e in He; unfold grel_e in He.
ispecialize He E₁; ispecialize He E₂; ispecialize He; assumption.
Qed.

Lemma rel_g_rule {V : Set} {Γ₁ Γ₂ : env V} {γ₁ γ₂ : V → exp0} {n : nat} :
  (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) → ∀ x : V, (n ⊨ rel_v (Γ₁ x) (Γ₂ x) (γ₁ x) (γ₂ x)).
Proof.
intros Hγ x; unfold rel_g in Hγ; iapply Hγ.
Qed.

Lemma rel_e_rule_open {V : Set} 
    {Γ₁ Γ₂ : env V} {e₁ e₂ : exp V} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ τ₁ τ₂) →
  ∀ γ₁ γ₂, (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) →
  ∀ E₁ E₂, (n ⊨ rel_k τ₁ τ₂ E₁ E₂) →
  (n ⊨ cplug E₁ (bind γ₁ e₁) ~ᵢ cplug E₂ (bind γ₂ e₂)).
Proof.
intros He γ₁ γ₂ Hγ E₁ E₂ HE.
unfold rel_e_open in He.
ispecialize He γ₁; ispecialize He γ₂; ispecialize He; [ assumption | ].
unfold rel_e in He; unfold grel_e in He.
ispecialize He E₁; ispecialize He E₂; ispecialize He; assumption.
Qed.

Lemma rel_e_open_rule {V : Set}
    {Γ₁ Γ₂ : env V} {e₁ e₂ : exp V} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ τ₁ τ₂) →
  ∀ γ₁ γ₂, (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) →
  (n ⊨ rel_e τ₁ τ₂ (bind γ₁ e₁) (bind γ₂ e₂)).
Proof.
intros He γ₁ γ₂ Hγ.
unfold rel_e_open in He.
ispecialize He γ₁; ispecialize He γ₂; ispecialize He; assumption.
Qed.