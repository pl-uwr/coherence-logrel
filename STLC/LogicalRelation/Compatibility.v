Require Import Utf8.
Require Import IxFree.Lib.
Require Import Common.Metatheory STLC.Types.
Require Import STLC.Target.Syntax STLC.Target.SyntaxMeta STLC.Target.Reduction.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.ObsApproximation.
Require Import STLC.LogicalRelation.StructuralLemmas.
Require Import STLC.LogicalRelation.DerivedRules.

Lemma compat_var {V : Set} (Γ₁ Γ₂ : env V) (x : V) :
  ⊨ rel_e_open Γ₁ Γ₂ (exp_var x) (exp_var x) (Γ₁ x) (Γ₂ x).
Proof.
intro n; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e; simpl.
apply rel_g_rule; assumption.
Qed.

Lemma compat_const {V : Set} (Γ₁ Γ₂ : env V) (m : nat) :
  ⊨ rel_e_open Γ₁ Γ₂ (exp_const m) (exp_const m) ℕ ℕ.
Proof.
intro n; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e; simpl; iintro.
constructor.
Qed.

Lemma compat_unit {V : Set} (Γ₁ Γ₂ : env V) :
  ⊨ rel_e_open Γ₁ Γ₂ exp_unit exp_unit ⊤ ⊤.
Proof.
intro n; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e; simpl; iintro.
repeat constructor.
Qed.

Lemma compat_abs {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc V))
  (σ₁ σ₂ τ₁ τ₂ : typ)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / σ₁) (Γ₂ / σ₂) e₁ e₂ τ₁ τ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_abs e₁) (exp_abs e₂) (σ₁ ↦ τ₁) (σ₂ ↦ τ₂)).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e; simpl.
isplit; [ iintro; split; constructor | ].
iintro v₁; iintro v₂; iintro Hv.
unfold grel_e; iintro E₁; iintro E₂; iintro HE.
destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ].
destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
eapply obs_beta_r.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
iintro; repeat rewrite subst_bind_lift.
eapply rel_e_rule_open; try eassumption.
unfold rel_g; iintro x; destruct x as [ | x ]; simpl.
  assumption.
  iapply Hγ.
Qed.

Lemma compat_app {V : Set} (Γ₁ Γ₂ : env V)
  (f₁ f₂ e₁ e₂ : exp V)
  (σ₁ σ₂ τ₁ τ₂ : typ)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ f₁ f₂ (σ₁ ↦ τ₁) (σ₂ ↦ τ₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ σ₁ σ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_app f₁ e₁) (exp_app f₂ e₂) τ₁ τ₂).
Proof.
intros Hf He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
unfold rel_e; unfold grel_e.
iintro E₁; iintro E₂; iintro HE; simpl.
destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
change (n ⊨ cplug (cctx_app1 E₁ (bind γ₁ e₁)) (bind γ₁ f₁)
         ~ᵢ cplug (cctx_app1 E₂ (bind γ₂ e₂)) (bind γ₂ f₂)).
apply (rel_e_rule_open Hf _ _ Hγ); clear Hf.
unfold grel_k; isplit; [ iintro; split; constructor; assumption | ].
iintro v₁; iintro v₂; iintro Hv.
destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ].
change (n ⊨ cplug (cctx_app2 v₁ E₁) (bind γ₁ e₁)
         ~ᵢ cplug (cctx_app2 v₂ E₂) (bind γ₂ e₂)).
apply (rel_e_rule_open He _ _ Hγ); clear He.
unfold grel_k; isplit; [ iintro; split; constructor; assumption | ].
iintro a₁; iintro a₂; iintro Ha; simpl.
apply (rel_v_arrow_rule Hv); assumption.
Qed.

Lemma compat_fix {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc (inc V)))
  (σ₁ σ₂ τ₁ τ₂ : typ)
  (n : nat) :
  (n ⊨ rel_e_open 
    (Γ₁ / σ₁ ↦ τ₁ / σ₁)
    (Γ₂ / σ₂ ↦ τ₂ / σ₂)
    e₁ e₂ τ₁ τ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_fix e₁) (exp_fix e₂) (σ₁ ↦ τ₁) (σ₂ ↦ τ₂)).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e.
loeb_induction; simpl.
isplit; [ iintro; split; constructor | ].
iintro v₁; iintro v₂; iintro Hv.
unfold grel_e; iintro E₁; iintro E₂; iintro HE.
destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ].
destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
eapply obs_beta_r.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
repeat rewrite subst_bind_lift2.
later_shift.
apply (rel_e_rule_open He); try assumption.
unfold rel_g; iintro x.
destruct x as [ | x ]; simpl; try assumption.
destruct x as [ | x ]; simpl; try assumption.
iapply Hγ.
Qed.

Lemma compat_succ {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ ℕ ℕ) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_succ e₁) (exp_succ e₂) ℕ ℕ).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
unfold rel_e; unfold grel_e.
iintro E₁; iintro E₂; iintro HE.
destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
change (n ⊨ cplug (cctx_succ E₁) (bind γ₁ e₁)
         ~ᵢ cplug (cctx_succ E₂) (bind γ₂ e₂)).
apply (rel_e_rule_open He); try assumption.
unfold rel_k; unfold grel_k.
isplit; [ iintro; split; constructor; assumption | ].
iintro v₁; iintro v₂; iintro Hv; simpl in Hv.
apply I_Prop_elim in Hv; inversion Hv; simpl.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ assumption | ].
  econstructor; trivial.
iintro.
apply (rel_k_rule HE); simpl.
iintro; constructor.
Qed.

Lemma compat_case {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ z₁ z₂ : exp V)
  (s₁ s₂ : exp (inc V))
  (τ₁ τ₂ : typ)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ ℕ ℕ) →
  (n ⊨ rel_e_open Γ₁ Γ₂ z₁ z₂ τ₁ τ₂) →
  (n ⊨ rel_e_open (Γ₁ / ℕ) (Γ₂ / ℕ) s₁ s₂ τ₁ τ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_case e₁ z₁ s₁) (exp_case e₂ z₂ s₂) τ₁ τ₂).
Proof.
intros He Hz Hs; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
unfold rel_e; unfold grel_e.
iintro E₁; iintro E₂; iintro HE.
destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
change (n ⊨ cplug (cctx_case E₁ (bind γ₁ z₁) (bind (lift γ₁) s₁)) (bind γ₁ e₁)
       ~ᵢ cplug (cctx_case E₂ (bind γ₂ z₂) (bind (lift γ₂) s₂)) (bind γ₂ e₂)).
apply (rel_e_rule_open He); try assumption.
unfold rel_k; unfold grel_k.
isplit; [ iintro; split; constructor; assumption | ].
iintro v₁; iintro v₂; iintro Hv.
apply I_Prop_elim in Hv; inversion_clear Hv as [ m ]; simpl.
destruct m.
+ eapply obs_beta_r.
    apply beta_step_in_ectx; [ assumption | constructor ].
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ assumption | constructor ].
  iintro.
  apply (rel_e_rule_open Hz); assumption.
+ eapply obs_beta_r.
    apply beta_step_in_ectx; [ assumption | constructor; trivial ].
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ assumption | constructor; trivial ].
  iintro; repeat rewrite subst_bind_lift.
  apply (rel_e_rule_open Hs); try assumption.
  unfold rel_g; iintro x; destruct x as [ | x ]; simpl.
  iintro; constructor.
  iapply Hγ.
Qed.

(* ========================================================================= *)
(* Closed variants *)

Lemma compat_const_cl (m : nat) :
  ⊨ rel_e ℕ ℕ (exp_const m) (exp_const m).
Proof.
intro n; apply rel_e_open_cl; apply compat_const.
Qed.

Lemma compat_unit_cl :
  ⊨ rel_e ⊤ ⊤ exp_unit exp_unit.
Proof.
intro n; apply rel_e_open_cl; apply compat_unit.
Qed.

Lemma compat_app_cl
  (f₁ f₂ e₁ e₂ : exp0)
  (σ₁ σ₂ τ₁ τ₂ : typ)
  (n : nat) :
  (n ⊨ rel_e (σ₁ ↦ τ₁) (σ₂ ↦ τ₂) f₁ f₂) →
  (n ⊨ rel_e σ₁ σ₂ e₁ e₂) →
  (n ⊨ rel_e τ₁ τ₂ (exp_app f₁ e₁) (exp_app f₂ e₂)).
Proof.
intros Hf He; apply rel_e_open_cl; eapply compat_app.
apply rel_e_open_cl; apply Hf.
apply rel_e_open_cl; apply He.
Qed.

Lemma compat_succ_cl
  (e₁ e₂ : exp0)
  (n : nat) :
  (n ⊨ rel_e ℕ ℕ e₁ e₂) →
  (n ⊨ rel_e ℕ ℕ (exp_succ e₁) (exp_succ e₂)).
Proof.
intro He; apply rel_e_open_cl; apply compat_succ.
apply rel_e_open_cl; assumption.
Qed.