Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Types.
Require Import STLC.Target.Syntax STLC.Target.SyntaxMeta.
Require Import STLC.Target.Typing STLC.Target.Reduction.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.StructuralLemmas.
Require Import STLC.LogicalRelation.DerivedRules.
Require Import STLC.LogicalRelation.ObsApproximation.
Require Import STLC.LogicalRelation.Compatibility.

Lemma rel_coercion_l_cl (c : crc) (σ τ : typ) : c ⊩ σ ≺: τ → 
  ∀ v₁ v₂ τ₀ n,
  (n ⊨ rel_e σ τ₀ v₁ v₂) → (n ⊨ rel_e τ τ₀ (exp_capp c v₁) v₂).
Proof.
induction 1; intros e₁ e₂ τ₀ n He;
  unfold rel_e; unfold grel_e;
  iintro E₁; iintro E₂; iintro HE;
  destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ];
  match goal with [ |- ?N ⊨ cplug _ (exp_capp ?C _) ~ᵢ _ ] =>
    change (n ⊨ cplug (cctx_capp C E₁) e₁ ~ᵢ cplug E₂ e₂)
  end; 
  apply (rel_e_rule He); unfold rel_k; unfold grel_k;
  isplit; try (iintro; split; try constructor; assumption; fail);
  iintro v₁; iintro v₂; iintro Hv;
  destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ]; simpl.
+ eapply obs_iota_l.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  apply (rel_k_rule HE); assumption.
+ eapply obs_iota_l.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  eapply rel_e_rule; [ | eassumption ].
  apply IHsub1; apply IHsub2.
  apply rel_v_in_rel_e; assumption.
+ eapply obs_iota_l.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  apply (rel_k_rule HE); simpl.
  iintro; split; [ constructor | assumption ].
+ apply (rel_k_rule HE); destruct τ₀; simpl.
  - apply Hv.
  - iintro; split; [ constructor | ]; assumption.
  - destruct (rel_v_relates_values Hv) as [ Hf₁ Hf₂ ].
    clear E₁ E₂ HE HE₁ HE₂; isplit.
      iintro; split; [ constructor | ]; assumption.
    iintro a₁; iintro a₂; iintro Ha.
    unfold grel_e; iintro E₁; iintro E₂; iintro HE.
    destruct (rel_v_relates_values Ha) as [ Ha₁ Ha₂ ].
    destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
    eapply obs_iota_l.
      apply iota_step_in_ectx; [ assumption | ].
      econstructor; trivial.
    eapply rel_e_rule; [ | eassumption ].
    apply IHsub2.
    eapply compat_app_cl.
      apply rel_v_in_rel_e; eassumption.
    apply IHsub1.
    apply rel_v_in_rel_e; assumption.
Qed.

Lemma rel_coercion_l (c : crc) (σ₁ τ₁ : typ) : c ⊩ σ₁ ≺: τ₁ → 
  ∀ (V : Set) (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (τ₂ : typ) (n : nat),
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ σ₁ τ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_capp c e₁) e₂ τ₁ τ₂).
Proof.
intros Hsub V Γ₁ Γ₂ e₁ e₂ τ₀ n He.
unfold rel_e_open; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply rel_coercion_l_cl; [ eassumption | ].
eapply rel_e_open_rule; eassumption.
Qed.

Lemma rel_coercion_r_cl (c : crc) (σ τ : typ) : c ⊩ σ ≺: τ → 
  ∀ v₁ v₂ τ₀ n,
  (n ⊨ rel_e τ₀ σ v₁ v₂) → (n ⊨ rel_e τ₀ τ v₁ (exp_capp c v₂)).
Proof.
induction 1; intros e₁ e₂ τ₀ n He;
  unfold rel_e; unfold grel_e;
  iintro E₁; iintro E₂; iintro HE;
  destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ];
  match goal with [ |- ?N ⊨ _ ~ᵢ cplug _ (exp_capp ?C _) ] =>
    change (n ⊨ cplug E₁ e₁ ~ᵢ cplug (cctx_capp C E₂) e₂)
  end;
  apply (rel_e_rule He); unfold rel_k; unfold grel_k;
  isplit; try (iintro; split; try constructor; assumption; fail);
  iintro v₁; iintro v₂; iintro Hv;
  destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ]; simpl.
+ eapply obs_iota_r.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  apply (rel_k_rule HE); assumption.
+ eapply obs_iota_r.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  eapply rel_e_rule; [ | eassumption ].
  apply IHsub1; apply IHsub2.
  apply rel_v_in_rel_e; assumption.
+ eapply obs_iota_r.
    apply iota_step_in_ectx; [ assumption | ].
    econstructor; trivial.
  apply (rel_k_rule HE); destruct τ₀; simpl.
  - iintro; split; [ assumption | constructor ].
  - iintro; split; [ assumption | constructor ].
  - iintro; split; [ assumption | constructor ].
+ apply (rel_k_rule HE); destruct τ₀; simpl.
  - apply Hv.
  - iintro; split; [ | constructor ]; assumption.
  - destruct (rel_v_relates_values Hv) as [ Hf₁ Hf₂ ].
    clear E₁ E₂ HE HE₁ HE₂; isplit.
      iintro; split; [ | constructor ]; assumption.
    iintro a₁; iintro a₂; iintro Ha.
    unfold grel_e; iintro E₁; iintro E₂; iintro HE.
    destruct (rel_v_relates_values Ha) as [ Ha₁ Ha₂ ].
    destruct (rel_k_relates_ectx HE) as [ HE₁ HE₂ ].
    eapply obs_iota_r.
      apply iota_step_in_ectx; [ assumption | ].
      econstructor; trivial.
    eapply rel_e_rule; [ | eassumption ].
    apply IHsub2.
    eapply compat_app_cl.
      apply rel_v_in_rel_e; eassumption.
    apply IHsub1.
    apply rel_v_in_rel_e; assumption.
Qed.

Lemma rel_coercion_r (c : crc) (σ₂ τ₂ : typ) : c ⊩ σ₂ ≺: τ₂ → 
  ∀ (V : Set) (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (τ₁ : typ) (n : nat),
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ τ₁ σ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ (exp_capp c e₂) τ₁ τ₂).
Proof.
intros Hsub V Γ₁ Γ₂ e₁ e₂ τ₁ n He.
unfold rel_e_open; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply rel_coercion_r_cl; [ eassumption | ].
eapply rel_e_open_rule; eassumption.
Qed.