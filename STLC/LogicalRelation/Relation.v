Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Types.
Require Import STLC.Target.Syntax STLC.LogicalRelation.ObsApproximation.

Inductive rel_nat : exp0 → exp0 → Prop :=
| RelNat : ∀ m, rel_nat (exp_const m) (exp_const m)
.

Definition grel_k (rel : exp0 → exp0 → IProp) (E₁ E₂ : cctx) : IProp :=
  (ectx E₁ ∧ ectx E₂)ᵢ ∧ᵢ ∀ᵢ v₁ v₂, rel v₁ v₂ ⇒ cplug E₁ v₁ ~ᵢ cplug E₂ v₂.

Definition grel_e (rel : exp0 → exp0 → IProp) (e₁ e₂ : exp0) : IProp :=
  ∀ᵢ E₁ E₂, grel_k rel E₁ E₂ ⇒ cplug E₁ e₁ ~ᵢ cplug E₂ e₂.

Fixpoint rel_v (τ₁ τ₂ : typ) (v₁ v₂ : exp0) : IProp :=
  match τ₁, τ₂ with
  | ℕ,       ℕ       => (rel_nat v₁ v₂)ᵢ
  | ⊤,       _       => (value v₁ ∧ value v₂)ᵢ
  | _,       ⊤       => (value v₁ ∧ value v₂)ᵢ
  | σ₁ ↦ τ₁, σ₂ ↦ τ₂ => (value v₁ ∧ value v₂)ᵢ ∧ᵢ
      ∀ᵢ a₁ a₂, rel_v σ₁ σ₂ a₁ a₂ ⇒ 
        grel_e (rel_v τ₁ τ₂) (exp_app v₁ a₁) (exp_app v₂ a₂)
  | ℕ,       _ ↦ _   => (False)ᵢ
  | _ ↦ _ ,  ℕ       => (False)ᵢ
  end.

Definition rel_k τ₁ τ₂ := grel_k (rel_v τ₁ τ₂).
Definition rel_e τ₁ τ₂ := grel_e (rel_v τ₁ τ₂).

Definition rel_g {V : Set} (Γ₁ Γ₂ : env V) (γ₁ γ₂ : V → exp0) : IProp :=
  ∀ᵢ x : V, rel_v (Γ₁ x) (Γ₂ x) (γ₁ x) (γ₂ x).

Definition rel_e_open {V : Set} 
    (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (τ₁ τ₂ : typ) : IProp :=
  ∀ᵢ γ₁ γ₂, rel_g Γ₁ Γ₂ γ₁ γ₂ ⇒ rel_e τ₁ τ₂ (bind γ₁ e₁) (bind γ₂ e₂).