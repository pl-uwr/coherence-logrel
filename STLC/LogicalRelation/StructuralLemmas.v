Require Import Utf8.
Require Import IxFree.Lib.
Require Import STLC.Types Common.Metatheory.
Require Import STLC.Target.Syntax STLC.Target.SyntaxMeta.
Require Import STLC.Target.SyntaxValues.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.ObsApproximation.

Lemma rel_v_in_rel_e {τ₁ τ₂ : typ} {v₁ v₂ : exp0} (n : nat) :
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) → (n ⊨ rel_e τ₁ τ₂ v₁ v₂).
Proof.
intro Hv.
unfold rel_e; unfold grel_e.
iintro E₁; iintro E₂; iintro HE.
unfold grel_k in HE; apply I_conj_elim2 in HE.
ispecialize HE v₁; simpl in HE; ispecialize HE v₂; simpl in HE.
iapply HE; assumption.
Qed.

Lemma rel_v_relates_values {τ₁ τ₂ : typ} {v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) → value v₁ ∧ value v₂.
Proof.
intro H; destruct τ₁; simpl in H.
+ destruct τ₂; simpl in H.
  - apply I_Prop_elim in H; inversion H; repeat constructor.
  - apply I_Prop_elim in H; assumption.
  - apply I_Prop_elim in H; destruct H.
+ apply I_Prop_elim in H; assumption.
+ destruct τ₂; simpl in H.
  - apply I_Prop_elim in H; destruct H.
  - apply I_Prop_elim in H; assumption.
  - apply I_conj_elim1 in H.
    apply I_Prop_elim in H; destruct H.
    split; assumption.
Qed.

Lemma rel_k_relates_ectx {τ₁ τ₂ : typ} {E₁ E₂ : cctx} {n : nat} :
  (n ⊨ rel_k τ₁ τ₂ E₁ E₂) → ectx E₁ ∧ ectx E₂.
Proof.
unfold rel_k; unfold grel_k; intro H.
apply I_conj_elim1 in H.
apply I_Prop_elim in H; assumption.
Qed.

Lemma rel_e_open_cl {e₁ e₂ : exp0} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_e_open of_empty of_empty e₁ e₂ τ₁ τ₂)
  <-> (n ⊨ rel_e τ₁ τ₂ e₁ e₂).
Proof.
split.
+ intro H; unfold rel_e_open in H.
  ispecialize H (@of_empty exp0); ispecialize H (@of_empty exp0).
  rewrite monad_bind_return in H; [ | intro x; destruct x ].
  rewrite monad_bind_return in H; [ | intro x; destruct x ].
  iapply H.
  unfold rel_g; iintro x; destruct x.
+ intro H; unfold rel_e_open.
  iintro γ₁; iintro γ₂; iintro Hγ.
  rewrite monad_bind_return; [ | intro x; destruct x ].
  rewrite monad_bind_return; [ | intro x; destruct x ].
  assumption.
Qed.