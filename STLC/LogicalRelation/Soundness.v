Require Import Utf8.
Require Import IxFree.Lib.
Require Import Common.Metatheory STLC.Types.
Require Import STLC.Target.Syntax STLC.Target.Typing.
Require Import STLC.Target.ContextualApproximation.
Require Import STLC.LogicalRelation.ObsApproximation.
Require Import STLC.LogicalRelation.Relation.
Require Import STLC.LogicalRelation.StructuralLemmas.
Require Import STLC.LogicalRelation.Compatibility.
Require Import STLC.LogicalRelation.Coercion.
Require Import STLC.LogicalRelation.DerivedRules.

Theorem rel_fundamental_property {V : Set} (Γ : env V) (e : exp V) τ :
  Γ ⊩ e ∷ τ → (⊨ rel_e_open Γ Γ e e τ τ).
Proof.
induction 1; intro n; simpl.
+ apply compat_var.
+ apply compat_const.
+ apply compat_unit.
+ apply compat_abs; auto.
+ eapply compat_app; [ apply IHtyping1 | apply IHtyping2 ].
+ eapply rel_coercion_l; [ | eapply rel_coercion_r ]; eauto.
+ apply compat_fix; auto.
+ apply compat_succ; auto.
+ apply compat_case;
  [ apply IHtyping1 | apply IHtyping2 | apply IHtyping3 ].
Qed.

Lemma rel_context_cl {V : Set} (C : ctx V) Γ τ ρ :
  C ∷[ Γ ⊢ τ ]↝ ρ →
  ∀ e₁ e₂ n, (n ⊨ rel_e_open Γ Γ e₁ e₂ τ τ) → 
    (n ⊨ rel_e ρ ρ (plug C e₁) (plug C e₂)).
Proof.
induction 1; intros f₁ f₂ n Hf; simpl.
+ apply rel_e_open_cl; assumption.
+ apply IHctx_typing; apply compat_abs; assumption.
+ apply IHctx_typing; eapply compat_app;
  try apply rel_fundamental_property; eassumption.
+ apply IHctx_typing; eapply compat_app;
  try apply rel_fundamental_property; eassumption.
+ apply IHctx_typing.
  eapply rel_coercion_l; [ eassumption | ].
  eapply rel_coercion_r; eassumption.
+ apply IHctx_typing; apply compat_fix; assumption.
+ apply IHctx_typing; apply compat_succ; assumption.
+ apply IHctx_typing; eapply compat_case;
  try apply rel_fundamental_property; eassumption.
+ apply IHctx_typing; eapply compat_case;
  try apply rel_fundamental_property; eassumption.
+ apply IHctx_typing; eapply compat_case;
  try apply rel_fundamental_property; eassumption.
Qed.

Lemma rel_e_is_obs (e₁ e₂ : exp0) τ :
  ⊨ rel_e τ τ e₁ e₂ ⇒ e₁ ~ᵢ e₂.
Proof.
intro n; iintro H.
change (n ⊨ cplug cctx_hole e₁ ~ᵢ cplug cctx_hole e₂).
eapply rel_e_rule; [ eassumption | ].
unfold rel_k; unfold grel_k.
isplit; [ iintro; split; constructor | ].
iintro v₁; iintro v₂; iintro Hv; simpl.
destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ].
apply I_valid_intro; intro; constructor 1; assumption.
Qed.

Theorem rel_soundness {V : Set} (Γ : env V) e₁ e₂ τ :
  (⊨ rel_e_open Γ Γ e₁ e₂ τ τ) → ctx_approx Γ e₁ e₂ τ.
Proof.
intros H C ρ HC.
apply obs_limit; intro n.
iapply (rel_e_is_obs (plug C e₁) (plug C e₂) ρ n).
eapply rel_context_cl; eauto.
Qed.