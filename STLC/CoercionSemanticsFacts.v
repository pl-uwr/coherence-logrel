Require Import Utf8.
Require Import STLC.Types.
Require Import STLC.Source.Typing STLC.CoercionSemantics.
Require Import STLC.Target.Syntax STLC.Target.Typing.

Lemma trans_sub_preserves_types {σ τ : typ} (D : σ ≺: τ) : sub S⟦ D ⟧ σ τ.
Proof.
induction D; simpl; econstructor; eassumption.
Qed.