Require Import Utf8.
Require Import STLC.Types.
Require Import STLC.Source.Typing.
Require Import STLC.Target.Syntax.
Require STLC.Source.Syntax.

Reserved Notation "S⟦ D ⟧".
Reserved Notation "T⟦ D ⟧".

Fixpoint trans_sub {σ τ : typ} (D : σ ≺: τ) : crc :=
  match D with
  | S_Refl        => crc_id
  | S_Trans D₁ D₂ => crc_comp S⟦ D₁ ⟧ S⟦ D₂ ⟧
  | S_Top         => crc_top
  | S_Arrow D₁ D₂ => crc_arrow S⟦ D₁ ⟧ S⟦ D₂ ⟧
  end
where "S⟦ D ⟧" := (@trans_sub _ _ D).

Fixpoint trans_typing {V : Set} {Γ : env V} {e : Source.Syntax.exp V} {τ : typ}
    (D : Γ ⊢ e ∷ τ) : exp V :=
  match D with
  | T_Var   x        => exp_var x
  | T_Const m        => exp_const m
  | T_Abs   D        => exp_abs T⟦ D ⟧
  | T_App   D₁ D₂    => exp_app T⟦ D₁ ⟧ T⟦ D₂ ⟧
  | T_Fix   D        => exp_fix T⟦ D ⟧
  | T_Succ  D        => exp_succ T⟦ D ⟧
  | T_Case  D₁ D₂ D₃ => exp_case T⟦ D₁ ⟧ T⟦ D₂ ⟧ T⟦ D₃ ⟧
  | T_Sub   D₁ D₂    => exp_capp S⟦ D₂ ⟧ T⟦ D₁ ⟧
  end
where "T⟦ D ⟧" := (@trans_typing _ _ _ _ D).