Require Import Utf8.
Require Import STLC.Types STLC.Target.Syntax.

Reserved Notation "c ⊩ A ≺: B" (at level 45).

Inductive sub : crc → typ → typ → Prop :=
| sub_refl  : ∀ τ, crc_id ⊩ τ ≺: τ
| sub_trans : ∀ τ₁ τ₂ τ₃ c₁ c₂,
    c₁ ⊩ τ₂ ≺: τ₃ →
    c₂ ⊩ τ₁ ≺: τ₂ →
    crc_comp c₁ c₂ ⊩ τ₁ ≺: τ₃
| sub_top   : ∀ τ,
    crc_top ⊩ τ ≺: ⊤
| sub_arrow : ∀ σ₁ σ₂ τ₁ τ₂ c₁ c₂,
    c₁ ⊩ σ₂ ≺: σ₁ → c₂ ⊩ τ₁ ≺: τ₂ →
    crc_arrow c₁ c₂ ⊩ (σ₁ ↦ τ₁) ≺: (σ₂ ↦ τ₂)
where "c ⊩ A ≺: B" := (sub c A B).

Reserved Notation "G ⊩ e ∷ T" (at level 45).

Inductive typing {V : Set} (Γ : env V) : exp V → typ → Prop :=
| T_Var   : ∀ x, Γ ⊩ exp_var x ∷ Γ x
| T_Const : ∀ m, Γ ⊩ exp_const m ∷ ℕ
| T_Unit  : Γ ⊩ exp_unit ∷ ⊤
| T_Abs   : ∀ e σ τ,
    Γ / σ ⊩ e ∷ τ →
    Γ ⊩ exp_abs e ∷ σ ↦ τ
| T_App   : ∀ e₁ e₂ σ τ,
    Γ ⊩ e₁ ∷ σ ↦ τ →
    Γ ⊩ e₂ ∷ σ →
    Γ ⊩ exp_app e₁ e₂ ∷ τ
| T_CApp  : ∀ c e σ τ,
    c ⊩ σ ≺: τ →
    Γ ⊩ e ∷ σ →
    Γ ⊩ exp_capp c e ∷ τ
| T_Fix   : ∀ e σ τ,
    Γ / σ ↦ τ / σ ⊩ e ∷ τ →
    Γ ⊩ exp_fix e ∷ σ ↦ τ
| T_Succ  : ∀ e,
    Γ ⊩ e ∷ ℕ → Γ ⊩ exp_succ e ∷ ℕ
| T_Case  : ∀ e₁ e₂ e₃ τ,
    Γ ⊩ e₁ ∷ ℕ →
    Γ ⊩ e₂ ∷ τ →
    Γ / ℕ ⊩ e₃ ∷ τ →
    Γ ⊩ exp_case e₁ e₂ e₃ ∷ τ
where "G ⊩ e ∷ T" := (@typing _ G e T).

Reserved Notation "C ∷[ G ⊢ A ]↝ B" (at level 45).

Inductive ctx_typing : ∀ V : Set, ctx V → env V → typ → typ → Prop :=
| CT_Hole : ∀ τ, ctx_hole ∷[ empty_env ⊢ τ ]↝ τ
| CT_Abs  : ∀ (V : Set) (C : ctx V) Γ σ τ ρ,
  C ∷[ Γ ⊢ σ ↦ τ]↝ ρ →
  ctx_abs C ∷[ Γ / σ ⊢ τ ]↝ ρ
| CT_App1 : ∀ (V : Set) (C : ctx V) Γ e σ τ ρ,
  C ∷[ Γ ⊢ τ ]↝ ρ →
  Γ ⊩ e ∷ σ →
  ctx_app1 C e ∷[ Γ ⊢ σ ↦ τ ]↝ ρ
| CT_App2 : ∀ (V : Set) (C : ctx V) Γ e σ τ ρ,
  Γ ⊩ e ∷ σ ↦ τ →
  C ∷[ Γ ⊢ τ ]↝ ρ →
  ctx_app2 e C ∷[ Γ ⊢ σ ]↝ ρ
| CT_CApp : ∀ (V : Set) (C : ctx V) Γ c σ τ ρ,
  c ⊩ σ ≺: τ →
  C ∷[ Γ ⊢ τ ]↝ ρ →
  ctx_capp c C ∷[ Γ ⊢ σ ]↝ ρ
| CT_Fix : ∀ (V : Set) (C : ctx V) Γ σ τ ρ,
  C ∷[ Γ ⊢ σ ↦ τ ]↝ ρ →
  ctx_fix C ∷[ Γ / σ ↦ τ / σ ⊢ τ ]↝ ρ
| CT_Succ : ∀ (V : Set) (C : ctx V) Γ ρ,
  C ∷[ Γ ⊢ ℕ ]↝ ρ →
  ctx_succ C ∷[ Γ ⊢ ℕ ]↝ ρ
| CT_Case : ∀ (V : Set) (C : ctx V) Γ e₁ e₂ τ ρ,
  C ∷[ Γ ⊢ τ ]↝ ρ →
  Γ ⊩ e₁ ∷ τ →
  Γ / ℕ ⊩ e₂ ∷ τ →
  ctx_case C e₁ e₂ ∷[ Γ ⊢ ℕ ]↝ ρ
| CT_Case1 : ∀ (V : Set) (C : ctx V) Γ e e₂ τ ρ,
  Γ ⊩ e ∷ ℕ →
  C ∷[ Γ ⊢ τ ]↝ ρ →
  Γ / ℕ ⊩ e₂ ∷ τ →
  ctx_case1 e C e₂ ∷[ Γ ⊢ τ ]↝ ρ
| CT_Case2 : ∀ (V : Set) (C : ctx V) Γ e e₁ τ ρ,
  Γ ⊩ e ∷ ℕ →
  Γ ⊩ e₁ ∷ τ →
  C ∷[ Γ ⊢ τ ]↝ ρ →
  ctx_case2 e e₁ C ∷[ Γ / ℕ ⊢ τ ]↝ ρ
where "C ∷[ G ⊢ A ]↝ B" := (ctx_typing _ C G A B).