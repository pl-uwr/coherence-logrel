Require Import Utf8.
Require Import Common.Metatheory.

Inductive crc : Set :=
| crc_id    : crc
| crc_comp  : crc → crc → crc
| crc_top   : crc
| crc_arrow : crc → crc → crc
.

Inductive exp (V : Set) : Set :=
| exp_var   : V → exp V
| exp_const : nat → exp V
| exp_unit  : exp V
| exp_abs   : exp (inc V) → exp V
| exp_app   : exp V → exp V → exp V
| exp_capp  : crc → exp V → exp V
| exp_fix   : exp (inc (inc V)) → exp V
| exp_succ  : exp V → exp V
| exp_case  : exp V → exp V → exp (inc V) → exp V
.

Arguments exp_var   [V] _.
Arguments exp_const [V] _.
Arguments exp_unit  [V].
Arguments exp_abs   [V] _.
Arguments exp_app   [V] _ _.
Arguments exp_capp  [V] _ _.
Arguments exp_fix   [V] _.
Arguments exp_succ  [V] _.
Arguments exp_case  [V] _ _ _.

Definition exp0 : Set := exp empty.
Definition exp1 : Set := exp (inc empty).
Definition exp2 : Set := exp (inc (inc empty)).

Inductive ctx : Set → Type :=
| ctx_hole  : ctx empty
| ctx_abs   : ∀ V, ctx V → ctx (inc V)
| ctx_app1  : ∀ V, ctx V → exp V → ctx V
| ctx_app2  : ∀ V, exp V → ctx V → ctx V
| ctx_capp  : ∀ V, crc → ctx V → ctx V
| ctx_fix   : ∀ V, ctx V → ctx (inc (inc V))
| ctx_succ  : ∀ V, ctx V → ctx V
| ctx_case  : ∀ V, ctx V → exp V → exp (inc V) → ctx V
| ctx_case1 : ∀ V, exp V → ctx V → exp (inc V) → ctx V
| ctx_case2 : ∀ V, exp V → exp V → ctx V → ctx (inc V)
.

Arguments ctx_abs   [V] _.
Arguments ctx_app1  [V] _ _.
Arguments ctx_app2  [V] _ _.
Arguments ctx_capp  [V] _ _.
Arguments ctx_fix   [V] _.
Arguments ctx_succ  [V] _.
Arguments ctx_case  [V] _ _ _.
Arguments ctx_case1 [V] _ _ _.
Arguments ctx_case2 [V] _ _ _.

Inductive cctx : Set :=
| cctx_hole : cctx
| cctx_app1 : cctx → exp0 → cctx
| cctx_app2 : exp0 → cctx → cctx
| cctx_capp : crc  → cctx → cctx
| cctx_succ : cctx → cctx
| cctx_case : cctx → exp0 → exp1 → cctx
.

Inductive fvalue : exp0 → Prop :=
| FVal_abs        : ∀ e, fvalue (exp_abs e)
| FVal_capp_arrow : ∀ c₁ c₂ f,
    fvalue f → fvalue (exp_capp (crc_arrow c₁ c₂) f)
| FVal_fix        : ∀ e, fvalue (exp_fix e)
.

Inductive value : exp0 → Prop :=
| Val_const      : ∀ m, value (exp_const m)
| Val_unit       : value exp_unit
| Val_abs        : ∀ e, value (exp_abs e)
| Val_capp_arrow : ∀ c₁ c₂ v,
    value v → value (exp_capp (crc_arrow c₁ c₂) v)
| Val_fix        : ∀ e, value (exp_fix e)
.

Inductive ectx : cctx → Prop :=
| ECtx_hole  : ectx cctx_hole
| ECtx_app1  : ∀ E e, ectx E → ectx (cctx_app1 E e)
| ECtx_app2  : ∀ v E, value v → ectx E → ectx (cctx_app2 v E)
| ECtx_capp  : ∀ c E, ectx E → ectx (cctx_capp c E)
| ECtx_succ  : ∀ E, ectx E → ectx (cctx_succ E)
| ECtx_case  : ∀ E e₁ e₂, ectx E → ectx (cctx_case E e₁ e₂)
.

Fixpoint map {V W : Set} (f : V → W) (e : exp V) : exp W :=
  match e with
  | exp_var  x       => exp_var (f x)
  | exp_const m      => exp_const m
  | exp_unit         => exp_unit
  | exp_abs  e       => exp_abs (map (inc_map f) e)
  | exp_app  e₁ e₂   => exp_app (map f e₁) (map f e₂)
  | exp_capp c  e    => exp_capp c (map f e)
  | exp_fix  e       => exp_fix (map (inc_map (inc_map f)) e)
  | exp_succ e       => exp_succ (map f e)
  | exp_case e e₁ e₂ => exp_case (map f e) (map f e₁) (map (inc_map f) e₂)
  end.

Definition lift {V W : Set} (f : V → exp W) (x : inc V) : exp (inc W) :=
  match x with
  | VZ   => exp_var VZ
  | VS y => map (@VS _) (f y)
  end.

Fixpoint bind {V W : Set} (f : V → exp W) (e : exp V) : exp W :=
  match e with
  | exp_var  x       => f x
  | exp_const m      => exp_const m
  | exp_unit         => exp_unit
  | exp_abs  e       => exp_abs (bind (lift f) e)
  | exp_app  e₁ e₂   => exp_app (bind f e₁) (bind f e₂)
  | exp_capp c  e    => exp_capp c (bind f e)
  | exp_fix  e       => exp_fix (bind (lift (lift f)) e)
  | exp_succ e       => exp_succ (bind f e)
  | exp_case e e₁ e₂ => exp_case (bind f e) (bind f e₁) (bind (lift f) e₂)
  end.

Definition subst_func {V : Set} (e : exp V) (x : inc V) : exp V :=
  match x with
  | VZ   => e
  | VS y => exp_var y
  end.

Definition subst {V : Set} (e₁ : exp (inc V)) (e₂ : exp V) : exp V :=
  bind (subst_func e₂) e₁.

Fixpoint plug {V : Set} (C : ctx V) : exp V → exp0 :=
  match C in ctx V return exp V → exp0 with
  | ctx_hole            => λ e, e
  | ctx_abs   C       => λ e, plug C (exp_abs e)
  | ctx_app1  C e₂    => λ e, plug C (exp_app e e₂)
  | ctx_app2  e₁ C    => λ e, plug C (exp_app e₁ e)
  | ctx_capp  c  C    => λ e, plug C (exp_capp c e)
  | ctx_fix   C       => λ e, plug C (exp_fix e)
  | ctx_succ  C       => λ e, plug C (exp_succ e)
  | ctx_case  C e₁ e₂ => λ e, plug C (exp_case e e₁ e₂)
  | ctx_case1 e₀ C e₂ => λ e, plug C (exp_case e₀ e e₂)
  | ctx_case2 e₀ e₁ C => λ e, plug C (exp_case e₀ e₁ e)
  end.

Fixpoint cplug (C : cctx) (e : exp0) : exp0 :=
  match C with
  | cctx_hole         => e
  | cctx_app1 C e₂    => cplug C (exp_app e e₂)
  | cctx_app2 e₁ C    => cplug C (exp_app e₁ e)
  | cctx_capp c  C    => cplug C (exp_capp c e)
  | cctx_succ C       => cplug C (exp_succ e)
  | cctx_case C e₁ e₂ => cplug C (exp_case e e₁ e₂)
  end.
