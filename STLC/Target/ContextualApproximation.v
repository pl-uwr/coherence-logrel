Require Import Utf8.
Require Import STLC.Types.
Require Import STLC.Target.Syntax STLC.Target.Typing.
Require Import STLC.Target.Reduction.

Definition obs (e₁ e₂ : exp0) : Prop :=
  stops e₁ → stops e₂.
Notation "A ~ B" := (obs A B) (at level 70).

Definition ctx_approx {V : Set} 
    (Γ : env V) (e₁ e₂ : exp V) (τ : typ) :=
  ∀ (C : ctx V) ρ, C ∷[ Γ ⊢ τ ]↝ ρ → plug C e₁ ~ plug C e₂.