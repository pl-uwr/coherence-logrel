Require Import Utf8.
Require Import STLC.Target.Syntax.

Lemma fvalue_is_value (v : exp0) : fvalue v → value v.
Proof.
induction 1; constructor; assumption.
Qed.
