Require Import Utf8.
Require Import Common.Metatheory STLC.Target.Syntax.

Lemma monad_map_id {V : Set} (e : exp V) :
  ∀ (f : V → V), (∀ x, f x = x) → map f e = e.
Proof.
induction e; intros; simpl.
+ congruence.
+ reflexivity.
+ reflexivity.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; congruence.
+ erewrite IHe1; [ erewrite IHe2; [ reflexivity | ] | ]; assumption.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  destruct i; simpl; congruence.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe1; [ erewrite IHe2 ; [ erewrite IHe3 ; [ reflexivity | ] | ] | ];
  try assumption.
  destruct x; simpl; congruence.
Qed.

Lemma monad_map_map {V : Set} (e : exp V) :
  ∀ (V₁ V₂ : Set) (f₁ : V → V₁) (f₂ : V₁ → V₂) (g : V → V₂),
  (∀ x, f₂ (f₁ x) = g x) →
  map f₂ (map f₁ e) = map g e.
Proof.
induction e; intros; simpl.
+ congruence.
+ reflexivity.
+ reflexivity.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; congruence.
+ erewrite IHe1; [ erewrite IHe2; [ reflexivity | ] | ]; assumption.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  destruct i; simpl; congruence.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe1; [ erewrite IHe2 ; [ erewrite IHe3 ; [ reflexivity | ] | ] | ];
  try assumption.
  destruct x; simpl; congruence.
Qed.

Lemma monad_map_map' {V₁ V₂ V₃ : Set} (e : exp V₁) (f₁ : V₁ → V₂) (f₂ : V₂ → V₃) :
  map f₂ (map f₁ e) = map (λ x, f₂ (f₁ x)) e.
Proof.
eapply monad_map_map; reflexivity.
Qed.

Lemma monad_bind_return {V : Set} (e : exp V) :
  ∀ (f : V → exp V), (∀ x, f x = exp_var x) → bind f e = e.
Proof.
induction e; intros; simpl.
+ congruence.
+ reflexivity.
+ reflexivity.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ | rewrite H ]; reflexivity.
+ erewrite IHe1; [ erewrite IHe2; [ reflexivity | ] | ]; assumption.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  destruct i; simpl; [ reflexivity | ].
  rewrite H; reflexivity.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe1; [ erewrite IHe2 ; [ erewrite IHe3 ; [ reflexivity | ] | ] | ];
  try assumption.
  destruct x; simpl; [ | rewrite H ]; reflexivity.
Qed.

Lemma monad_bind_map {V : Set} (e : exp V) :
  ∀ (W₁ W₂ X : Set)
  (f₁ : V → W₁) (f₂ : W₁ → exp X)
  (g₁ : V → exp W₂) (g₂ : W₂ → X),
  (∀ x, f₂ (f₁ x) = map g₂ (g₁ x)) →
  bind f₂ (map f₁ e) = map g₂ (bind g₁ e).
Proof.
induction e; intros; simpl.
+ auto.
+ reflexivity.
+ reflexivity.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  rewrite H; rewrite monad_map_map'; rewrite monad_map_map'; reflexivity.
+ erewrite IHe1; [ erewrite IHe2; [ reflexivity | ] | ]; assumption.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  destruct i; simpl; [ reflexivity | ].
  rewrite H; repeat rewrite monad_map_map'; reflexivity.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe1; [ erewrite IHe2; [ erewrite IHe3; [ reflexivity | ] | ] | ];
  try assumption.
  destruct x; simpl; [ reflexivity | ].
  rewrite H; rewrite monad_map_map'; rewrite monad_map_map'; reflexivity.
Qed.

Lemma monad_bind_bind {V : Set} (e : exp V) :
  ∀ (V₁ V₂ : Set) (f₁ : V → exp V₁) (f₂ : V₁ → exp V₂) (g : V → exp V₂),
  (∀ x, bind f₂ (f₁ x) = g x) →
  bind f₂ (bind f₁ e) = bind g e.
Proof.
induction e; intros; simpl.
+ auto.
+ reflexivity.
+ reflexivity.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  rewrite <- H; erewrite monad_bind_map; reflexivity.
+ erewrite IHe1; [ erewrite IHe2; [ reflexivity | ] | ]; assumption.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe; [ reflexivity | ].
  destruct x; simpl; [ reflexivity | ].
  destruct i; simpl; [ reflexivity | ].
  rewrite <- H; repeat rewrite monad_map_map'.
  erewrite monad_bind_map; [ reflexivity | ].
  intro; apply monad_map_map; reflexivity.
+ erewrite IHe; [ reflexivity | ]; assumption.
+ erewrite IHe1; [ erewrite IHe2; [ erewrite IHe3; [ reflexivity | ] | ] | ];
  try assumption.
  destruct x; simpl; [ reflexivity | ].
  rewrite <- H; erewrite monad_bind_map; reflexivity.
Qed.

Definition subst_extend {V W : Set} 
    (f : V → exp W) (e : exp W) (x : inc V) : exp W :=
  match x with
  | VZ   => e
  | VS y => f y
  end.

Definition subst_extend2 {V W : Set}
    (f : V → exp W) (e₁ e₂ : exp W) (x : inc (inc V)) : exp W :=
  match x with
  | VZ        => e₁
  | VS VZ     => e₂
  | VS (VS y) => f y
  end.

Lemma subst_bind_lift {V W : Set} 
    (f : V → exp W) (e : exp (inc V)) (v : exp W) :
  subst (bind (lift f) e) v = bind (subst_extend f v) e.
Proof.
apply monad_bind_bind.
destruct x; simpl; [ reflexivity | ].
rewrite monad_bind_map with (g₁ := @exp_var _) (g₂ := λ x, x); 
  [ | reflexivity ].
rewrite monad_map_id; [ | reflexivity ].
apply monad_bind_return; reflexivity.
Qed.

Lemma subst_bind_lift2 {V W : Set}
    (f : V → exp W) (e : exp (inc (inc V))) (v₁ v₂ : exp W) :
  subst (subst (bind (lift (lift f)) e) (map (@VS _) v₁)) v₂ =
  bind (subst_extend2 f v₁ v₂) e.
Proof.
rewrite subst_bind_lift; apply monad_bind_bind.
destruct x; simpl.
+ erewrite monad_bind_map.
    rewrite monad_map_id; [ | reflexivity ].
    apply monad_bind_return; reflexivity.
  reflexivity.
+ destruct i; simpl.
    reflexivity.
  erewrite monad_bind_map.
    rewrite monad_map_id; [ | reflexivity ].
    apply monad_bind_return; reflexivity.
  reflexivity.
Qed.
