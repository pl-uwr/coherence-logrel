Require Import Utf8.
Require Import Common.Metatheory.

Inductive typ : Set :=
| typ_nat   : typ
| typ_top   : typ
| typ_arrow : typ → typ → typ
.

Notation "'ℕ'" := typ_nat.
Notation "'⊤'" := typ_top.
Notation "A ↦ B" := (typ_arrow A B) (at level 38, right associativity).

Definition env (V : Set) : Set := V → typ.

Definition empty_env : env empty := of_empty.

Definition extend {V : Set} (Γ : env V) (τ : typ) : env (inc V) :=
  fun x =>
  match x with
  | VZ   => τ
  | VS y => Γ y
  end.

Notation "G / T" := (@extend _ G T) (at level 40, left associativity).