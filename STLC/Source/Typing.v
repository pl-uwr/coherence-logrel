Require Import Utf8.
Require Import Common.Metatheory STLC.Types.
Require Import STLC.Source.Syntax.

Reserved Notation "A ≺: B" (at level 45).

Inductive sub : typ → typ → Set :=
| S_Refl  : ∀ τ, 
  τ ≺: τ
| S_Trans : ∀ τ₁ τ₂ τ₃,
  τ₂ ≺: τ₃ → τ₁ ≺: τ₂ → τ₁ ≺: τ₃
| S_Top   : ∀ τ, 
  τ ≺: ⊤
| S_Arrow : ∀ σ₁ σ₂ τ₁ τ₂,
  σ₂ ≺: σ₁ → τ₁ ≺: τ₂ → (σ₁ ↦ τ₁) ≺: (σ₂ ↦ τ₂)
where "A ≺: B" := (sub A B).

Arguments S_Refl  [τ].
Arguments S_Trans [τ₁] [τ₂] [τ₃] _ _.
Arguments S_Top   [τ].
Arguments S_Arrow [σ₁] [σ₂] [τ₁] [τ₂] _ _.

Reserved Notation "G ⊢ e ∷ T" (at level 45).

Inductive typing {V : Set} (Γ : env V) : exp V → typ → Set :=
| T_Var   : ∀ x, 
    Γ ⊢ exp_var x ∷ Γ x
| T_Const : ∀ m, 
    Γ ⊢ exp_const m ∷ ℕ
| T_Abs   : ∀ e σ τ,
    Γ / σ ⊢ e ∷ τ → Γ ⊢ exp_abs e ∷ σ ↦ τ
| T_App   : ∀ e₁ e₂ σ τ,
    Γ ⊢ e₁ ∷ σ ↦ τ →
    Γ ⊢ e₂ ∷ σ →
    Γ ⊢ exp_app e₁ e₂ ∷ τ
| T_Fix   : ∀ e σ τ,
    Γ / σ ↦ τ / σ ⊢ e ∷ τ →
    Γ ⊢ exp_fix e ∷ σ ↦ τ
| T_Succ  : ∀ e,
    Γ ⊢ e ∷ ℕ → Γ ⊢ exp_succ e ∷ ℕ
| T_Case  : ∀ e₁ e₂ e₃ τ,
    Γ ⊢ e₁ ∷ ℕ →
    Γ ⊢ e₂ ∷ τ →
    Γ / ℕ ⊢ e₃ ∷ τ →
    Γ ⊢ exp_case e₁ e₂ e₃ ∷ τ
| T_Sub   : ∀ e σ τ,
    Γ ⊢ e ∷ σ → σ ≺: τ → Γ ⊢ e ∷ τ
where "G ⊢ e ∷ T" := (@typing _ G e T).

Arguments T_Var   [V] [Γ] _.
Arguments T_Const [V] [Γ] _.
Arguments T_Abs   [V] [Γ] [e]  [σ]  [τ] _.
Arguments T_App   [V] [Γ] [e₁] [e₂] [σ] [τ] _ _.
Arguments T_Fix   [V] [Γ] [e]  [σ]  [τ] _.
Arguments T_Succ  [V] [Γ] [e] _.
Arguments T_Case  [V] [Γ] [e₁] [e₂] [e₃] [τ] _ _ _.
Arguments T_Sub   [V] [Γ] [e]  [σ]  [τ] _ _.