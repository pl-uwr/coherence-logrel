Require Import Utf8.
Require Import STLC.Types.
Require Import STLC.Source.Syntax STLC.Source.Typing.

Fixpoint sub_depth {V : Set} {Γ : env V} {e} {τ} (D : Γ ⊢ e ∷ τ) : nat :=
  match D with
  | T_Sub D _ => S (sub_depth D)
  | _ => 0
  end.

Lemma typing_var_ind {V : Set} {Γ : env V} x
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Hvar : P (exp_var x) (Γ x) (T_Var x))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_var x ∷ τ₁),
    P (exp_var x) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_var x) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_var x ∷ τ), P (exp_var x) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_var x ∷ τ), d = sub_depth D → P (exp_var x) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_const_ind {V : Set} {Γ : env V} m
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Hconst : P (exp_const m) ℕ (T_Const m))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_const m ∷ τ₁),
    P (exp_const m) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_const m) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_const m ∷ τ), P (exp_const m) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_const m ∷ τ), d = sub_depth D → P (exp_const m) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_abs_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Habs : ∀ τ₁ τ₂ (D₀ : Γ / τ₁ ⊢ e₀ ∷ τ₂),
    P (exp_abs e₀) (τ₁ ↦ τ₂) (T_Abs D₀))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_abs e₀ ∷ τ₁),
    P (exp_abs e₀) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_abs e₀) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_abs e₀ ∷ τ), P (exp_abs e₀) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_abs e₀ ∷ τ), d = sub_depth D → P (exp_abs e₀) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_app_ind {V : Set} {Γ : env V} e₁ e₂
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Happ : ∀ τ₁ τ₂ (D₁ : Γ ⊢ e₁ ∷ τ₂ ↦ τ₁) (D₂ : Γ ⊢ e₂ ∷ τ₂),
    P (exp_app e₁ e₂) τ₁ (T_App D₁ D₂))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_app e₁ e₂ ∷ τ₁),
    P (exp_app e₁ e₂) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_app e₁ e₂) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_app e₁ e₂ ∷ τ), P (exp_app e₁ e₂) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_app e₁ e₂ ∷ τ), 
    d = sub_depth D → P (exp_app e₁ e₂) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_fix_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Hfix : ∀ τ₁ τ₂ (D₀ : Γ / τ₁ ↦ τ₂ / τ₁ ⊢ e₀ ∷ τ₂),
    P (exp_fix e₀) (τ₁ ↦ τ₂) (T_Fix D₀))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_fix e₀ ∷ τ₁),
    P (exp_fix e₀) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_fix e₀) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_fix e₀ ∷ τ), P (exp_fix e₀) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_fix e₀ ∷ τ), d = sub_depth D → P (exp_fix e₀) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_succ_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Hsucc : ∀ (D₀ : Γ ⊢ e₀ ∷ ℕ),
    P (exp_succ e₀) ℕ (T_Succ D₀))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_succ e₀ ∷ τ₁),
    P (exp_succ e₀) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_succ e₀) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_succ e₀ ∷ τ), P (exp_succ e₀) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_succ e₀ ∷ τ), d = sub_depth D → P (exp_succ e₀) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_case_ind {V : Set} {Γ : env V} e₁ e₂ e₃
  (P : ∀ e τ, Γ ⊢ e ∷ τ → Prop)
  (Hcase : ∀ τ 
    (D₁ : Γ ⊢ e₁ ∷ ℕ)
    (D₂ : Γ ⊢ e₂ ∷ τ)
    (D₃ : Γ / ℕ ⊢ e₃ ∷ τ),
    P (exp_case e₁ e₂ e₃) τ (T_Case D₁ D₂ D₃))
  (Hsub : ∀ τ₁ τ₂ (D₁ : Γ ⊢ exp_case e₁ e₂ e₃ ∷ τ₁),
    P (exp_case e₁ e₂ e₃) τ₁ D₁ → ∀ D₂ : τ₁ ≺: τ₂,
    P (exp_case e₁ e₂ e₃) τ₂ (T_Sub D₁ D₂)) :
  ∀ τ (D : Γ ⊢ exp_case e₁ e₂ e₃ ∷ τ), P (exp_case e₁ e₂ e₃) τ D.
Proof.
assert (∀ d τ (D : Γ ⊢ exp_case e₁ e₂ e₃ ∷ τ), 
    d = sub_depth D → P (exp_case e₁ e₂ e₃) τ D).
  induction d; intros τ D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.
