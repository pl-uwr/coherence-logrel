# Logical Relations for Coherence of Effect Subtyping

This is the Coq formalization that accompanies the
"Logical Relations for Coherence of Effect Subtyping" paper.

The code has been tested with Coq version 8.6.1.
