Require Import Utf8.
Require Import Shift0.Types.
Require Import Shift0.Source.Syntax Shift0.Source.Typing.

Fixpoint sub_depth {V : Set} {Γ : env V} {e} {τ} (D : Γ ⊢ e ∷ τ) : nat :=
  match D with
  | T_Sub D _ => S (sub_depth D)
  | _ => 0
  end.

Lemma typing_var_ind {V : Set} {Γ : env V} x
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hvar : P (exp_var x) (Γ x)ε (T_Var x))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_var x ∷ T₁),
    P (exp_var x) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_var x) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_var x ∷ T), P (exp_var x) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_var x ∷ T), d = sub_depth D → P (exp_var x) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_const_ind {V : Set} {Γ : env V} m
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hconst : P (exp_const m) (ℕ)ε (T_Const m))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_const m ∷ T₁),
    P (exp_const m) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_const m) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_const m ∷ T), P (exp_const m) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_const m ∷ T), 
    d = sub_depth D → P (exp_const m) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_abs_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Habs : ∀ τ T (D₀ : Γ / τ ⊢ e₀ ∷ T),
    P (exp_abs e₀) (τ ↦ T)ε (T_Abs D₀))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_abs e₀ ∷ T₁),
    P (exp_abs e₀) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_abs e₀) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_abs e₀ ∷ T), P (exp_abs e₀) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_abs e₀ ∷ T), d = sub_depth D → P (exp_abs e₀) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_app_ind {V : Set} {Γ : env V} e₁ e₂
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hpapp : ∀ τ T (D₁ : Γ ⊢ e₁ ∷ (τ ↦ T)ε) (D₂ : Γ ⊢ e₂ ∷ (τ)ε),
    P (exp_app e₁ e₂) T (T_PApp D₁ D₂))
  (Happ : ∀ τ₁ τ₂ U₁ U₂ U₃ U₄ 
    (D₁ : Γ ⊢ e₁ ∷ (τ₂ ↦ τ₁ ![ U₄ ] U₃) ![ U₂ ] U₁)
    (D₂ : Γ ⊢ e₂ ∷ τ₂ ![ U₃ ] U₂),
    P (exp_app e₁ e₂) (τ₁ ![ U₄ ] U₁) (T_App D₁ D₂))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_app e₁ e₂ ∷ T₁),
    P (exp_app e₁ e₂) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_app e₁ e₂) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_app e₁ e₂ ∷ T), P (exp_app e₁ e₂) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_app e₁ e₂ ∷ T), 
    d = sub_depth D → P (exp_app e₁ e₂) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_shift0_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hshift0 : ∀ τ T U (D₀ : Γ / τ ↦ T ⊢ e₀ ∷ U),
    P (exp_shift0 e₀) (τ ![ T ] U) (T_Sft D₀))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_shift0 e₀ ∷ T₁),
    P (exp_shift0 e₀) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_shift0 e₀) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_shift0 e₀ ∷ T), P (exp_shift0 e₀) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_shift0 e₀ ∷ T), 
    d = sub_depth D → P (exp_shift0 e₀) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_reset0_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hreset0 : ∀ τ T (D₀ : Γ ⊢ e₀ ∷ τ ![ (τ)ε ] T),
    P (exp_reset0 e₀) T (T_Rst D₀))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_reset0 e₀ ∷ T₁),
    P (exp_reset0 e₀) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_reset0 e₀) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_reset0 e₀ ∷ T), P (exp_reset0 e₀) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_reset0 e₀ ∷ T), 
    d = sub_depth D → P (exp_reset0 e₀) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_dollar_ind {V : Set} {Γ : env V} e₁ e₂
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hpdollar : ∀ τ T U (D₁ : Γ ⊢ e₁ ∷ (τ ↦ T)ε) (D₂ : Γ ⊢ e₂ ∷ (τ ![ T ] U)),
    P (exp_dollar e₁ e₂) U (T_PDol D₁ D₂))
  (Hdollar : ∀ τ τ' T U₁ U₂ U₃
    (D₁ : Γ ⊢ e₁ ∷ (τ ↦ T) ![ U₂ ] U₁)
    (D₂ : Γ ⊢ e₂ ∷ τ ![ T ] τ' ![ U₃ ] U₂),
    P (exp_dollar e₁ e₂) (τ' ![ U₃ ] U₁) (T_Dol D₁ D₂))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_dollar e₁ e₂ ∷ T₁),
    P (exp_dollar e₁ e₂) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_dollar e₁ e₂) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_dollar e₁ e₂ ∷ T), P (exp_dollar e₁ e₂) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_dollar e₁ e₂ ∷ T), 
    d = sub_depth D → P (exp_dollar e₁ e₂) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_fix_ind {V : Set} {Γ : env V} e₀
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hfix : ∀ τ T (D₀ : Γ / τ ↦ T / τ ⊢ e₀ ∷ T),
    P (exp_fix e₀) (τ ↦ T)ε (T_Fix D₀))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_fix e₀ ∷ T₁),
    P (exp_fix e₀) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_fix e₀) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_fix e₀ ∷ T), P (exp_fix e₀) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_fix e₀ ∷ T), d = sub_depth D → P (exp_fix e₀) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_succ_ind {V : Set} {Γ : env V} e
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hpsucc : ∀ (D : Γ ⊢ e ∷ (ℕ)ε), P (exp_succ e) (ℕ)ε (T_PSucc D))
  (Hsucc : ∀ T U (D : Γ ⊢ e ∷ ℕ ![ T ] U),
    P (exp_succ e) (ℕ ![ T ] U) (T_Succ D))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_succ e ∷ T₁),
    P (exp_succ e) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_succ e) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_succ e ∷ T), P (exp_succ e) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_succ e ∷ T), d = sub_depth D → P (exp_succ e) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.

Lemma typing_case_ind {V : Set} {Γ : env V} e e₁ e₂
  (P : ∀ e T, Γ ⊢ e ∷ T → Prop)
  (Hpcase : ∀ T 
    (D  : Γ ⊢ e ∷ (ℕ)ε)
    (D₁ : Γ ⊢ e₁ ∷ T)
    (D₂ : Γ / ℕ ⊢ e₂ ∷ T), 
    P (exp_case e e₁ e₂) T (T_PCase D D₁ D₂))
  (Hcase : ∀ τ U₁ U₂ U₃
    (D  : Γ ⊢ e ∷ ℕ ![ U₂ ] U₁)
    (D₁ : Γ ⊢ e₁ ∷ τ ![ U₃ ] U₂)
    (D₂ : Γ / ℕ ⊢ e₂ ∷ τ ![ U₃ ] U₂),
    P (exp_case e e₁ e₂) (τ ![ U₃ ] U₁) (T_Case D D₁ D₂))
  (Hsub : ∀ T₁ T₂ (D₁ : Γ ⊢ exp_case e e₁ e₂ ∷ T₁),
    P (exp_case e e₁ e₂) T₁ D₁ → ∀ D₂ : T₁ ≺: T₂,
    P (exp_case e e₁ e₂) T₂ (T_Sub D₁ D₂)) :
  ∀ T (D : Γ ⊢ exp_case e e₁ e₂ ∷ T), P (exp_case e e₁ e₂) T D.
Proof.
assert (∀ d T (D : Γ ⊢ exp_case e e₁ e₂ ∷ T), 
    d = sub_depth D → P (exp_case e e₁ e₂) T D).
  induction d; intros T D; dependent inversion D; simpl; auto; congruence.
intros; eapply H; reflexivity.
Qed.
