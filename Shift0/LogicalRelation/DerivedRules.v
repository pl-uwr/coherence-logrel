Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.LogicalRelation.Relation.

Lemma rel_v_arrow_rule {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff} 
    {f₁ f₂ v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ T₂) f₁ f₂) →
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) →
  (n ⊨ rel_e T₁ T₂ (exp_app f₁ v₁) (exp_app f₂ v₂)).
Proof.
intros Hf Hv; simpl in Hf.
apply I_conj_elim2 in Hf; iespecialize Hf; iapply Hf; assumption.
Qed.

Lemma rel_e_rule {T₁ T₂ : typ_eff} {e₁ e₂ : exp0} {E₁ E₂ : cctx} {n : nat} :
  (n ⊨ rel_e T₁ T₂ e₁ e₂) → 
  (n ⊨ rel_k T₁ T₂ E₁ E₂) →
  (n ⊨ cplug E₁ e₁ ~ᵢ cplug E₂ e₂).
Proof.
intros He HE; unfold grel_e in He; iespecialize He; iapply He; eassumption.
Qed.

Lemma rel_k_pure_rule {τ₁ τ₂ : typ} {E₁ E₂ : cctx} {v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_k (τ₁)ε (τ₂)ε E₁ E₂) →
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) →
  (n ⊨ cplug E₁ v₁ ~ᵢ cplug E₂ v₂).
Proof.
intros HE Hv; simpl in HE.
apply I_conj_elim2 in HE; iespecialize HE; iapply HE; assumption.
Qed.

Lemma rel_ev_rule {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
    {E : cctx} {k v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_ev τ₁ T₁ τ₂ T₂ E k) →
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) →
  (n ⊨ rel_e T₁ T₂ (cplug E v₁) (exp_app k v₂)).
Proof.
intros HEk Hv; unfold grel_ev in HEk.
apply I_conj_elim2 in HEk; iespecialize HEk; iapply HEk; assumption.
Qed.

Lemma rel_ve_rule {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
    {k : exp0} {E : cctx} {v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_ve τ₁ T₁ τ₂ T₂ k E) →
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) →
  (n ⊨ rel_e T₁ T₂ (exp_app k v₁) (cplug E v₂)).
Proof.
intros HkE Hv; unfold grel_ve in HkE.
apply I_conj_elim2 in HkE; iespecialize HkE; iapply HkE; assumption.
Qed.

Lemma rel_g_rule {V : Set} {Γ₁ Γ₂ : env V} {γ₁ γ₂ : V → exp0} {n : nat} :
  (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) → ∀ x : V, (n ⊨ rel_v (Γ₁ x) (Γ₂ x) (γ₁ x) (γ₂ x)).
Proof.
intros Hγ x; unfold rel_g in Hγ; iapply Hγ.
Qed.

Lemma rel_e_open_rule {V : Set}
    {Γ₁ Γ₂ : env V} {e₁ e₂ : exp V} {T₁ T₂ : typ_eff} {n : nat} :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ T₁ T₂) →
  ∀ γ₁ γ₂, (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) →
  (n ⊨ rel_e T₁ T₂ (bind γ₁ e₁) (bind γ₂ e₂)).
Proof.
intros He γ₁ γ₂ Hγ.
unfold rel_e_open in He.
iespecialize He; iapply He; assumption.
Qed.

Lemma rel_v_open_rule {V : Set}
    {Γ₁ Γ₂ : env V} {v₁ v₂ : exp V} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_v_open Γ₁ Γ₂ v₁ v₂ τ₁ τ₂) →
  ∀ γ₁ γ₂, (n ⊨ rel_g Γ₁ Γ₂ γ₁ γ₂) →
  (n ⊨ rel_v τ₁ τ₂ (bind γ₁ v₁) (bind γ₂ v₂)).
Proof.
intros Hv γ₁ γ₂ Hγ.
unfold rel_v_open in Hv.
iespecialize Hv; iapply Hv; assumption.
Qed.
