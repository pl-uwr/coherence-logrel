Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.LogicalRelation.Relation.
Require Import Shift0.LogicalRelation.Auto.
Require Import Shift0.LogicalRelation.ECtxReduction.
Require Import Shift0.LogicalRelation.DerivedRules.
Require Import Shift0.LogicalRelation.Compatibility.

Lemma rel_k_coercion_id_l_aux (T₁ T₂ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (n ⊨ rel_k T₁ T₂ E₁ E₂) → (n ⊨ rel_k T₁ T₂ (cctx_capp crc_id E₁) E₂).
Proof.
intro HE.
apply @rel_k_ectx_iota_l with (E₁' := E₁).
  constructor; logrel_auto.
  intros v Hv; simpl.
  apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
assumption.
Qed.

Lemma rel_k_coercion_comp_l_aux (c₁ c₂ : crc)
  (T₁ T₂ T₃ T₀ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₃ T₀ E₁ E₂) → 
    (n ⊨ rel_k T₂ T₀ (cctx_capp c₁ E₁) E₂)) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₂ T₀ E₁ E₂) →
    (n ⊨ rel_k T₁ T₀ (cctx_capp c₂ E₁) E₂)) →
  (n ⊨ rel_k T₃ T₀ E₁ E₂) → 
    (n ⊨ rel_k T₁ T₀ (cctx_capp (crc_comp c₁ c₂) E₁) E₂).
Proof.
intros IHc₁ IHc₂ HE.
apply @rel_k_ectx_iota_l with (E₁' := cctx_capp c₂ (cctx_capp c₁ E₁)).
  constructor; logrel_auto.
  intros v Hv; simpl.
  apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
apply IHc₂; apply IHc₁; assumption.
Qed.

Lemma rel_e_IH_l {c : crc} {T₁ T₂ T₀ : typ_eff} {e₁ e₂ : exp0} {n : nat} :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₂ T₀ E₁ E₂) →
    (n ⊨ rel_k T₁ T₀ (cctx_capp c E₁) E₂)) →
  (n ⊨ rel_e T₁ T₀ e₁ e₂) →
    (n ⊨ rel_e T₂ T₀ (exp_capp c e₁) e₂).
Proof.
intros IHc He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug (cctx_capp c E₁) e₁ ~ᵢ cplug E₂ e₂).
apply (rel_e_rule He).
apply IHc; assumption.
Qed.

Lemma rel_coercion_arrow_l_val_pure (c₁ c₂ : crc)
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) τ₀ (v₁ v₂ : exp0) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k (τ₁)ε T₀ E₁ E₂) →
    (n ⊨ rel_k (τ₂)ε T₀ (cctx_capp c₁ E₁) E₂)) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₂ T₀ E₁ E₂) →
    (n ⊨ rel_k T₁ T₀ (cctx_capp c₂ E₁) E₂)) →
  (n ⊨ rel_v (τ₁ ↦ T₁) τ₀ v₁ v₂) →
    (n ⊨ rel_v (τ₂ ↦ T₂) τ₀ (exp_capp (crc_arrow c₁ c₂) v₁) v₂).
Proof.
intros IHc₁ IHc₂ Hv.
destruct τ₀ as [ | τ₀ T₀ ].
  apply Hv.
simpl; logrel_auto.
iintro a₁; iintro a₂; iintro Ha; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_iota_l.
  apply iota_step_in_ectx; logrel_auto.
  constructor; logrel_auto.
eapply rel_e_rule; [ | apply HE ].
apply (rel_e_IH_l IHc₂); eapply compat_app_cl.
  apply rel_v_in_rel_e; eassumption.
apply (rel_e_IH_l IHc₁); apply rel_v_in_rel_e; eassumption.
Qed.

Lemma rel_k_coercion_arrow_l_aux (c₁ c₂ : crc)
  (τ₁ τ₂ : typ) (T₁ T₂ T₀ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k (τ₁)ε T₀ E₁ E₂) →
    (n ⊨ rel_k (τ₂)ε T₀ (cctx_capp c₁ E₁) E₂)) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₂ T₀ E₁ E₂) →
    (n ⊨ rel_k T₁ T₀ (cctx_capp c₂ E₁) E₂)) →
  (n ⊨ rel_k (τ₂ ↦ T₂)ε T₀ E₁ E₂) →
    (n ⊨ rel_k (τ₁ ↦ T₁)ε T₀ (cctx_capp (crc_arrow c₁ c₂) E₁) E₂).
Proof.
intros IHc₁ IHc₂ HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  apply (rel_k_pure_rule HE).
  eapply rel_coercion_arrow_l_val_pure; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE.
  idestruct HE as T HE; iexists T.
  idestruct HE as E HE; iexists (cctx_capp (crc_arrow c₁ c₂) E).
  idestruct HE as k HE; iexists k.
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists E₂'; logrel_auto.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; destruct HE as [ HE ? ].
    eapply ectx_iota_rt_capp_arrow; eassumption.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE.
    unfold_rel_ev.
    iintro v₁; iintro v₂; iintro Hv; simpl.
    apply (rel_ev_rule HE).
    eapply rel_coercion_arrow_l_val_pure; eassumption.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; eassumption.
Qed.

Lemma rel_k_coercion_lift_l_aux (c : crc)
  (τ : typ) (T U T₀ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k U T₀ E₁ E₂) →
    (n ⊨ rel_k T T₀ (cctx_capp c E₁) E₂)) →
  (n ⊨ rel_k (τ ![ T ] U) T₀ E₁ E₂) →
    (n ⊨ rel_k (τ)ε T₀ (cctx_capp (crc_lift c) E₁) E₂).
Proof.
intros IHc HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  simpl in HE.
  idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T₀ HE; idestruct HE as k₀ HE; idestruct HE as E₀ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₀ [ Hk₀ [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'.
  apply (obs_ectx_iota_rt_l Hι₁); simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; trivial.
    constructor; logrel_auto.
  later_shift.
  apply rel_k_fix_unroll in HE'; apply rel_ve_fix_unroll in HkE.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  rewrite cplug_cctx_append.
  eapply rel_e_rule; [ | eassumption ].
  apply (rel_e_IH_l IHc).
  apply (rel_ve_rule HkE); assumption.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  iexists U.
  idestruct HE as k₁ HE;
    iexists (cctx_capp (crc_lift c) (cctx_app1 cctx_hole k₁)).
  idestruct HE as k₂ HE; iexists k₂.
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists E₂'.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ ↦ T) (τ₀ ↦ T₀) k₁ k₂) in Hk.
  logrel_auto.
  - apply (ectx_iota_rt_capp_lift Hι₁).
  - assumption.
  - unfold_rel_ev.
    iintro v₁; iintro v₂; iintro Hv; simpl.
    unfold_rel_e; iintro F₁; iintro F₂; iintro HF.
    eapply obs_beta_l.
      apply beta_step_in_ectx; logrel_auto.
      constructor; logrel_auto.
    iintro; eapply rel_e_rule; [ | apply HF ].
    apply (rel_e_IH_l IHc).
    apply (rel_v_arrow_rule Hk Hv).
  - assumption.
Qed.

Lemma rel_k_coercion_cons_l_aux (c c₁ c₂ : crc)
  (τ₁ τ₂ : typ) (T₁ T₂ U₁ U₂ T₀ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k (τ₂)ε T₀ E₁ E₂) →
    (n ⊨ rel_k (τ₁)ε T₀ (cctx_capp c E₁) E₂)) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₁ T₀ E₁ E₂) →
    (n ⊨ rel_k T₂ T₀ (cctx_capp c₁ E₁) E₂)) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k U₂ T₀ E₁ E₂) →
    (n ⊨ rel_k U₁ T₀ (cctx_capp c₂ E₁) E₂)) →
  (n ⊨ rel_k (τ₂ ![ T₂ ] U₂) T₀ E₁ E₂) →
    (n ⊨ rel_k (τ₁ ![ T₁ ] U₁) T₀ (cctx_capp (crc_cons c c₁ c₂) E₁) E₂).
Proof.
intros IHc IHc₁ IHc₂ HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; iexists T.
  idestruct HE as k₀ HE; iexists (exp_capp (crc_arrow c c₁) k₀).
  idestruct HE as E₀ HE; iexists E₀.
  idestruct HE as E₁' HE; iexists (cctx_capp c₂ E₁').
  idestruct HE as E₂' HE; iexists E₂'.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁' [ Hk₀ [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'.
  logrel_auto.
  - eapply ectx_iota_rt_trans.
      eapply ectx_iota_rt_capp_cons; eassumption.
    constructor 1; constructor; logrel_auto.
    intros v Hv; simpl.
    apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
  - assumption.
  - later_shift.
    apply rel_ve_fix_roll; apply rel_ve_fix_unroll in HkE.
    unfold_rel_ve.
    iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
    iintro F₁; iintro F₂; iintro HF.
    eapply obs_iota_l.
      apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
    change (n ⊨ cplug (cctx_capp c (cctx_app2 k₀ (cctx_capp c₁ F₁))) v₁
             ~ᵢ cplug F₂ (cplug E₀ v₂)).
    rewrite <- cplug_cctx_append.
    eapply rel_k_pure_rule; [ | eassumption ].
    apply IHc; simpl; logrel_auto.
    iintro a₁; iintro a₂; iintro H; rewrite cplug_cctx_append.
    eapply rel_e_rule; [ | apply HF ].
    apply (rel_e_IH_l IHc₁).
    apply (rel_ve_rule HkE); assumption.
  - later_shift.
    apply rel_k_fix_roll; apply rel_k_fix_unroll in HE'.
    apply IHc₂; assumption.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; iexists (exp_capp (crc_arrow c c₁) k₁).
  idestruct HE as k₂ HE; iexists k₂.
  idestruct HE as E₁' HE; iexists (cctx_capp c₂ E₁').
  idestruct HE as E₂' HE; iexists E₂'.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'; change (n ⊨ rel_v (τ₂ ↦ T₂) (τ₀ ↦ T₀) k₁ k₂) in Hk.
  isplit; [ logrel_auto | isplit ].
  - eapply ectx_iota_rt_trans.
      eapply ectx_iota_rt_capp_cons; eassumption.
    constructor 1; constructor; logrel_auto.
    intros v Hv; simpl.
    apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
  - assumption.
  - change (n ⊨ rel_v (τ₁ ↦ T₁) (τ₀ ↦ T₀) (exp_capp (crc_arrow c c₁) k₁) k₂).
    eapply rel_coercion_arrow_l_val_pure; eassumption.
  - apply IHc₂; assumption.
Qed.

Lemma rel_k_coercion_l (c : crc) (T U : typ_eff) : c ⊩ T ≺: U → 
  ∀ E₁ E₂ T₀ n,
  (n ⊨ rel_k U T₀ E₁ E₂) → (n ⊨ rel_k T T₀ (cctx_capp c E₁) E₂).
Proof.
induction 1; intros.
+ apply rel_k_coercion_id_l_aux; assumption.
+ eapply rel_k_coercion_comp_l_aux; eassumption.
+ eapply rel_k_coercion_arrow_l_aux; eassumption.
+ eapply rel_k_coercion_lift_l_aux; eassumption.
+ eapply rel_k_coercion_cons_l_aux; eassumption.
Qed.

Lemma rel_coercion_l_cl (c : crc) (T U T₀ : typ_eff) e₁ e₂ n : c ⊩ T ≺: U → 
  (n ⊨ rel_e T T₀ e₁ e₂) → (n ⊨ rel_e U T₀ (exp_capp c e₁) e₂).
Proof.
intros Hc He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug (cctx_capp c E₁) e₁ ~ᵢ cplug E₂ e₂).
apply (rel_e_rule He).
eapply rel_k_coercion_l; eassumption.
Qed.

Lemma rel_coercion_l (c : crc) (T₁ T₂ : typ_eff) : c ⊩ T₁ ≺: T₂ → 
  ∀ (V : Set) (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (T₀ : typ_eff) (n : nat),
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ T₁ T₀) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_capp c e₁) e₂ T₂ T₀).
Proof.
intros Hsub V Γ₁ Γ₂ e₁ e₂ T₀ n He.
unfold rel_e_open; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply rel_coercion_l_cl; [ eassumption | ].
eapply rel_e_open_rule; eassumption.
Qed.

Lemma rel_v_coercion_arrow_l_cl (v₁ v₂ : exp0) 
    (c₁ c₂ : crc) (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (τ₀ : typ) (n : nat) :
  c₁ ⊩ (τ₂)ε ≺: (τ₁)ε → c₂ ⊩ T₁ ≺: T₂ →
  (n ⊨ rel_v (τ₁ ↦ T₁) τ₀ v₁ v₂) →
  (n ⊨ rel_v (τ₂ ↦ T₂) τ₀ (exp_capp (crc_arrow c₁ c₂) v₁) v₂).
Proof.
intros Hc₁ Hc₂ Hv.
eapply rel_coercion_arrow_l_val_pure.
+ apply rel_k_coercion_l; eassumption.
+ apply rel_k_coercion_l; eassumption.
+ assumption.
Qed.

Lemma rel_v_coercion_arrow_l {V : Set} (Γ₁ Γ₂ : env V) (v₁ v₂ : exp V) 
    (c₁ c₂ : crc) (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (τ₀ : typ) (n : nat) :
  c₁ ⊩ (τ₂)ε ≺: (τ₁)ε → c₂ ⊩ T₁ ≺: T₂ →
  (n ⊨ rel_v_open Γ₁ Γ₂ v₁ v₂ (τ₁ ↦ T₁) τ₀) →
  (n ⊨ rel_v_open Γ₁ Γ₂ (exp_capp (crc_arrow c₁ c₂) v₁) v₂ (τ₂ ↦ T₂) τ₀).
Proof.
intros Hc₁ Hc₂ Hv; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ; simpl.
eapply rel_v_coercion_arrow_l_cl; [ eassumption | eassumption | ].
eapply rel_v_open_rule; eassumption.
Qed.

(* ========================================================================= *)

Lemma rel_k_coercion_id_r_aux (T₁ T₂ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (n ⊨ rel_k T₁ T₂ E₁ E₂) → (n ⊨ rel_k T₁ T₂ E₁ (cctx_capp crc_id E₂)).
Proof.
intro HE.
apply @rel_k_ectx_iota_r with (E₂' := E₂).
  constructor; logrel_auto.
  intros v Hv; simpl.
  apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
assumption.
Qed.

Lemma rel_k_coercion_comp_r_aux (c₁ c₂ : crc)
  (T₀ T₁ T₂ T₃ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₃ E₁ E₂) → 
    (n ⊨ rel_k T₀ T₂ E₁ (cctx_capp c₁ E₂))) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₂ E₁ E₂) →
    (n ⊨ rel_k T₀ T₁ E₁ (cctx_capp c₂ E₂))) →
  (n ⊨ rel_k T₀ T₃ E₁ E₂) → 
    (n ⊨ rel_k T₀ T₁ E₁ (cctx_capp (crc_comp c₁ c₂) E₂)).
Proof.
intros IHc₁ IHc₂ HE.
apply @rel_k_ectx_iota_r with (E₂' := cctx_capp c₂ (cctx_capp c₁ E₂)).
  constructor; logrel_auto.
  intros v Hv; simpl.
  apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
apply IHc₂; apply IHc₁; assumption.
Qed.

Lemma rel_e_IH_r {c : crc} {T₀ T₁ T₂ : typ_eff} {e₁ e₂ : exp0} {n : nat} :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₂ E₁ E₂) →
    (n ⊨ rel_k T₀ T₁ E₁ (cctx_capp c E₂))) →
  (n ⊨ rel_e T₀ T₁ e₁ e₂) →
    (n ⊨ rel_e T₀ T₂ e₁ (exp_capp c e₂)).
Proof.
intros IHc He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug E₁ e₁ ~ᵢ cplug (cctx_capp c E₂) e₂).
apply (rel_e_rule He).
apply IHc; assumption.
Qed.

Lemma rel_coercion_arrow_r_val_pure (c₁ c₂ : crc)
  τ₀ (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (v₁ v₂ : exp0) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ (τ₁)ε E₁ E₂) →
    (n ⊨ rel_k T₀ (τ₂)ε E₁ (cctx_capp c₁ E₂))) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₂ E₁ E₂) →
    (n ⊨ rel_k T₀ T₁ E₁ (cctx_capp c₂ E₂))) →
  (n ⊨ rel_v τ₀ (τ₁ ↦ T₁) v₁ v₂) →
    (n ⊨ rel_v τ₀ (τ₂ ↦ T₂) v₁ (exp_capp (crc_arrow c₁ c₂) v₂)).
Proof.
intros IHc₁ IHc₂ Hv.
destruct τ₀ as [ | τ₀ T₀ ].
  apply Hv.
simpl; logrel_auto.
iintro a₁; iintro a₂; iintro Ha; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_iota_r.
  apply iota_step_in_ectx; logrel_auto.
  constructor; logrel_auto.
eapply rel_e_rule; [ | apply HE ].
apply (rel_e_IH_r IHc₂); eapply compat_app_cl.
  apply rel_v_in_rel_e; eassumption.
apply (rel_e_IH_r IHc₁); apply rel_v_in_rel_e; eassumption.
Qed.

Lemma rel_k_coercion_arrow_r_aux (c₁ c₂ : crc)
  T₀ (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ (τ₁)ε E₁ E₂) →
    (n ⊨ rel_k T₀ (τ₂)ε E₁ (cctx_capp c₁ E₂))) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₂ E₁ E₂) →
    (n ⊨ rel_k T₀ T₁ E₁ (cctx_capp c₂ E₂))) →
  (n ⊨ rel_k T₀ (τ₂ ↦ T₂)ε E₁ E₂) →
    (n ⊨ rel_k T₀ (τ₁ ↦ T₁)ε E₁ (cctx_capp (crc_arrow c₁ c₂) E₂)).
Proof.
intros IHc₁ IHc₂ HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  change (n ⊨ rel_v τ₀ (τ₁ ↦ T₁) v₁ v₂) in Hv.
  apply (rel_k_pure_rule HE).
  eapply rel_coercion_arrow_r_val_pure; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; iexists T.
  idestruct HE as k HE; iexists k.
  idestruct HE as E HE; iexists (cctx_capp (crc_arrow c₁ c₂) E).
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists E₂'.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁' [ Hk [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'; logrel_auto.
  - assumption.
  - eapply ectx_iota_rt_capp_arrow; eassumption.
  - later_shift.
    change (n ⊨ (rel_ve_fix τ₀ T₀ (τ₁ ↦ T₁) T) 
        k (cctx_capp (crc_arrow c₁ c₂) E)).
    change (n ⊨ (rel_ve_fix τ₀ T₀ (τ₂ ↦ T₂) T) k E) in HkE.
    apply rel_ve_fix_roll; apply rel_ve_fix_unroll in HkE.
    unfold_rel_ve.
    iintro v₁; iintro v₂; iintro Hv; simpl.
    apply (rel_ve_rule HkE).
    eapply rel_coercion_arrow_r_val_pure; eassumption.
  - assumption.
Qed.

Lemma rel_k_coercion_lift_r_aux (c : crc)
  T₀ (τ : typ) (T U : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ U E₁ E₂) →
    (n ⊨ rel_k T₀ T E₁ (cctx_capp c E₂))) →
  (n ⊨ rel_k T₀ (τ ![ T ] U) E₁ E₂) →
    (n ⊨ rel_k T₀ (τ)ε E₁ (cctx_capp (crc_lift c) E₂)).
Proof.
intros IHc HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  simpl in HE.
  idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T₀ HE; idestruct HE as E₀ HE; idestruct HE as k₀ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as HEk HE'.
  apply (obs_ectx_iota_rt_r Hι₂); simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; logrel_auto.
    constructor; logrel_auto.
  eapply obs_ectx_iota_rt_l; [ eassumption | ].
  rewrite cplug_cctx_append.
  eapply rel_e_rule; [ | eassumption ].
  apply (rel_e_IH_r IHc).
  apply (rel_ev_rule HEk); assumption.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  iexists U.
  idestruct HE as k₁ HE; iexists k₁.
  idestruct HE as k₂ HE;
    iexists (cctx_capp (crc_lift c) (cctx_app1 cctx_hole k₂)).
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists E₂'.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ₀ ↦ T₀) (τ ↦ T) k₁ k₂) in Hk.
  logrel_auto.
  - assumption.
  - apply (ectx_iota_rt_capp_lift Hι₂).
  - iintro; apply rel_ve_fix_roll; unfold_rel_ev.
    iintro v₁; iintro v₂; iintro Hv; simpl.
    unfold_rel_e; iintro F₁; iintro F₂; iintro HF.
    eapply obs_beta_r.
      apply beta_step_in_ectx; logrel_auto.
      constructor; logrel_auto.
    eapply rel_e_rule; [ | apply HF ].
    apply (rel_e_IH_r IHc).
    apply (rel_v_arrow_rule Hk Hv).
  - iintro; apply rel_k_fix_roll; assumption.
Qed.

Lemma rel_k_coercion_cons_r_aux (c c₁ c₂ : crc)
  T₀ (τ₁ τ₂ : typ) (T₁ T₂ U₁ U₂ : typ_eff) (E₁ E₂ : cctx) (n : nat) :
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ (τ₂)ε E₁ E₂) →
    (n ⊨ rel_k T₀ (τ₁)ε E₁ (cctx_capp c E₂))) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ T₁ E₁ E₂) →
    (n ⊨ rel_k T₀ T₂ E₁ (cctx_capp c₁ E₂))) →
  (∀ E₁ E₂ T₀ n, (n ⊨ rel_k T₀ U₂ E₁ E₂) →
    (n ⊨ rel_k T₀ U₁ E₁ (cctx_capp c₂ E₂))) →
  (n ⊨ rel_k T₀ (τ₂ ![ T₂ ] U₂) E₁ E₂) →
    (n ⊨ rel_k T₀ (τ₁ ![ T₁ ] U₁) E₁ (cctx_capp (crc_cons c c₁ c₂) E₂)).
Proof.
intros IHc IHc₁ IHc₂ HE.
destruct T₀ as [ τ₀ | τ₀ T₀ U₀ ]; simpl; logrel_auto.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; iexists T.
  idestruct HE as E₀ HE; iexists E₀.
  idestruct HE as k₀ HE; iexists (exp_capp (crc_arrow c c₁) k₀).
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists (cctx_capp c₂ E₂').
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as HEk HE'.
  logrel_auto.
  - assumption.
  - eapply ectx_iota_rt_trans.
      eapply ectx_iota_rt_capp_cons; eassumption.
    constructor 1; constructor; logrel_auto.
    intros v Hv; simpl.
    apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
  - unfold_rel_ve.
    iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
    iintro F₁; iintro F₂; iintro HF.
    eapply obs_iota_r.
      apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
    change (n ⊨ cplug F₁ (cplug E₀ v₁)
             ~ᵢ cplug (cctx_capp c (cctx_app2 k₀ (cctx_capp c₁ F₂))) v₂).
    rewrite <- cplug_cctx_append.
    eapply rel_k_pure_rule; [ | eassumption ].
    apply IHc; simpl; logrel_auto.
    iintro a₁; iintro a₂; iintro H; rewrite cplug_cctx_append.
    eapply rel_e_rule; [ | apply HF ].
    apply (rel_e_IH_r IHc₁).
    apply (rel_ev_rule HEk); assumption.
  - apply IHc₂; assumption.
+ simpl in HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; iexists k₁.
  idestruct HE as k₂ HE; iexists (exp_capp (crc_arrow c c₁) k₂).
  idestruct HE as E₁' HE; iexists E₁'.
  idestruct HE as E₂' HE; iexists (cctx_capp c₂ E₂').
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'; change (n ⊨ rel_v (τ₀ ↦ T₀) (τ₂ ↦ T₂) k₁ k₂) in Hk.
  isplit; [ logrel_auto | isplit ].
  - assumption.
  - eapply ectx_iota_rt_trans.
      eapply ectx_iota_rt_capp_cons; eassumption.
    constructor 1; constructor; logrel_auto.
    intros v Hv; simpl.
    apply iota_step_in_ectx; [ | constructor ]; logrel_auto.
  - change (n ⊨ rel_v (τ₀ ↦ T₀) (τ₁ ↦ T₁) k₁ (exp_capp (crc_arrow c c₁) k₂)).
    eapply rel_coercion_arrow_r_val_pure; eassumption.
  - apply IHc₂; assumption.
Qed.

Lemma rel_k_coercion_r (c : crc) (T U : typ_eff) : c ⊩ T ≺: U → 
  ∀ E₁ E₂ T₀ n,
  (n ⊨ rel_k T₀ U E₁ E₂) → (n ⊨ rel_k T₀ T E₁ (cctx_capp c E₂)).
Proof.
induction 1; intros.
+ apply rel_k_coercion_id_r_aux; assumption.
+ eapply rel_k_coercion_comp_r_aux; eassumption.
+ eapply rel_k_coercion_arrow_r_aux; eassumption.
+ eapply rel_k_coercion_lift_r_aux; eassumption.
+ eapply rel_k_coercion_cons_r_aux; eassumption.
Qed.

Lemma rel_coercion_r_cl (c : crc) (T₀ T U : typ_eff) e₁ e₂ n : c ⊩ T ≺: U → 
  (n ⊨ rel_e T₀ T e₁ e₂) → (n ⊨ rel_e T₀ U e₁ (exp_capp c e₂)).
Proof.
intros Hc He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug E₁ e₁ ~ᵢ cplug (cctx_capp c E₂) e₂).
apply (rel_e_rule He).
eapply rel_k_coercion_r; eassumption.
Qed.

Lemma rel_coercion_r (c : crc) (T₁ T₂ : typ_eff) : c ⊩ T₁ ≺: T₂ → 
  ∀ (V : Set) (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (T₀ : typ_eff) (n : nat),
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ T₀ T₁) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ (exp_capp c e₂) T₀ T₂).
Proof.
intros Hsub V Γ₁ Γ₂ e₁ e₂ T₀ n He.
unfold rel_e_open; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply rel_coercion_r_cl; [ eassumption | ].
eapply rel_e_open_rule; eassumption.
Qed.

Lemma rel_v_coercion_arrow_r_cl (v₁ v₂ : exp0) 
    (c₁ c₂ : crc) (τ₀ τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (n : nat) :
  c₁ ⊩ (τ₂)ε ≺: (τ₁)ε → c₂ ⊩ T₁ ≺: T₂ →
  (n ⊨ rel_v τ₀ (τ₁ ↦ T₁) v₁ v₂) →
  (n ⊨ rel_v τ₀ (τ₂ ↦ T₂) v₁ (exp_capp (crc_arrow c₁ c₂) v₂)).
Proof.
intros Hc₁ Hc₂ Hv.
eapply rel_coercion_arrow_r_val_pure.
+ apply rel_k_coercion_r; eassumption.
+ apply rel_k_coercion_r; eassumption.
+ assumption.
Qed.

Lemma rel_v_coercion_arrow_r {V : Set} (Γ₁ Γ₂ : env V) (v₁ v₂ : exp V) 
    (c₁ c₂ : crc) (τ₀ τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (n : nat) :
  c₁ ⊩ (τ₂)ε ≺: (τ₁)ε → c₂ ⊩ T₁ ≺: T₂ →
  (n ⊨ rel_v_open Γ₁ Γ₂ v₁ v₂ τ₀ (τ₁ ↦ T₁)) →
  (n ⊨ rel_v_open Γ₁ Γ₂ v₁ (exp_capp (crc_arrow c₁ c₂) v₂) τ₀ (τ₂ ↦ T₂)).
Proof.
intros Hc₁ Hc₂ Hv; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ; simpl.
eapply rel_v_coercion_arrow_r_cl; [ eassumption | eassumption | ].
eapply rel_v_open_rule; eassumption.
Qed.
