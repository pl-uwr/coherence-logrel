Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.LogicalRelation.Relation.
Require Export Shift0.LogicalRelation.StructuralLemmas.

Ltac logrel_auto_fvalue :=
  match goal with
  | [ |- fvalue (exp_abs _) ] => constructor
  | _ => idtac
  end.

Ltac logrel_auto_value :=
  match goal with
  | [ |- value (exp_abs _) ] => constructor
  | [ |- value (exp_fix _) ] => constructor
  | [ |- value (exp_capp (crc_arrow _ _) _) ] =>
      constructor; logrel_auto_value
  | [ H: _ ⊨ rel_v _ _ ?V _ |- value ?V ] =>
      apply (proj1 (rel_v_relates_values H))
  | [ H: _ ⊨ rel_v _ _ _ ?V |- value ?V ] =>
      apply (proj2 (rel_v_relates_values H))
  | [ H: _ ⊨ rel_ev _ _ _ _ _ ?V |- value ?V ] =>
      apply (proj2 (rel_ev_relates_ectx_and_value H))
  | [ H: fvalue ?V |- value ?V ] =>
      apply fvalue_is_value; apply H
  | [ H: value ?V |- value ?V ] => apply H
  | _ => idtac
  end.

Ltac logrel_auto_ectx :=
  match goal with
  | [ H: ectx ?E |- ectx ?E ] => apply H
  | [ |- ectx cctx_hole ] => constructor
  | [ |- ectx (cctx_app1 _ _) ] => constructor; logrel_auto_ectx
  | [ |- ectx (cctx_app2 _ _) ] => 
      constructor; [ logrel_auto_value | logrel_auto_ectx ]
  | [ |- ectx (cctx_capp _ _) ] => constructor; logrel_auto_ectx
  | [ |- ectx (cctx_succ _) ] => constructor; logrel_auto_ectx
  | [ |- ectx (cctx_case _ _ _) ] => constructor; logrel_auto_ectx
  | [ |- ectx (cctx_append _ _ ) ] => apply ectx_append; logrel_auto_ectx
  | [ H: _ ⊨ rel_k _ _ ?E _ |- ectx ?E ] =>
      apply (proj1 (rel_k_relates_ectx H))
  | [ H: _ ⊨ rel_k _ _ _ ?E |- ectx ?E ] =>
      apply (proj2 (rel_k_relates_ectx H))
  | [ H: _ ⊨ rel_ev _ _ _ _ ?E _ |- ectx ?E ] =>
      apply (proj1 (rel_ev_relates_ectx_and_value H))
  | [ H: _ ⊨ rel_ve _ _ _ _ _ ?E |- ectx ?E ] =>
      apply (proj2 (rel_ve_relates_value_and_ectx H))
  | _ => idtac
  end.

Ltac logrel_auto :=
  match goal with
  | [ |- _ ⊨ _ ∧ᵢ _ ] => isplit; logrel_auto
  | [ |- _ ⊨ (_)ᵢ ]   => iintro; logrel_auto
  | [ |- _ ∧ _ ]      => split; logrel_auto
  | [ |- value _ ]    => logrel_auto_value
  | [ |- fvalue _ ]   => logrel_auto_fvalue
  | [ |- ectx _ ]     => logrel_auto_ectx
  | _ => idtac
  end.

Ltac unfold_rel_e :=
  unfold grel_e.
Ltac unfold_rel_ev :=
  unfold grel_ev; isplit; [ logrel_auto | ].
Ltac unfold_rel_ve :=
  unfold grel_ve; isplit; [ logrel_auto | ].

Lemma rel_k_pure_eff_fold {τ₁ τ₂ : typ} {T₂ U₂ : typ_eff}
    {E₁ E₂ : cctx} {n : nat}
    {T : typ_eff} {E : cctx} {k : exp0} {E₁' E₂' : cctx} :
  ectx E₁ → ectx E₂ →
  ectx_iota_rt E₁ (cctx_append E₁' E) →
  ectx_iota_rt E₂ (cctx_app1 E₂' k) →
  (n ⊨ rel_ev τ₁ T τ₂ T₂ E k) →
  (n ⊨ rel_k T U₂ E₁' E₂') →
  (n ⊨ rel_k (τ₁)ε (τ₂ ![ T₂ ] U₂) E₁ E₂).
Proof.
intros HE₁ HE₂ Hι₁ Hι₂ HEk HE'.
simpl; logrel_auto.
iexists T; iexists E; iexists k; iexists E₁'; iexists E₂'.
logrel_auto; assumption.
Qed.

Lemma rel_k_eff_pure_fold {τ₁ τ₂ : typ} {T₁ U₁ : typ_eff}
    {E₁ E₂ : cctx} {n : nat}
    {T : typ_eff} {k : exp0} {E : cctx} {E₁' E₂' : cctx} :
  ectx E₁ → ectx E₂ → ectx E₁' → value k →
  ectx_iota_rt E₁ (cctx_app1 E₁' k) →
  ectx_iota_rt E₂ (cctx_append E₂' E) →
  (n ⊨ ▷rel_ve τ₁ T₁ τ₂ T k E) →
  (n ⊨ ▷rel_k U₁ T E₁' E₂') →
  (n ⊨ rel_k (τ₁ ![ T₁ ] U₁) (τ₂)ε E₁ E₂).
Proof.
intros HE₁ HE₂ HE₁' Hk Hι₁ Hι₂ HkE HE'.
simpl; logrel_auto.
iexists T; iexists k; iexists E; iexists E₁'; iexists E₂'.
logrel_auto; try assumption.
+ later_shift; apply rel_ve_fix_roll; assumption.
+ later_shift; apply rel_k_fix_roll; assumption.
Qed.

Lemma rel_k_eff_eff_fold {τ₁ τ₂ : typ} {T₁ T₂ U₁ U₂ : typ_eff}
    {E₁ E₂ : cctx} {n : nat}
    {k₁ k₂ : exp0} {E₁' E₂' : cctx} :
  ectx E₁ → ectx E₂ →
  ectx_iota_rt E₁ (cctx_app1 E₁' k₁) →
  ectx_iota_rt E₂ (cctx_app1 E₂' k₂) →
  (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ T₂) k₁ k₂) →
  (n ⊨ rel_k U₁ U₂ E₁' E₂') →
  (n ⊨ rel_k (τ₁ ![ T₁ ] U₁) (τ₂ ![ T₂ ] U₂) E₁ E₂).
Proof.
intros HE₁ HE₂ Hι₁ Hι₂ Hk HE'.
simpl; logrel_auto.
iexists k₁; iexists k₂; iexists E₁'; iexists E₂'.
isplit; [ logrel_auto | isplit ]; assumption.
Qed.

Ltac unfold_rel_k :=
  match goal with
  | [ |- _ ⊨ rel_k (_)ε (_ ![ _ ] _) _ _ ] =>
      eapply rel_k_pure_eff_fold; logrel_auto
  | [ |- _ ⊨ rel_k (_ ![ _ ] _) (_)ε _ _ ] =>
      eapply rel_k_eff_pure_fold; logrel_auto
  | [ |- _ ⊨ rel_k (_ ![ _ ] _) (_ ![ _ ] _) _ _ ] =>
      eapply rel_k_eff_eff_fold; logrel_auto
  end.