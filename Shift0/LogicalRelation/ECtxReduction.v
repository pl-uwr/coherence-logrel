Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.Target.ReductionClassify.
Require Import Shift0.Target.IotaNormalization.
Require Import Shift0.LogicalRelation.Relation.
Require Import Shift0.LogicalRelation.DerivedRules.
Require Import Shift0.LogicalRelation.Auto.

Lemma not_value_ectx (E : cctx) :
  ∀ e, ¬value e → ¬value (cplug E e).
Proof.
induction E; simpl; intros e₀ He₀.
+ auto.
+ apply IHE; intro Hv; inversion Hv.
+ apply IHE; intro Hv; inversion Hv.
+ apply IHE; intro Hv; inversion Hv; auto.
+ apply IHE; intro Hv; inversion Hv.
+ apply IHE; intro Hv; inversion Hv.
Qed.

Lemma obs_ectx_iota_l_aux {E₁ E₂ : cctx} e₂ n :
  (ectx_iota E₁ E₂) → (n ⊨ ∀ᵢ e₁, cplug E₂ e₁ ~ᵢ e₂ ⇒ cplug E₁ e₁ ~ᵢ e₂).
Proof.
intro Hι; destruct Hι as [ HE₁ HE₂ HE ].
loeb_induction.
iintro e₁.
induction (iota_is_normalizing e₁); iintro H₁.
+ destruct (beta_iota_classify e) as [ Hv | e' Hβ | e' Hι | Hst Hβ Hι ].
  - eapply obs_iota_l; [ apply HE | ]; eassumption.
  - eapply obs_beta_l; [ apply Hβ; assumption | ].
    eapply obs_beta_l' in H₁; [ | apply Hβ; assumption ].
    later_shift.
    iespecialize IH; iapply IH; eassumption.
  - exfalso; eapply H; apply Hι with (E := cctx_hole); constructor.
  - apply I_valid_intro; intro Hs; inversion Hs; exfalso.
    * eapply not_value_ectx; eassumption.
    * eapply Hβ; [ | eassumption ]; assumption.
    * eapply Hι; [ | eassumption ]; assumption.
+ eapply obs_iota_l.
    apply iota_in_ectx; eassumption.
  iapply IHi.
  eapply obs_iota_l'.
    apply iota_in_ectx; eassumption.
  assumption.
Qed.

Lemma obs_ectx_iota_l {E₁ E₂ : cctx} {e₁ e₂ : exp0} {n : nat} :
  (ectx_iota E₁ E₂) → (n ⊨ cplug E₂ e₁ ~ᵢ e₂) → (n ⊨ cplug E₁ e₁ ~ᵢ e₂).
Proof.
intros Hι Hobs; assert (Ht := obs_ectx_iota_l_aux e₂ n Hι).
iespecialize Ht; iapply Ht; assumption.
Qed.

Lemma obs_ectx_iota_rt_l {E₁ E₂ : cctx} {e₁ e₂ : exp0} {n : nat} :
  (ectx_iota_rt E₁ E₂) → (n ⊨ cplug E₂ e₁ ~ᵢ e₂) → (n ⊨ cplug E₁ e₁ ~ᵢ e₂).
Proof.
induction 1; auto. 
apply obs_ectx_iota_l; auto.
Qed.

Lemma obs_ectx_iota_r {E₁ E₂ : cctx} {e₁ e₂ : exp0} {n : nat} :
  (ectx_iota E₁ E₂) → (n ⊨ e₁ ~ᵢ cplug E₂ e₂) → (n ⊨ e₁ ~ᵢ cplug E₁ e₂).
Proof.
intros Hι Hobs; destruct Hι as [ Hι₁ Hι₂ HEι ].
apply I_valid_intro; simpl; unfold obs_func.
apply I_valid_elim in Hobs; simpl in Hobs; unfold obs_func in Hobs.
intro Hsn; specialize (Hobs Hsn); clear Hsn.
assert (H: ∀ e, stops e → ∀ e₂, e = cplug E₂ e₂ → stops (cplug E₁ e₂));
  [ | eapply H; try eassumption; trivial ].
clear Hobs e₁ e₂; induction 1; intros e₂ He; subst.
+ destruct (beta_iota_classify e₂) as [ Hv | e' Hβ | e' Hι | Hst Hβ Hι ].
  - econstructor 3; [ eapply HEι; assumption | ].
    constructor; assumption.
  - exfalso; eapply value_beta_normal; [ eassumption | ].
    apply Hβ; assumption.
  - exfalso; eapply value_iota_normal; [ eassumption | ].
    apply Hι; assumption.
  - exfalso; eapply not_value_ectx; eassumption.
+ destruct (beta_iota_classify e₂) as [ Hv | e Hβ | e Hι | Hst Hβ Hι ].
  - econstructor 3; [ eapply HEι; assumption | ].
    econstructor 2; eassumption.
  - econstructor 2; [ apply Hβ; assumption | ].
    apply IHstops; eapply beta_unique; [ eassumption | ].
    apply Hβ; assumption.
  - exfalso; eapply beta_or_iota; [ eassumption | ].
    apply Hι; assumption.
  - exfalso; eapply Hβ; [ | eassumption ]; assumption.
+ destruct (beta_iota_classify e₂) as [ Hv | e Hβ | e Hι | Hst Hβ Hι ].
  - econstructor 3; [ eapply HEι; assumption | ].
    econstructor 3; eassumption.
  - exfalso; eapply beta_or_iota; [ | eassumption ].
    apply Hβ; assumption.
  - econstructor 3; [ apply Hι; assumption | ].
    apply IHstops; eapply iota_unique; [ eassumption | ].
    apply Hι; assumption.
  - exfalso; eapply Hι; [ | eassumption ]; assumption.
Qed.

Lemma obs_ectx_iota_rt_r {E₁ E₂ : cctx} {e₁ e₂ : exp0} {n : nat} :
  (ectx_iota_rt E₁ E₂) → (n ⊨ e₁ ~ᵢ cplug E₂ e₂) → (n ⊨ e₁ ~ᵢ cplug E₁ e₂).
Proof.
induction 1.
+ eapply obs_ectx_iota_r; eassumption.
+ auto.
+ auto.
Qed.

Lemma rel_k_ectx_iota_l {T₁ T₂ : typ_eff} {E₁ E₁' E₂ : cctx} {n : nat} :
  (ectx_iota E₁ E₁') → (n ⊨ rel_k T₁ T₂ E₁' E₂) → (n ⊨ rel_k T₁ T₂ E₁ E₂).
Proof.
intros Hι HE; destruct Hι as [ Hι₁ Hι₂ Hι ].
destruct T₁ as [ τ₁ | τ₁ T₁ U₁ ]; destruct T₂ as [ τ₂ | τ₂ T₂ U₂ ];
  simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  eapply obs_iota_l.
    apply Hι; logrel_auto.
  apply (rel_k_pure_rule HE); assumption.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as T HE; iexists T.
  idestruct HE as E HE; iexists E.
  idestruct HE as k HE; iexists k.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  logrel_auto.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₁'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as T HE; iexists T.
  idestruct HE as k HE; iexists k.
  idestruct HE as E HE; iexists E.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  logrel_auto.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₁'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as k₁ HE; iexists k₁.
  idestruct HE as k₂ HE; iexists k₂.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  isplit; [ logrel_auto | isplit ].
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₁'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
Qed.

Lemma rel_k_ectx_iota_r {T₁ T₂ : typ_eff} {E₁ E₂ E₂' : cctx} {n : nat} :
  (ectx_iota E₂ E₂') → (n ⊨ rel_k T₁ T₂ E₁ E₂') → (n ⊨ rel_k T₁ T₂ E₁ E₂).
Proof.
intros Hι HE; destruct Hι as [ Hι₁ Hι₂ Hι ].
destruct T₁ as [ τ₁ | τ₁ T₁ U₁ ]; destruct T₂ as [ τ₂ | τ₂ T₂ U₂ ];
  simpl; logrel_auto.
+ iintro v₁; iintro v₂; iintro Hv.
  eapply obs_iota_r.
    apply Hι; logrel_auto.
  apply (rel_k_pure_rule HE); assumption.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as T HE; iexists T.
  idestruct HE as E HE; iexists E.
  idestruct HE as k HE; iexists k.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  logrel_auto.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₂'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as T HE; iexists T.
  idestruct HE as k HE; iexists k.
  idestruct HE as E HE; iexists E.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  logrel_auto.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₂'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
+ simpl in HE; apply I_conj_elim2 in HE.
  idestruct HE as k₁ HE; iexists k₁.
  idestruct HE as k₂ HE; iexists k₂.
  idestruct HE as F₁ HE; iexists F₁.
  idestruct HE as F₂ HE; iexists F₂.
  isplit; [ logrel_auto | isplit ].
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE; apply HE.
  - apply I_conj_elim1 in HE; apply I_Prop_elim in HE.
    apply (ectx_iota_rt_trans E₂'); [ | apply HE ].
      constructor 1; constructor; assumption.
  - apply I_conj_elim2 in HE; apply I_conj_elim1 in HE; apply HE.
  - apply I_conj_elim2 in HE; apply I_conj_elim2 in HE; apply HE.
Qed.
