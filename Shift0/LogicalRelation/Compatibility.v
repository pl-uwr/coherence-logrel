Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.LogicalRelation.Relation.
Require Import Shift0.LogicalRelation.Auto.
Require Import Shift0.LogicalRelation.DerivedRules.
Require Import Shift0.LogicalRelation.ECtxReduction.

Lemma compat_var_v {V : Set} (Γ₁ Γ₂ : env V) (x : V) :
  ⊨ rel_v_open Γ₁ Γ₂ (exp_var x) (exp_var x) (Γ₁ x) (Γ₂ x).
Proof.
intro n; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ; simpl.
apply rel_g_rule; assumption.
Qed.

Lemma compat_var {V : Set} (Γ₁ Γ₂ : env V) (x : V) :
  ⊨ rel_e_open Γ₁ Γ₂ (exp_var x) (exp_var x) (Γ₁ x)ε (Γ₂ x)ε.
Proof.
intro n; apply rel_v_open_in_rel_e_open.
apply compat_var_v; assumption.
Qed.

Ltac apply_compat_var :=
  match goal with
| [ |- ?n ⊨ rel_v_open ?Γ₁ ?Γ₂ (exp_var ?x) (exp_var ?x) (?τ₁) (?τ₂) ] =>
    unify (Γ₁ x) τ₁; unify (Γ₂ x) τ₂;
    change (n ⊨ rel_v_open Γ₁ Γ₂ (exp_var x) (exp_var x) (Γ₁ x) (Γ₂ x));
    apply compat_var_v
  | [ |- ?n ⊨ rel_e_open ?Γ₁ ?Γ₂ (exp_var ?x) (exp_var ?x) (?τ₁)ε (?τ₂)ε ] =>
    unify (Γ₁ x) τ₁; unify (Γ₂ x) τ₂;
    change (n ⊨ rel_e_open Γ₁ Γ₂ (exp_var x) (exp_var x) (Γ₁ x)ε (Γ₂ x)ε);
    apply compat_var
  end.

Lemma compat_const_v {V : Set} (Γ₁ Γ₂ : env V) (m : nat) :
  ⊨ rel_v_open Γ₁ Γ₂ (exp_const m) (exp_const m) ℕ ℕ.
Proof.
intro n; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ.
simpl; iintro; constructor.
Qed.

Lemma compat_const {V : Set} (Γ₁ Γ₂ : env V) (m : nat) :
  ⊨ rel_e_open Γ₁ Γ₂ (exp_const m) (exp_const m) (ℕ)ε (ℕ)ε.
Proof.
intro n; apply rel_v_open_in_rel_e_open.
apply compat_const_v.
Qed.

Lemma compat_abs_v {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc V))
  (τ₁ τ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / τ₁) (Γ₂ / τ₂) e₁ e₂ T₁ T₂) →
  (n ⊨ rel_v_open Γ₁ Γ₂ (exp_abs e₁) (exp_abs e₂) (τ₁ ↦ T₁) (τ₂ ↦ T₂)).
Proof.
intro He; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ.
simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
iintro; repeat rewrite subst_bind_lift.
eapply rel_e_rule; [ | eassumption ].
apply (rel_e_open_rule He).
unfold rel_g; iintro x; destruct x as [ | x ]; simpl.
  assumption.
  iapply Hγ.
Qed.

Lemma compat_abs {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc V))
  (τ₁ τ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / τ₁) (Γ₂ / τ₂) e₁ e₂ T₁ T₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_abs e₁) (exp_abs e₂) (τ₁ ↦ T₁)ε (τ₂ ↦ T₂)ε).
Proof.
intro He; apply rel_v_open_in_rel_e_open.
apply compat_abs_v; assumption.
Qed.

Lemma compat_app_cl
  (f₁ f₂ e₁ e₂ : exp0)
  (τ₁ τ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e (τ₁ ↦ T₁)ε (τ₂ ↦ T₂)ε f₁ f₂) →
  (n ⊨ rel_e (τ₁)ε (τ₂)ε e₁ e₂) →
  (n ⊨ rel_e T₁ T₂ (exp_app f₁ e₁) (exp_app f₂ e₂)).
Proof.
intros Hf He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug (cctx_app1 E₁ e₁) f₁ ~ᵢ cplug (cctx_app1 E₂ e₂) f₂).
apply (rel_e_rule Hf); clear f₁ f₂ Hf; simpl; logrel_auto.
iintro f₁; iintro f₂; iintro Hf.
change (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ T₂) f₁ f₂) in Hf.
change (n ⊨ cplug (cctx_app2 f₁ E₁) e₁ ~ᵢ cplug (cctx_app2 f₂ E₂) e₂).
apply (rel_e_rule He); simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv.
eapply rel_e_rule; [ | apply HE ].
apply (rel_v_arrow_rule Hf Hv).
Qed.

Lemma compat_app {V : Set} (Γ₁ Γ₂ : env V)
  (f₁ f₂ e₁ e₂ : exp V)
  (τ₁ τ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ f₁ f₂ (τ₁ ↦ T₁)ε (τ₂ ↦ T₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁)ε (τ₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_app f₁ e₁) (exp_app f₂ e₂) T₁ T₂).
Proof.
intros Hf He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; simpl.
eapply compat_app_cl.
+ apply (rel_e_open_rule Hf); assumption.
+ apply (rel_e_open_rule He); assumption.
Qed.

Lemma compat_kabs {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc V))
  (τ₁ τ₂ : typ)
  (T₁ T₂ U₁ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / τ₁ ↦ T₁) (Γ₂ / τ₂ ↦ T₂) e₁ e₂ U₁ U₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_abs e₁) (exp_abs e₂) 
      (τ₁ ![ T₁ ] U₁) (τ₂ ![ T₂ ] U₂)).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE; simpl; simpl in HE.
idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁ HE₂ ].
idestruct HE as k₁ HE; idestruct HE as k₂ HE.
idestruct HE as E₁' HE; idestruct HE as E₂' HE.
idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀; destruct HE₀ as [HEι₁ HEι₂].
idestruct HE as Hk HE'.
change (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ T₂) k₁ k₂) in Hk.
eapply obs_ectx_iota_rt_r; [ eassumption | ].
eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
iintro; repeat rewrite subst_bind_lift.
eapply rel_e_rule; [ | eassumption ].
apply (rel_e_open_rule He).
unfold rel_g; iintro x; destruct x as [ | x ]; simpl.
  assumption.
  iapply Hγ.
Qed.

Lemma compat_kapp_cl
  (e₁ e₂ k₁ k₂ : exp0)
  (τ₁ τ₂ : typ)
  (T₁ T₂ U₁ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e (τ₁ ![ T₁ ] U₁) (τ₂ ![ T₂ ] U₂) e₁ e₂) →
  (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ T₂) k₁ k₂) →
  (n ⊨ rel_e U₁ U₂ (exp_app e₁ k₁) (exp_app e₂ k₂)).
Proof.
intros He Hl; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
change (n ⊨ cplug (cctx_app1 E₁ k₁) e₁ ~ᵢ cplug (cctx_app1 E₂ k₂) e₂).
apply (rel_e_rule He).
unfold_rel_k; [ econstructor 2 | econstructor 2 | | ]; assumption.
Qed.

Lemma compat_kapp {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ k₁ k₂ : exp V)
  (τ₁ τ₂ : typ)
  (T₁ T₂ U₁ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁ ![ T₁ ] U₁) (τ₂ ![ T₂ ] U₂)) →
  (n ⊨ rel_v_open Γ₁ Γ₂ k₁ k₂ (τ₁ ↦ T₁) (τ₂ ↦ T₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_app e₁ k₁) (exp_app e₂ k₂) U₁ U₂).
Proof.
intros He Hk; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; simpl.
eapply compat_kapp_cl.
+ apply (rel_e_open_rule He); assumption.
+ apply (rel_v_open_rule Hk); assumption.
Qed.

Lemma compat_fix_v {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc (inc V)))
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / τ₁ ↦ T₁ / τ₁) (Γ₂ / τ₂ ↦ T₂ / τ₂) e₁ e₂ T₁ T₂) →
  (n ⊨ rel_v_open Γ₁ Γ₂ (exp_fix e₁) (exp_fix e₂) (τ₁ ↦ T₁) (τ₂ ↦ T₂)).
Proof.
intro He; unfold rel_v_open.
iintro γ₁; iintro γ₂; iintro Hγ.
loeb_induction.
simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
repeat rewrite subst_bind_lift2.
later_shift.
eapply rel_e_rule; [ | eassumption ].
apply (rel_e_open_rule He).
unfold rel_g; iintro x; destruct x as [ | x ]; [ | destruct x ].
+ simpl; assumption.
+ apply IH.
+ simpl; apply rel_g_rule; assumption.
Qed.

Lemma compat_fix {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc (inc V)))
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / τ₁ ↦ T₁ / τ₁) (Γ₂ / τ₂ ↦ T₂ / τ₂) e₁ e₂ T₁ T₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_fix e₁) (exp_fix e₂) (τ₁ ↦ T₁)ε (τ₂ ↦ T₂)ε).
Proof.
intro He; apply rel_v_open_in_rel_e_open.
apply compat_fix_v; assumption.
Qed.

Lemma compat_succ_cl (e₁ e₂ : exp0) (n : nat) :
  (n ⊨ rel_e (ℕ)ε (ℕ)ε e₁ e₂) →
  (n ⊨ rel_e (ℕ)ε (ℕ)ε (exp_succ e₁) (exp_succ e₂)).
Proof.
intro He; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE; simpl.
change (n ⊨ cplug (cctx_succ E₁) e₁ ~ᵢ cplug (cctx_succ E₂) e₂).
eapply (rel_e_rule He).
simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv.
apply I_Prop_elim in Hv; inversion Hv.
eapply obs_beta_r.
  apply beta_step_in_ectx; logrel_auto; constructor.
eapply obs_beta_l.
  apply beta_step_in_ectx; logrel_auto; constructor.
iintro; eapply rel_k_pure_rule; [ eassumption | ].
simpl; iintro; constructor.
Qed.

Lemma compat_succ {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ)ε (ℕ)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_succ e₁) (exp_succ e₂) (ℕ)ε (ℕ)ε).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
change (n ⊨ rel_e (ℕ )ε (ℕ )ε (exp_succ (bind γ₁ e₁)) (exp_succ (bind γ₂ e₂))).
apply compat_succ_cl.
apply (rel_e_open_rule He); assumption.
Qed.

Lemma compat_case {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ z₁ z₂ : exp V)
  (s₁ s₂ : exp (inc V))
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ)ε (ℕ)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ z₁ z₂ T₁ T₂) →
  (n ⊨ rel_e_open (Γ₁ / ℕ) (Γ₂ / ℕ) s₁ s₂ T₁ T₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_case e₁ z₁ s₁) (exp_case e₂ z₂ s₂) T₁ T₂).
Proof.
intros He Hz Hs; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE; simpl.
change (n ⊨ cplug (cctx_case E₁ (bind γ₁ z₁) (bind (lift γ₁) s₁)) (bind γ₁ e₁)
       ~ᵢ cplug (cctx_case E₂ (bind γ₂ z₂) (bind (lift γ₂) s₂)) (bind γ₂ e₂)).
eapply rel_e_rule.
  apply (rel_e_open_rule He); assumption.
simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv.
apply I_Prop_elim in Hv; inversion Hv as [ m ]; destruct m.
+ eapply obs_beta_r.
    apply beta_step_in_ectx; [ | constructor ]; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | constructor ]; logrel_auto.
  iintro; eapply rel_e_rule; [ | eassumption ].
  apply (rel_e_open_rule Hz); assumption.
+ eapply obs_beta_r.
    apply beta_step_in_ectx; [ | constructor ]; trivial; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | constructor ]; trivial; logrel_auto.
  repeat rewrite subst_bind_lift.
  iintro; eapply rel_e_rule; [ | eassumption ].
  apply (rel_e_open_rule Hs).
  unfold rel_g; iintro x; destruct x; simpl.
  - iintro; constructor.
  - apply rel_g_rule; assumption.
Qed.

Lemma compat_weaken {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (τ₁ τ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ T₁ T₂) →
  (n ⊨ rel_e_open (Γ₁ / τ₁) (Γ₂ / τ₂) (weaken e₁) (weaken e₂) T₁ T₂).
Proof.
intros He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
repeat erewrite monad_bind_map;
  [
  | intro x; rewrite monad_map_id'; 
    instantiate (1 := λ x, _ (VS x)); reflexivity
  | intro x; rewrite monad_map_id';
    instantiate (1 := λ x, _ (VS x)); reflexivity
  ].
repeat rewrite monad_map_id'.
repeat rewrite monad_bind_return'.
apply (rel_e_open_rule He).
unfold rel_g; iintro x.
unfold rel_g in Hγ; ispecialize Hγ (VS x).
apply Hγ.
Qed.

Lemma compat_weaken1 {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp (inc V))
  (τ₁ τ₂ σ₁ σ₂ : typ)
  (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open (Γ₁ / σ₁) (Γ₂ / σ₂) e₁ e₂ T₁ T₂) →
  (n ⊨ rel_e_open (Γ₁ / τ₁ / σ₁) (Γ₂ / τ₂ / σ₂) 
      (weaken1 e₁) (weaken1 e₂) T₁ T₂).
Proof.
intros He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
repeat erewrite monad_bind_map;
  [ repeat rewrite monad_map_id'; repeat rewrite monad_bind_id'
  | instantiate (1 := λ x, _ (inc_map (@VS _) x)); destruct x;
    symmetry; apply monad_map_id'
  | instantiate (1 := λ x, _ (inc_map (@VS _) x)); destruct x;
    symmetry; apply monad_map_id'
  ].
apply (rel_e_open_rule He).
unfold rel_g; iintro x; destruct x as [ | x ]; simpl.
+ unfold rel_g in Hγ; ispecialize Hγ (@VZ (inc V)).
  apply Hγ.
+ unfold rel_g in Hγ; ispecialize Hγ (VS (VS x)).
  apply Hγ.
Qed.