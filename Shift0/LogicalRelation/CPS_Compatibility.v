Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Types.
Require Import Shift0.Target.Lang.
Require Import Shift0.CPS.
Require Import Shift0.LogicalRelation.Main.
Require Import Shift0.LogicalRelation.ECtxReduction.
Require Import Shift0.LogicalRelation.Auto.

Lemma compat_cps_app {V : Set} (Γ₁ Γ₂ : env V)
  (f₁ f₂ e₁ e₂ : exp V)
  (σ₁ σ₂ τ₁ τ₂ : typ)
  (U₁₁ U₁₂ U₂₁ U₂₂ U₃₁ U₃₂ U₄₁ U₄₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ f₁ f₂ 
      ((σ₁ ↦ τ₁ ![ U₄₁ ] U₃₁) ![ U₂₁ ] U₁₁)
      ((σ₂ ↦ τ₂ ![ U₄₂ ] U₃₂) ![ U₂₂ ] U₁₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (σ₁ ![ U₃₁ ] U₂₁) (σ₂ ![ U₃₂ ] U₂₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_app f₁ e₁) (cps_app f₂ e₂) 
      (τ₁ ![ U₄₁ ] U₁₁) (τ₂ ![ U₄₂ ] U₁₂)).
Proof.
intros Hf He; unfold cps_app.
apply compat_kabs.
eapply compat_kapp.
  apply compat_weaken; apply Hf.
apply compat_abs_v; eapply compat_kapp.
  apply compat_weaken; apply compat_weaken; apply He.
apply compat_abs_v; eapply compat_kapp.
  eapply compat_app; apply_compat_var.
apply_compat_var.
Qed.

Lemma compat_cps_app_l {V : Set} (Γ₁ Γ₂ : env V)
  (f₁ f₂ e₁ e₂ : exp V)
  (σ₁ σ₂ τ₁ : typ) (T₂ : typ_eff)
  (U₁ U₂ U₃ U₄ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ f₁ f₂ ((σ₁ ↦ τ₁ ![ U₄ ] U₃) ![ U₂ ] U₁) (σ₂ ↦ T₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (σ₁ ![ U₃ ] U₂) (σ₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_app f₁ e₁) (exp_app f₂ e₂) (τ₁ ![ U₄ ] U₁) T₂).
Proof.
intros Hf He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct T₂ as [ τ₂ | τ₂ T₂ S₂ ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as k HE; idestruct HE as E HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁' [ Hk [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  later_shift; apply rel_ve_fix_unroll in HkE; apply rel_k_fix_unroll in HE'.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?f₁ ?k₁) ~ᵢ cplug _ (exp_app ?f₂ ?e₂) ] =>
    change (n ⊨  cplug (cctx_app1 E₁' k₁) f₁
             ~ᵢ cplug (cctx_append E₂' (cctx_app1 E e₂)) f₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hf); assumption.
  clear f₁ f₂ Hf.
  unfold_rel_k;
    [ | | econstructor 2 | econstructor 2 | | iintro; eassumption ]; 
    logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ HE₁' Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro f₁; iintro f₂; iintro Hf; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro; rewrite <- cplug_cctx_append.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ ?k₁) ~ᵢ cplug _ (exp_app ?f₂ ?e₂) ] =>
    change (n ⊨ cplug (cctx_app1 E₁ k₁) e₁
             ~ᵢ cplug (cctx_append E₂ (cctx_app2 f₂ E)) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k;
    [ | | econstructor 2 | econstructor 2 | | iintro; eassumption ];
    logrel_auto.
  clear E₁ E₂ HE; iintro; unfold_rel_ve.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  change (n ⊨ cplug (cctx_app1 E₁ k) (exp_app f₁ v₁) 
           ~ᵢ cplug E₂ (cplug E (exp_app f₂ v₂))).
  rewrite <- cplug_cctx_append.
  eapply rel_e_rule.
    apply (rel_v_arrow_rule Hf Hv).
  unfold_rel_k;
    [ | | econstructor 2 | econstructor 2 
    | iintro; eassumption | iintro; eassumption ]; logrel_auto.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; idestruct HE as k₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ₁ ↦ U₄) (τ₂ ↦ T₂) k₁ k₂) in Hk.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?f₁ ?k₁) 
          ~ᵢ cplug _ (exp_app (exp_app ?f₂ ?e₂) ?k₂) ] =>
    change (n ⊨ 
      cplug (cctx_app1 E₁' k₁) f₁
      ~ᵢ cplug (cctx_append E₂' (cctx_app1 (cctx_app1 cctx_hole k₂) e₂)) f₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hf); assumption.
  clear f₁ f₂ Hf.
  unfold_rel_k;
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ];
    logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro f₁; iintro f₂; iintro Hf; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ ?k₁) 
          ~ᵢ cplug _ (exp_app (exp_app ?f₂ ?e₂) ?k₂) ] =>
    change (n ⊨ cplug (cctx_app1 E₁ k₁) e₁
      ~ᵢ cplug (cctx_append E₂ (cctx_app2 f₂ (cctx_app1 cctx_hole k₂))) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k;
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ];
    logrel_auto.
  clear E₁ E₂ HE; iintro; unfold_rel_ve.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  change (n ⊨ cplug (cctx_app1 E₁ k₁) (exp_app f₁ v₁)
           ~ᵢ cplug (cctx_app1 E₂ k₂) (exp_app f₂ v₂)).
  eapply rel_e_rule.
    apply (rel_v_arrow_rule Hf Hv).
  unfold_rel_k; [ econstructor 2 | econstructor 2 | | ]; assumption.
Qed.

Lemma compat_cps_app_r {V : Set} (Γ₁ Γ₂ : env V)
  (f₁ f₂ e₁ e₂ : exp V)
  (σ₁ σ₂ : typ) (T₁ : typ_eff) (τ₂ : typ)
  (U₁ U₂ U₃ U₄ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ f₁ f₂ (σ₁ ↦ T₁)ε ((σ₂ ↦ τ₂ ![ U₄ ] U₃) ![ U₂ ] U₁)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (σ₁)ε (σ₂ ![ U₃ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_app f₁ e₁) (cps_app f₂ e₂) T₁ (τ₂ ![ U₄ ] U₁)).
Proof.
intros Hf He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct T₁ as [ τ₁ | τ₁ T₁ S₁ ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as E HE; idestruct HE as k HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as HEk HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?f₁ ?e₁) ~ᵢ cplug _ (exp_app ?f₂ ?k₂) ] =>
    change (n ⊨ cplug (cctx_append E₁' (cctx_app1 E e₁)) f₁ 
             ~ᵢ cplug (cctx_app1 E₂' k₂) f₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hf); assumption.
  clear f₁ f₂ Hf.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro f₁; iintro f₂; iintro Hf; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  rewrite <- cplug_cctx_append.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?f₁ ?e₁) ~ᵢ cplug _ (exp_app ?e₂ ?k₂) ] =>
    change (n ⊨ cplug (cctx_append E₁ (cctx_app2 f₁ E)) e₁ 
             ~ᵢ cplug (cctx_app1 E₂ k₂) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ HE; unfold_rel_ev.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  change (n ⊨ cplug E₁ (cplug E (exp_app f₁ v₁)) 
           ~ᵢ cplug (cctx_app1 E₂ k) (exp_app f₂ v₂)).
  rewrite <- cplug_cctx_append.
  eapply rel_e_rule.
    apply (rel_v_arrow_rule Hf Hv).
  unfold_rel_k; try eassumption; constructor 2.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; idestruct HE as k₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ₁ ↦ T₁) (τ₂ ↦ U₄) k₁ k₂) in Hk.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app (exp_app ?f₁ ?e₁) ?k₁) 
          ~ᵢ cplug _ (exp_app ?f₂ ?k₂) ] =>
    change (n ⊨ 
      cplug (cctx_append E₁' (cctx_app1 (cctx_app1 cctx_hole k₁) e₁)) f₁
      ~ᵢ cplug (cctx_app1 E₂' k₂) f₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hf); assumption.
  clear f₁ f₂ Hf.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro f₁; iintro f₂; iintro Hf; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app (exp_app ?f₁ ?e₁) ?k₁) 
          ~ᵢ cplug _ (exp_app ?e₂ ?k₂) ] =>
    change (n ⊨ 
      cplug (cctx_append E₁ (cctx_app2 f₁ (cctx_app1 cctx_hole k₁))) e₁ 
      ~ᵢ cplug (cctx_app1 E₂ k₂) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ HE; unfold_rel_ev.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  change (n ⊨ cplug (cctx_app1 E₁ k₁) (exp_app f₁ v₁)
           ~ᵢ cplug (cctx_app1 E₂ k₂) (exp_app f₂ v₂)).
  eapply rel_e_rule.
    apply (rel_v_arrow_rule Hf Hv).
  unfold_rel_k; [ econstructor 2 | econstructor 2 | | ]; assumption.
Qed.

(* ========================================================================= *)

Lemma compat_cps_reset0 {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁ ![ (τ₁)ε ] T₁) (τ₂ ![ (τ₂)ε ] T₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ 
     (exp_app e₁ (exp_abs (exp_var VZ)))
     (exp_app e₂ (exp_abs (exp_var VZ))) T₁ T₂).
Proof.
intro He; eapply compat_kapp.
  apply He.
apply compat_abs_v; apply_compat_var.
Qed.

(* ========================================================================= *)

Lemma compat_cps_pure_dollar {V : Set} (Γ₁ Γ₂ : env V)
  (k₁ k₂ e₁ e₂ : exp V)
  (τ₁ τ₂ : typ) (T₁ T₂ U₁ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ k₁ k₂ (τ₁ ↦ T₁)ε (τ₂ ↦ T₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁ ![ T₁ ] U₁) (τ₂ ![ T₂ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_pure_dollar k₁ e₁) (cps_pure_dollar k₂ e₂) U₁ U₂).
Proof.
intros Hk He; unfold cps_pure_dollar.
eapply compat_app.
  apply compat_abs; eapply compat_kapp.
    apply compat_weaken; apply He.
  apply_compat_var.
apply Hk.
Qed.

Lemma compat_cps_dollar {V : Set} (Γ₁ Γ₂ : env V)
  (k₁ k₂ e₁ e₂ : exp V)
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) 
  (τ₁' τ₂' : typ) (U₁₁ U₁₂ U₂₁ U₂₂ U₃₁ U₃₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ k₁ k₂ 
      ((τ₁ ↦ T₁) ![ U₂₁ ] U₁₁) ((τ₂ ↦ T₂) ![ U₂₂ ] U₁₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂
      (τ₁ ![ T₁ ] τ₁' ![ U₃₁ ] U₂₁) (τ₂ ![ T₂ ] τ₂' ![ U₃₂ ] U₂₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_dollar k₁ e₁) (cps_dollar k₂ e₂)
      (τ₁' ![ U₃₁ ] U₁₁) (τ₂' ![ U₃₂ ] U₁₂)).
Proof.
intros Hk He; unfold cps_dollar.
apply compat_kabs; eapply compat_kapp.
  apply compat_weaken; apply Hk.
apply compat_abs_v; eapply compat_kapp; [ | apply_compat_var ].
eapply compat_kapp; [ | apply_compat_var ].
apply compat_weaken; apply compat_weaken; apply He.
Qed.

Lemma compat_cps_dollar_l {V : Set} (Γ₁ Γ₂ : env V)
  (k₁ k₂ e₁ e₂ : exp V)
  (τ₁ τ₂ : typ) (T₁ T₂ : typ_eff) (τ₁' : typ) (U₁ U₂ U₃ U : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ k₁ k₂ ((τ₁ ↦ T₁) ![ U₂ ] U₁) (τ₂ ↦ T₂)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁ ![ T₁ ] τ₁' ![ U₃ ] U₂) (τ₂ ![ T₂ ] U)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ 
     (cps_dollar k₁ e₁) (cps_pure_dollar k₂ e₂) (τ₁' ![ U₃ ] U₁) U).
Proof.
intros Hk He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct U as [ τ₂' | τ₂' S U ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as κ HE; idestruct HE as E HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁' [ Hκ [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  later_shift; apply rel_ve_fix_unroll in HkE; apply rel_k_fix_unroll in HE'.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?k₁ ?f₁) ~ᵢ cplug _ (exp_app ?e₂ ?k₂)] =>
    change (n ⊨ cplug (cctx_app1 E₁' f₁) k₁
             ~ᵢ cplug (cctx_append E₂' (cctx_app2 e₂ E)) k₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hk); assumption.
  clear k₁ k₂ Hk.
  unfold_rel_k; 
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ];
    logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ HE₁' Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro k₁; iintro k₂; iintro Hk; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  simpl; rewrite <- cplug_cctx_append.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ _) ~ᵢ ?e₂ ] =>
    change (n ⊨ cplug (cctx_app1 E₁ κ) e₁ ~ᵢ e₂)
  end.
  eapply rel_e_rule.
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule He); assumption.
  unfold_rel_k; [ | | constructor 2 | constructor 2 | | ]; logrel_auto;
    iintro; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as κ₁ HE; idestruct HE as κ₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hκ HE'.
  change (n ⊨ rel_v (τ₁' ↦ U₃) (τ₂' ↦ S) κ₁ κ₂) in Hκ.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?k₁ ?f₁)
          ~ᵢ cplug _ (exp_app (exp_app ?e₂ ?k₂) _) ] =>
    change (n ⊨ cplug (cctx_app1 E₁' f₁) k₁
      ~ᵢ cplug (cctx_append E₂' (cctx_app2 e₂ (cctx_app1 cctx_hole κ₂))) k₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hk); assumption.
  clear k₁ k₂ Hk.
  unfold_rel_k; 
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ];
    logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro k₁; iintro k₂; iintro Hk; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  match goal with
  | [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app ?e₂ _) ] =>
      change (n ⊨ e₁ ~ᵢ cplug (cctx_app1 E₂ κ₂) e₂)
  end.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro; eapply rel_e_rule; [ | eassumption ].
  eapply compat_kapp_cl; [ | eassumption ].
  eapply compat_kapp_cl; [ | eassumption ].
  apply (rel_e_open_rule He); assumption.
Qed.

Lemma compat_cps_dollar_r {V : Set} (Γ₁ Γ₂ : env V)
  (k₁ k₂ e₁ e₂ : exp V)
  (τ₁ τ₂ : typ) (T₁ T₂ U : typ_eff) (τ₂' : typ) (U₁ U₂ U₃ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ k₁ k₂ (τ₁ ↦ T₁)ε ((τ₂ ↦ T₂) ![ U₂ ] U₁)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (τ₁ ![ T₁ ] U) (τ₂ ![ T₂ ] τ₂' ![ U₃ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ 
     (cps_pure_dollar k₁ e₁) (cps_dollar k₂ e₂) U (τ₂' ![ U₃ ] U₁)).
Proof.
intros Hk He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct U as [ τ₁' | τ₁' S U ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as E HE; idestruct HE as κ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as HEk HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ ?k₁) ~ᵢ cplug _ (exp_app ?k₂ ?f₂) ] =>
    change (n ⊨ cplug (cctx_append E₁' (cctx_app2 e₁ E)) k₁
             ~ᵢ cplug (cctx_app1 E₂' f₂) k₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hk); assumption.
  clear k₁ k₂ Hk.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro k₁; iintro k₂; iintro Hk; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE.
  simpl; rewrite <- cplug_cctx_append.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro.
  match goal with
  | [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app ?e₂ _) ] =>
    change (n ⊨ e₁ ~ᵢ cplug (cctx_app1 E₂ κ) e₂)
  end.
  eapply rel_e_rule.
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule He); assumption.
  unfold_rel_k; [ econstructor 2 | econstructor 2 | | ]; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as κ₁ HE; idestruct HE as κ₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hκ HE'.
  change (n ⊨ rel_v (τ₁' ↦ S) (τ₂' ↦ U₃) κ₁ κ₂) in Hκ.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app (exp_app ?e₁ ?k₁) _) 
          ~ᵢ cplug _ (exp_app ?k₂ ?f₂) ] =>
    change (n ⊨ 
      cplug (cctx_append E₁' (cctx_app2 e₁ (cctx_app1 cctx_hole κ₁))) k₁
      ~ᵢ cplug (cctx_app1 E₂' f₂) k₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule Hk); assumption.
  clear k₁ k₂ Hk.
  unfold_rel_k; [ econstructor 2 | econstructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro k₁; iintro k₂; iintro Hk; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ _) ~ᵢ ?e₂ ] =>
      change (n ⊨ cplug (cctx_app1 E₁ κ₁) e₁ ~ᵢ e₂)
  end.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
  iintro; eapply rel_e_rule; [ | eassumption ].
  eapply compat_kapp_cl; [ | eassumption ].
  eapply compat_kapp_cl; [ | eassumption ].
  apply (rel_e_open_rule He); assumption.
Qed.

(* ========================================================================= *)

Lemma compat_cps_succ {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (T₁ T₂ U₁ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ ![ T₁ ] U₁) (ℕ ![ T₂ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_succ e₁) (cps_succ e₂) 
      (ℕ ![ T₁ ] U₁) (ℕ ![ T₂ ] U₂)).
Proof.
intro He; unfold cps_succ.
apply compat_kabs; eapply compat_kapp.
  apply compat_weaken; apply He.
apply compat_abs_v; eapply compat_app.
  apply_compat_var.
apply compat_succ; apply_compat_var.
Qed.

Lemma compat_cps_succ_l {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (T₁ U₁ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ ![ T₁ ] U₁) (ℕ)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_succ e₁) (exp_succ e₂) (ℕ ![ T₁ ] U₁) (ℕ)ε).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
simpl in HE.
idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
destruct HE₀ as [ HE₁ HE₂ ].
idestruct HE as T HE; idestruct HE as k HE; idestruct HE as E HE.
idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁' [ Hk [ Hι₁ Hι₂ ]]].
idestruct HE as HkE HE'.
change (n ⊨ ▷rel_ve_fix ℕ T₁ ℕ T k E) in HkE.
eapply obs_ectx_iota_rt_r; [ eassumption | ].
eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
later_shift; apply rel_ve_fix_unroll in HkE; apply rel_k_fix_unroll in HE'.
match goal with
| [ |- _ ⊨  cplug ?E₁ (exp_app ?e₁ ?k₁) ~ᵢ cplug _ (exp_succ ?e₂) ] =>
    change (n ⊨  cplug (cctx_app1 E₁ k₁) e₁
             ~ᵢ cplug (cctx_append E₂' (cctx_succ E)) e₂)
  end.
eapply rel_e_rule.
  apply (rel_e_open_rule He); assumption.
unfold_rel_k;
   [ | | econstructor 2 | constructor 2 | | iintro; eassumption ];
   logrel_auto.
iintro; clear E₁ E₂ E₁' E₂' HE₁ HE₂ HE₁' Hι₁ Hι₂ HE'; unfold_rel_ve.
iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_beta_l.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
iintro; rewrite <- cplug_cctx_append.
match goal with
| [ |- _ ⊨ cplug _ (exp_app _ ?e₁) ~ᵢ ?e₂ ] =>
  change (n ⊨ cplug (cctx_app2 k E₁) e₁ ~ᵢ e₂)
end.
eapply rel_e_rule.
  apply compat_succ_cl; apply rel_v_in_rel_e; assumption.
simpl; logrel_auto.
iintro a₁; iintro a₂; iintro Ha.
rewrite cplug_cctx_append; eapply rel_e_rule; [ | eassumption ].
apply (rel_ve_rule HkE); apply Ha.
Qed.

Lemma compat_cps_succ_r {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ : exp V)
  (T₂ U₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ)ε (ℕ ![ T₂ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_succ e₁) (cps_succ e₂) (ℕ)ε (ℕ ![ T₂ ] U₂)).
Proof.
intro He; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
simpl in HE.
idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
destruct HE₀ as [ HE₁ HE₂ ].
idestruct HE as T HE; idestruct HE as E HE; idestruct HE as k HE.
idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
idestruct HE as HEk HE'.
change (n ⊨ rel_ev ℕ T ℕ T₂ E k) in HEk.
eapply obs_ectx_iota_rt_r; [ eassumption | ].
eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
match goal with
| [ |- _ ⊨ cplug _ (exp_succ ?e₁) ~ᵢ cplug ?E₂ (exp_app ?e₂ ?k₂) ] =>
    change (n ⊨  cplug (cctx_append E₁' (cctx_succ E)) e₁
             ~ᵢ cplug (cctx_app1 E₂ k₂) e₂)
  end.
eapply rel_e_rule.
  apply (rel_e_open_rule He); assumption.
unfold_rel_k; [ econstructor 2 | constructor 2 | | eassumption ].
clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
eapply obs_beta_r.
  apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
simpl; repeat rewrite bind_lift_weaken; repeat erewrite subst_weaken.
rewrite <- cplug_cctx_append.
match goal with
| [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app _ ?e₂) ] =>
  change (n ⊨ e₁ ~ᵢ cplug (cctx_app2 k E₂) e₂)
end.
eapply rel_e_rule.
  apply compat_succ_cl; apply rel_v_in_rel_e; assumption.
simpl; logrel_auto.
iintro a₁; iintro a₂; iintro Ha.
rewrite cplug_cctx_append; eapply rel_e_rule; [ | eassumption ].
apply (rel_ev_rule HEk); apply Ha.
Qed.

(* ========================================================================= *)

Lemma compat_cps_case {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ z₁ z₂ : exp V)
  (s₁ s₂ : exp (inc V))
  (τ₁ τ₂ : typ) (U₁₁ U₁₂ U₂₁ U₂₂ U₃₁ U₃₂ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ ![ U₂₁ ] U₁₁) (ℕ ![ U₂₂ ] U₁₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ z₁ z₂ (τ₁ ![ U₃₁ ] U₂₁) (τ₂ ![ U₃₂ ] U₂₂)) →
  (n ⊨ rel_e_open (Γ₁ / ℕ) (Γ₂ / ℕ) s₁ s₂ 
      (τ₁ ![ U₃₁ ] U₂₁) (τ₂ ![ U₃₂ ] U₂₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_case e₁ z₁ s₁) (cps_case e₂ z₂ s₂)
      (τ₁ ![ U₃₁ ] U₁₁) (τ₂ ![ U₃₂ ] U₁₂)).
Proof.
intros He Hz Hs; unfold cps_case.
apply compat_kabs; eapply compat_kapp.
  apply compat_weaken; apply He.
apply compat_abs_v; apply compat_case.
+ apply_compat_var.
+ eapply compat_kapp; [ | apply_compat_var ].
  apply compat_weaken; apply compat_weaken; apply Hz.
+ eapply compat_kapp; [ | apply_compat_var ].
  apply compat_weaken1; apply compat_weaken1; apply Hs.
Qed.

Lemma compat_cps_case_l {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ z₁ z₂ : exp V)
  (s₁ s₂ : exp (inc V))
  (τ₁ : typ) (T₂ U₁ U₂ U₃ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ ![ U₂ ] U₁) (ℕ)ε) →
  (n ⊨ rel_e_open Γ₁ Γ₂ z₁ z₂ (τ₁ ![ U₃ ] U₂) T₂) →
  (n ⊨ rel_e_open (Γ₁ / ℕ) (Γ₂ / ℕ) s₁ s₂ (τ₁ ![ U₃ ] U₂) T₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (cps_case e₁ z₁ s₁) (exp_case e₂ z₂ s₂) 
      (τ₁ ![ U₃ ] U₁) T₂).
Proof.
intros He Hz Hs; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct T₂ as [ τ₂ | τ₂ S U ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as k HE; idestruct HE as E HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ HE₁' [ Hk [ Hι₁ Hι₂ ]]].
  idestruct HE as HkE HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  later_shift; simpl.
  repeat rewrite bind_lift_weaken; repeat rewrite bind_lift_lift_weaken1.
  repeat erewrite subst_weaken; repeat rewrite subst_weaken1.
  apply rel_ve_fix_unroll in HkE; apply rel_k_fix_unroll in HE'.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ ?k₁) ~ᵢ cplug _ (exp_case ?e₂ ?z₂ ?s₂) ] =>
    change (n ⊨ cplug (cctx_app1 E₁' k₁) e₁ 
             ~ᵢ cplug (cctx_append E₂' (cctx_case E z₂ s₂)) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k;
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ]; logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ HE₁' Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl.
  repeat rewrite bind_lift_weaken.
  repeat rewrite subst_weaken; repeat rewrite subst_weaken1.
  rewrite <- cplug_cctx_append; iintro.
  simpl in Hv; apply I_Prop_elim in Hv; inversion Hv as [ m ]; destruct m.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    iintro.
    match goal with
    | [ |- _ ⊨ cplug _ (exp_app ?e₁ _) ~ᵢ ?e₂] =>
      change (n ⊨ cplug (cctx_app1 E₁ k) e₁ ~ᵢ e₂)
    end.
    eapply rel_e_rule.
      apply (rel_e_open_rule Hz); assumption.
    unfold_rel_k; [ | | constructor 2 | constructor 2 | | ];
      logrel_auto; iintro; eassumption.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    simpl; repeat rewrite subst_bind_lift; repeat rewrite subst_weaken.
    iintro.
    match goal with
    | [ |- _ ⊨ cplug _ (exp_app ?e₁ _) ~ᵢ ?e₂ ] =>
      change (n ⊨ cplug (cctx_app1 E₁ k) e₁ ~ᵢ e₂)
    end.
    eapply rel_e_rule.
      apply (rel_e_open_rule Hs).
      unfold rel_g; iintro x; destruct x; simpl.
      iintro; constructor.
      apply rel_g_rule; assumption.
    unfold_rel_k; [ | | constructor 2 | constructor 2 | | ]; 
      logrel_auto; iintro; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; idestruct HE as k₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ₁ ↦ U₃) (τ₂ ↦ S) k₁ k₂) in Hk.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; iintro.
  repeat rewrite bind_lift_weaken; repeat rewrite bind_lift_lift_weaken1.
  repeat erewrite subst_weaken; repeat rewrite subst_weaken1.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app ?e₁ ?k₁) 
          ~ᵢ cplug _ (exp_app (exp_case ?e₂ ?z₂ ?s₂) ?k₂) ] =>
    change (n ⊨ cplug (cctx_app1 E₁' k₁) e₁
      ~ᵢ cplug (cctx_append E₂' (cctx_case (cctx_app1 cctx_hole k₂) z₂ s₂)) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k; 
    [ | | constructor 2 | constructor 2 | | iintro; eassumption ]; logrel_auto.
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; iintro; unfold_rel_ve.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  eapply obs_beta_l.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl; iintro.
  repeat rewrite bind_lift_weaken.
  repeat rewrite subst_weaken; repeat rewrite subst_weaken1.
  match goal with
  [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app ?e₂ _) ] =>
    change (n ⊨ e₁ ~ᵢ cplug (cctx_app1 E₂ k₂) e₂)
  end.
  simpl in Hv; apply I_Prop_elim in Hv; inversion Hv as [ m ]; destruct m.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    iintro; simpl.
    eapply rel_e_rule; [ | eassumption ].
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule Hz); assumption.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    iintro; simpl.
    repeat rewrite subst_bind_lift; rewrite subst_weaken.
    eapply rel_e_rule; [ | eassumption ].
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule Hs).
    unfold rel_g; iintro x; destruct x; simpl.
      iintro; constructor.
    apply rel_g_rule; assumption.
Qed.

Lemma compat_cps_case_r {V : Set} (Γ₁ Γ₂ : env V)
  (e₁ e₂ z₁ z₂ : exp V)
  (s₁ s₂ : exp (inc V))
  (T₁ : typ_eff) (τ₂ : typ) (U₁ U₂ U₃ : typ_eff)
  (n : nat) :
  (n ⊨ rel_e_open Γ₁ Γ₂ e₁ e₂ (ℕ)ε (ℕ ![ U₂ ] U₁)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ z₁ z₂ T₁ (τ₂ ![ U₃ ] U₂)) →
  (n ⊨ rel_e_open (Γ₁ / ℕ) (Γ₂ / ℕ) s₁ s₂ T₁ (τ₂ ![ U₃ ] U₂)) →
  (n ⊨ rel_e_open Γ₁ Γ₂ (exp_case e₁ z₁ s₁) (cps_case e₂ z₂ s₂) 
      T₁ (τ₂ ![ U₃ ] U₁)).
Proof.
intros He Hz Hs; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ; unfold_rel_e.
iintro E₁; iintro E₂; iintro HE.
destruct T₁ as [ τ₁ | τ₁ S U ].
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as T HE; idestruct HE as E HE; idestruct HE as k HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as HEk HE'.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl.
  repeat rewrite bind_lift_weaken; repeat rewrite bind_lift_lift_weaken1.
  repeat erewrite subst_weaken; repeat rewrite subst_weaken1.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_case ?e₁ ?z₁ ?s₁) ~ᵢ cplug _ (exp_app ?e₂ ?k₂) ] =>
    change (n ⊨ cplug (cctx_append E₁' (cctx_case E z₁ s₁)) e₁ 
             ~ᵢ cplug (cctx_app1 E₂' k₂) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl.
  repeat rewrite bind_lift_weaken.
  repeat rewrite subst_weaken; repeat rewrite subst_weaken1.
  rewrite <- cplug_cctx_append.
  simpl in Hv; apply I_Prop_elim in Hv; inversion Hv as [ m ]; destruct m.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    iintro.
    match goal with
    | [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app ?e₂ _) ] =>
      change (n ⊨ e₁ ~ᵢ cplug (cctx_app1 E₂ k) e₂)
    end.
    eapply rel_e_rule.
      apply (rel_e_open_rule Hz); assumption.
    unfold_rel_k; [ constructor 2 | constructor 2 | | ]; eassumption.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    simpl; repeat rewrite subst_bind_lift; repeat rewrite subst_weaken.
    iintro.
    match goal with
    | [ |- _ ⊨ ?e₁ ~ᵢ cplug _ (exp_app ?e₂ _) ] =>
      change (n ⊨ e₁ ~ᵢ cplug (cctx_app1 E₂ k) e₂)
    end.
    eapply rel_e_rule.
      apply (rel_e_open_rule Hs).
      unfold rel_g; iintro x; destruct x; simpl.
      iintro; constructor.
      apply rel_g_rule; assumption.
    unfold_rel_k; [ constructor 2 | constructor 2 | | ]; eassumption.
+ simpl in HE.
  idestruct HE as HE₀ HE; apply I_Prop_elim in HE₀.
  destruct HE₀ as [ HE₁ HE₂ ].
  idestruct HE as k₁ HE; idestruct HE as k₂ HE.
  idestruct HE as E₁' HE; idestruct HE as E₂' HE; idestruct HE as HE₀ HE.
  apply I_Prop_elim in HE₀; destruct HE₀ as [ Hι₁ Hι₂ ].
  idestruct HE as Hk HE'.
  change (n ⊨ rel_v (τ₁ ↦ S) (τ₂ ↦ U₃) k₁ k₂) in Hk.
  eapply obs_ectx_iota_rt_r; [ eassumption | ].
  eapply obs_ectx_iota_rt_l; [ eassumption | ]; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl.
  repeat rewrite bind_lift_weaken; repeat rewrite bind_lift_lift_weaken1.
  repeat erewrite subst_weaken; repeat rewrite subst_weaken1.
  match goal with
  | [ |- _ ⊨ cplug _ (exp_app (exp_case ?e₁ ?z₁ ?s₁) ?k₁) 
          ~ᵢ cplug _ (exp_app ?e₂ ?k₂) ] =>
    change (n ⊨ 
      cplug (cctx_append E₁' (cctx_case (cctx_app1 cctx_hole k₁) z₁ s₁)) e₁
      ~ᵢ cplug (cctx_app1 E₂' k₂) e₂)
  end.
  eapply rel_e_rule.
    apply (rel_e_open_rule He); assumption.
  clear e₁ e₂ He.
  unfold_rel_k; [ constructor 2 | constructor 2 | | eassumption ].
  clear E₁ E₂ E₁' E₂' HE₁ HE₂ Hι₁ Hι₂ HE'; unfold_rel_ev.
  iintro v₁; iintro v₂; iintro Hv; unfold_rel_e.
  iintro E₁; iintro E₂; iintro HE; simpl.
  eapply obs_beta_r.
    apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
  simpl.
  repeat rewrite bind_lift_weaken.
  repeat rewrite subst_weaken; repeat rewrite subst_weaken1.
  match goal with
  [ |- _ ⊨ cplug _ (exp_app ?e₁ _) ~ᵢ ?e₂ ] =>
    change (n ⊨ cplug (cctx_app1 E₁ k₁) e₁ ~ᵢ e₂)
  end.
  simpl in Hv; apply I_Prop_elim in Hv; inversion Hv as [ m ]; destruct m.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; logrel_auto.
    iintro; simpl.
    eapply rel_e_rule; [ | eassumption ].
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule Hz); assumption.
  - eapply obs_beta_r.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    eapply obs_beta_l.
      apply beta_step_in_ectx; [ | econstructor ]; trivial; logrel_auto.
    iintro; simpl.
    repeat rewrite subst_bind_lift; rewrite subst_weaken.
    eapply rel_e_rule; [ | eassumption ].
    eapply compat_kapp_cl; [ | eassumption ].
    apply (rel_e_open_rule Hs).
    unfold rel_g; iintro x; destruct x; simpl.
      iintro; constructor.
    apply rel_g_rule; assumption.
Qed.