Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.Target.ContextualApproximation.
Require Import Shift0.LogicalRelation.Relation.
Require Import Shift0.LogicalRelation.Auto.
Require Import Shift0.LogicalRelation.DerivedRules.
Require Import Shift0.LogicalRelation.Compatibility.
Require Import Shift0.LogicalRelation.Coercion.

Lemma fundamental_property_aux {V : Set} (Γ : env V) (e : exp V) T :
  Γ ⊩ e ∷ T → 
  (⊨ rel_e_open Γ Γ e e T T) ∧
  (open_value e → ∀ τ, T = (τ)ε → ⊨ rel_v_open Γ Γ e e τ τ).
Proof.
induction 1; simpl; split.
+ intro n; apply compat_var.
+ intros Hv τ Hτ n.
  injection Hτ; intro; subst.
  apply compat_var_v.
+ intro n; apply compat_const.
+ intros Hv τ Hτ n.
  injection Hτ; intro; subst.
  apply compat_const_v.
+ intro n; apply compat_abs; apply (proj1 IHtyping).
+ intros Hv σ Hσ n.
  injection Hσ; intro; subst.
  apply compat_abs_v; apply (proj1 IHtyping).
+ intro n; eapply compat_app; 
  [ apply (proj1 IHtyping1) | apply (proj1 IHtyping2) ].
+ intro Hv; inversion Hv.
+ intro n; apply compat_kabs; apply (proj1 IHtyping).
+ intros Hv σ Hσ; discriminate Hσ.
+ intro n; eapply compat_kapp; [ apply (proj1 IHtyping1) | ].
  apply (proj2 IHtyping2); trivial.
+ intro Hv; inversion Hv.
+ intro n.
  eapply rel_coercion_l; [ eassumption | ].
  eapply rel_coercion_r; [ eassumption | ].
  apply (proj1 IHtyping).
+ intros Hv τ Hτ n; inversion Hv.
  - subst; inversion H; subst.
    eapply rel_v_coercion_arrow_l; [ eassumption | eassumption | ].
    eapply rel_v_coercion_arrow_r; [ eassumption | eassumption | ].
    apply (proj2 IHtyping); trivial.
  - subst; inversion H.
  - subst; inversion H.
+ intro n; apply compat_fix; apply (proj1 IHtyping).
+ intros Hv σ Hσ n.
  injection Hσ; intro; subst.
  apply compat_fix_v; apply (proj1 IHtyping).
+ intro n; apply compat_succ; apply (proj1 IHtyping).
+ intro Hv; inversion Hv.
+ intro n; apply compat_case;
  [ apply (proj1 IHtyping1) 
  | apply (proj1 IHtyping2) 
  | apply (proj1 IHtyping3) ].
+ intro Hv; inversion Hv.
Qed.

Theorem fundamental_property {V : Set} (Γ : env V) (e : exp V) T :
  Γ ⊩ e ∷ T → (⊨ rel_e_open Γ Γ e e T T).
Proof.
intro H; apply (proj1 (fundamental_property_aux Γ e T H)).
Qed.

Theorem fundamental_property_v {V : Set} (Γ : env V) (v : exp V) τ :
  open_value v → Γ ⊩ v ∷ (τ)ε → (⊨ rel_v_open Γ Γ v v τ τ).
Proof.
intros Hv H; apply (proj2 (fundamental_property_aux Γ v (τ)ε H)); trivial.
Qed.

Lemma rel_context_cl {V : Set} (C : ctx V) Γ T U :
  C ∷[ Γ ⊢ T ]↝ U →
  ∀ e₁ e₂ n, (n ⊨ rel_e_open Γ Γ e₁ e₂ T T) → 
    (n ⊨ rel_e U U (plug C e₁) (plug C e₂)).
Proof.
induction 1; intros f₁ f₂ n Hf; simpl.
+ apply rel_e_open_cl; assumption.
+ apply IHctx_typing; apply compat_abs; assumption.
+ apply IHctx_typing; eapply compat_app;
  [ eassumption | apply fundamental_property; assumption ].
+ apply IHctx_typing; eapply compat_app;
  [ apply fundamental_property; eassumption | assumption ].
+ apply IHctx_typing; eapply compat_kabs; assumption.
+ apply IHctx_typing; eapply compat_kapp;
  [ eassumption | apply fundamental_property_v; assumption ].
+ apply IHctx_typing.
  eapply rel_coercion_l; [ eassumption | ].
  eapply rel_coercion_r; [ eassumption | ].
  assumption.
+ apply IHctx_typing; apply compat_fix; assumption.
+ apply IHctx_typing; apply compat_succ; assumption.
+ apply IHctx_typing; apply compat_case; [ eassumption | | ];
    apply fundamental_property; assumption.
+ apply IHctx_typing; apply compat_case; [ | eassumption | ];
    apply fundamental_property; assumption.
+ apply IHctx_typing; apply compat_case; [ | | eassumption ];
    apply fundamental_property; assumption.
Qed.

Lemma rel_e_is_obs (e₁ e₂ : exp0) τ :
  ⊨ rel_e (τ)ε (τ)ε e₁ e₂ ⇒ e₁ ~ᵢ e₂.
Proof.
intro n; iintro H.
change (n ⊨ cplug cctx_hole e₁ ~ᵢ cplug cctx_hole e₂).
eapply rel_e_rule; [ eassumption | ].
simpl; logrel_auto.
iintro v₁; iintro v₂; iintro Hv; simpl.
destruct (rel_v_relates_values Hv) as [ Hv₁ Hv₂ ].
apply I_valid_intro; simpl; intro; constructor 1; assumption.
Qed.

Theorem relation_soundness {V : Set} (Γ : env V) e₁ e₂ T :
  (⊨ rel_e_open Γ Γ e₁ e₂ T T) → ctx_approx Γ e₁ e₂ T.
Proof.
intros H C ρ HC.
apply obs_limit; intro n.
iapply (rel_e_is_obs (plug C e₁) (plug C e₂) ρ n).
eapply rel_context_cl; eauto.
Qed.