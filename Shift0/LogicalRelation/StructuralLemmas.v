Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Target.Lang.
Require Import Shift0.LogicalRelation.Relation.

Lemma rel_v_relates_values {τ₁ τ₂ : typ} {v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) → value v₁ ∧ value v₂.
Proof.
intro Hv; destruct τ₁ as [ | τ₁ T₁]; destruct τ₂ as [ | τ₂ T₂ ]; simpl in Hv.
+ apply I_Prop_elim in Hv; inversion Hv; split; constructor.
+ apply I_Prop_elim in Hv; destruct Hv.
+ apply I_Prop_elim in Hv; destruct Hv.
+ apply I_conj_elim1 in Hv; apply I_Prop_elim in Hv.
  split; apply Hv.
Qed.

Lemma rel_k_relates_ectx {T₁ T₂ : typ_eff} {E₁ E₂ : cctx} {n : nat} :
  (n ⊨ rel_k T₁ T₂ E₁ E₂) → ectx E₁ ∧ ectx E₂.
Proof.
destruct T₁ as [ τ₁ | τ₁ T₁ U₁ ]; destruct T₂ as [ τ₂ | τ₂ T₂ U₂ ]; simpl;
  intro H; eapply I_Prop_elim; eapply I_conj_elim1; eassumption.
Qed.

Lemma rel_ev_relates_ectx_and_value {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
     {E : cctx} {k : exp0} {n : nat} :
  (n ⊨ rel_ev τ₁ T₁ τ₂ T₂ E k) → ectx E ∧ value k.
Proof.
intro HkE; unfold grel_ev in HkE.
apply I_conj_elim1 in HkE; apply I_Prop_elim in HkE; assumption.
Qed.

Lemma rel_ve_relates_value_and_ectx {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
     {k : exp0} {E : cctx} {n : nat} :
  (n ⊨ rel_ve τ₁ T₁ τ₂ T₂ k E) → value k ∧ ectx E.
Proof.
intro HkE; unfold grel_ve in HkE.
apply I_conj_elim1 in HkE; apply I_Prop_elim in HkE; assumption.
Qed.

Lemma rel_v_in_rel_e {τ₁ τ₂ : typ} {v₁ v₂ : exp0} {n : nat} :
  (n ⊨ rel_v τ₁ τ₂ v₁ v₂) → (n ⊨ rel_e (τ₁)ε (τ₂)ε v₁ v₂).
Proof.
intro Hv; unfold grel_e.
iintro E₁; iintro E₂; iintro HE.
simpl in HE; apply I_conj_elim2 in HE; iespecialize HE; iapply HE; assumption.
Qed.

Lemma rel_v_open_in_rel_e_open {V : Set}
    {Γ₁ Γ₂ : env V} {v₁ v₂ : exp V} {τ₁ τ₂ : typ} {n : nat} :
  (n ⊨ rel_v_open Γ₁ Γ₂ v₁ v₂ τ₁ τ₂) →
  (n ⊨ rel_e_open Γ₁ Γ₂ v₁ v₂ (τ₁)ε (τ₂)ε).
Proof.
intro Hv; unfold rel_e_open.
iintro γ₁; iintro γ₂; iintro Hγ.
apply rel_v_in_rel_e.
unfold rel_v_open in Hv; iespecialize Hv; iapply Hv; assumption.
Qed.

Lemma rel_e_open_cl {e₁ e₂ : exp0} {T₁ T₂ : typ_eff} {n : nat} :
  (n ⊨ rel_e_open ø ø e₁ e₂ T₁ T₂)
  ↔ (n ⊨ rel_e T₁ T₂ e₁ e₂).
Proof.
split.
+ intro H; unfold rel_e_open in H.
  ispecialize H (@of_empty exp0); ispecialize H (@of_empty exp0).
  rewrite monad_bind_return in H; [ | intro x; destruct x ].
  rewrite monad_bind_return in H; [ | intro x; destruct x ].
  iapply H.
  unfold rel_g; iintro x; destruct x.
+ intro H; unfold rel_e_open.
  iintro γ₁; iintro γ₂; iintro Hγ.
  rewrite monad_bind_return; [ | intro x; destruct x ].
  rewrite monad_bind_return; [ | intro x; destruct x ].
  assumption.
Qed.