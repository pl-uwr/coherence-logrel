Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Types.
Require Import Shift0.Target.Syntax Shift0.Target.SyntaxContext.
Require Import Shift0.Target.ECtxReduction.
Require Export Shift0.LogicalRelation.ObsApproximation.

Definition typ_eff_krsig : list Type := 
  ((typ_eff:Type) :: (typ_eff:Type) :: (cctx:Type) :: (cctx:Type) :: nil)%list.

Inductive rel_nat : exp0 → exp0 → Prop :=
| RelNat : ∀ m, rel_nat (exp_const m) (exp_const m)
.

Definition grel_e (rel : cctx → cctx → IProp) (e₁ e₂ : exp0) : IProp :=
  ∀ᵢ E₁ E₂, rel E₁ E₂ ⇒ cplug E₁ e₁ ~ᵢ cplug E₂ e₂.
Definition grel_ev (rel_arg : exp0 → exp0 → IProp) 
    (rel_cont : cctx → cctx → IProp) E k : IProp :=
  (ectx E ∧ value k)ᵢ ∧ᵢ
  ∀ᵢ v₁ v₂, rel_arg v₁ v₂ ⇒ 
    grel_e rel_cont (cplug E v₁) (exp_app k v₂).
Definition grel_ve (rel_arg : exp0 → exp0 → IProp)
    (rel_cont : cctx → cctx → IProp) k E : IProp :=
  (value k ∧ ectx E)ᵢ ∧ᵢ
  ∀ᵢ v₁ v₂, rel_arg v₁ v₂ ⇒ 
    grel_e rel_cont (exp_app k v₁) (cplug E v₂).

Fixpoint F_rel_v (rel : IRel typ_eff_krsig)
    (τ₁ τ₂ : typ) (v₁ v₂ : exp0) {struct τ₂} : IProp :=
  match τ₁, τ₂ with
  | ℕ,       ℕ       => (rel_nat v₁ v₂)ᵢ
  | ℕ,       _ ↦ _   => (False)ᵢ
  | _ ↦ _,   ℕ       => (False)ᵢ
  | τ₁ ↦ T₁, τ₂ ↦ T₂ => (value v₁ ∧ value v₂)ᵢ ∧ᵢ
      ∀ᵢ a₁ a₂, F_rel_v rel τ₁ τ₂ a₁ a₂ ⇒
        grel_e (F_rel_k rel T₁ T₂) (exp_app v₁ a₁) (exp_app v₂ a₂)
  end
with F_rel_k (rel : IRel typ_eff_krsig) 
    (T₁ T₂ : typ_eff) (E₁ E₂ : cctx) {struct T₂} : IProp :=
  match T₁, T₂ with
  | (τ₁)ε, (τ₂)ε => (ectx E₁ ∧ ectx E₂)ᵢ ∧ᵢ
      ∀ᵢ v₁ v₂, F_rel_v rel τ₁ τ₂ v₁ v₂ ⇒ cplug E₁ v₁ ~ᵢ cplug E₂ v₂
  | (τ₁)ε, τ₂ ![ T₂ ] U₂ => (ectx E₁ ∧ ectx E₂)ᵢ ∧ᵢ
      ∃ᵢ T E k E₁' E₂', 
      (ectx_iota_rt E₁ (cctx_append E₁' E) ∧ 
        ectx_iota_rt E₂ (cctx_app1 E₂' k))ᵢ ∧ᵢ
      grel_ev (F_rel_v rel τ₁ τ₂) (F_rel_k rel T T₂) E k ∧ᵢ
      F_rel_k rel T U₂ E₁' E₂'
  | τ₁ ![ T₁ ] U₁, (τ₂)ε => (ectx E₁ ∧ ectx E₂)ᵢ ∧ᵢ
      ∃ᵢ T k E E₁' E₂',
      (ectx E₁' ∧ value k ∧
        ectx_iota_rt E₁ (cctx_app1 E₁' k) ∧ 
        ectx_iota_rt E₂ (cctx_append E₂' E))ᵢ ∧ᵢ
      ▷grel_ve (F_rel_v rel τ₁ τ₂) (rel T₁ T) k E ∧ᵢ
      ▷rel U₁ T E₁' E₂'
  | τ₁ ![ T₁ ] U₁, τ₂ ![ T₂ ] U₂ => (ectx E₁ ∧ ectx E₂)ᵢ ∧ᵢ
      ∃ᵢ k₁ k₂ E₁' E₂',
      (ectx_iota_rt E₁ (cctx_app1 E₁' k₁) ∧ 
        ectx_iota_rt E₂ (cctx_app1 E₂' k₂))ᵢ ∧ᵢ
      ((value k₁ ∧ value k₂)ᵢ ∧ᵢ ∀ᵢ a₁ a₂, F_rel_v rel τ₁ τ₂ a₁ a₂ ⇒
        grel_e (F_rel_k rel T₁ T₂) (exp_app k₁ a₁) (exp_app k₂ a₂)) ∧ᵢ
      F_rel_k rel U₁ U₂ E₁' E₂'
  end.

Lemma F_rel_k_contractive : contractive typ_eff_krsig F_rel_k.
Proof.
unfold contractive.
intros R₁ R₂ n; iintro HR; simpl.
iintro T₁; iintro T₂; generalize T₁; 
  apply I_forall_elim; generalize T₂; clear T₁ T₂.
apply typ_eff_typ_ind with
  (P := λ τ₂, n ⊨ ∀ᵢ τ₁ v₁ v₂, F_rel_v R₁ τ₁ τ₂ v₁ v₂ ⇔ F_rel_v R₂ τ₁ τ₂ v₁ v₂).
+ iintro τ₁; destruct τ₁ as [ | τ₁ T₁ ]; iintro v₁; iintro v₂; simpl; auto_contr.
+ intros τ₂ IHτ₂ T₂ IHT₂.
  iintro τ₁; destruct τ₁ as [ | τ₁ T₁ ]; iintro v₁; iintro v₂; simpl; auto_contr.
    iespecialize IHτ₂; eassumption.
  unfold grel_e; auto_contr.
    iespecialize IHT₂; eassumption.
+ intros τ₂ IHτ₂.
  iintro T₁; destruct T₁ as [ τ₁ | τ₁ T₁ U₁ ]; iintro E₁; iintro E₂; 
      simpl; auto_contr.
  - iespecialize IHτ₂; eassumption.
  - unfold grel_ve; unfold grel_e; auto_contr.
      iespecialize IHτ₂; eassumption.
    simpl in HR; iespecialize HR; eassumption.
  - simpl in HR; iespecialize HR; eassumption.
+ intros τ₂ IHτ₂ T₂ IHT₂ U₂ IHU₂.
  iintro T₁; destruct T₁ as [ τ₁ | τ₁ T₁ U₁ ]; iintro E₁; iintro E₂; 
      simpl; auto_contr.
  - unfold grel_ev; unfold grel_e; auto_contr.
      iespecialize IHτ₂; eassumption.
    iespecialize IHT₂; eassumption.
  - iespecialize IHU₂; eassumption.
  - iespecialize IHτ₂; eassumption.
  - unfold grel_e; auto_contr.
    iespecialize IHT₂; eassumption.
  - iespecialize IHU₂; eassumption.
Qed.

Notation rel_k_fix :=
  (I_fix typ_eff_krsig F_rel_k F_rel_k_contractive).

Notation rel_v := (F_rel_v rel_k_fix).
Notation rel_k := (F_rel_k rel_k_fix).
Notation rel_e T₁ T₂ := (grel_e (rel_k T₁ T₂)).
Notation rel_e_fix T₁ T₂ := (grel_e (rel_k_fix T₁ T₂)).
Notation rel_ev τ₁ T₁ τ₂ T₂ := (grel_ev (rel_v τ₁ τ₂) (rel_k T₁ T₂)).
Notation rel_ve τ₁ T₁ τ₂ T₂ := (grel_ve (rel_v τ₁ τ₂) (rel_k T₁ T₂)).
Notation rel_ve_fix τ₁ T₁ τ₂ T₂ := (grel_ve (rel_v τ₁ τ₂) (rel_k_fix T₁ T₂)).

Definition rel_g {V : Set} (Γ₁ Γ₂ : env V) (γ₁ γ₂ : V → exp0) : IProp :=
  ∀ᵢ x : V, rel_v (Γ₁ x) (Γ₂ x) (γ₁ x) (γ₂ x).

Definition rel_e_open {V : Set} 
    (Γ₁ Γ₂ : env V) (e₁ e₂ : exp V) (T₁ T₂ : typ_eff) : IProp :=
  ∀ᵢ γ₁ γ₂, rel_g Γ₁ Γ₂ γ₁ γ₂ ⇒ rel_e T₁ T₂ (bind γ₁ e₁) (bind γ₂ e₂).
Definition rel_v_open {V : Set}
    (Γ₁ Γ₂ : env V) (v₁ v₂ : exp V) (τ₁ τ₂ : typ) : IProp :=
  ∀ᵢ γ₁ γ₂, rel_g Γ₁ Γ₂ γ₁ γ₂ ⇒ rel_v τ₁ τ₂ (bind γ₁ v₁) (bind γ₂ v₂).

Lemma rel_k_fix_roll {T₁ T₂ : typ_eff} {E₁ E₂ : cctx} {n : nat} :
  (n ⊨ rel_k T₁ T₂ E₁ E₂) → (n ⊨ rel_k_fix T₁ T₂ E₁ E₂).
Proof.
intro H; apply (I_fix_roll typ_eff_krsig); assumption.
Qed.

Lemma rel_k_fix_unroll {T₁ T₂ : typ_eff} {E₁ E₂ : cctx} {n : nat} :
  (n ⊨ rel_k_fix T₁ T₂ E₁ E₂) → (n ⊨ rel_k T₁ T₂ E₁ E₂).
Proof.
intro H; apply (I_fix_unroll typ_eff_krsig); assumption.
Qed.

Lemma rel_e_fix_roll {T₁ T₂ : typ_eff} {e₁ e₂ : exp0} {n : nat} :
  (n ⊨ rel_e T₁ T₂ e₁ e₂) → (n ⊨ rel_e_fix T₁ T₂ e₁ e₂).
Proof.
unfold grel_e; intro H.
iintro E₁; iintro E₂; iintro HE.
iespecialize H; iapply H.
apply rel_k_fix_unroll; assumption.
Qed.

Lemma rel_e_fix_unroll {T₁ T₂ : typ_eff} {e₁ e₂ : exp0} {n : nat} :
  (n ⊨ rel_e_fix T₁ T₂ e₁ e₂) → (n ⊨ rel_e T₁ T₂ e₁ e₂).
Proof.
unfold grel_e; intro H.
iintro E₁; iintro E₂; iintro HE.
iespecialize H; iapply H.
apply rel_k_fix_roll; assumption.
Qed.

Lemma rel_ve_fix_roll {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
    {k : exp0} {E : cctx} {n : nat} :
  (n ⊨ rel_ve τ₁ T₁ τ₂ T₂ k E) → (n ⊨ rel_ve_fix τ₁ T₁ τ₂ T₂ k E).
Proof.
unfold grel_ve; intro H; isplit.
  apply I_conj_elim1 in H; assumption.
iintro v₁; iintro v₂; iintro Hv.
apply rel_e_fix_roll.
apply I_conj_elim2 in H; iespecialize H; iapply H; assumption.
Qed.

Lemma rel_ve_fix_unroll {τ₁ τ₂ : typ} {T₁ T₂ : typ_eff}
    {k : exp0} {E : cctx} {n : nat} :
  (n ⊨ rel_ve_fix τ₁ T₁ τ₂ T₂ k E) → (n ⊨ rel_ve τ₁ T₁ τ₂ T₂ k E).
Proof.
unfold grel_ve; intro H; isplit.
  apply I_conj_elim1 in H; assumption.
iintro v₁; iintro v₂; iintro Hv.
apply rel_e_fix_unroll.
apply I_conj_elim2 in H; iespecialize H; iapply H; assumption.
Qed.