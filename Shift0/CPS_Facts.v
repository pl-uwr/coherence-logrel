Require Import Utf8.
Require Import Common.Metatheory Shift0.Types.
Require Import Shift0.Source.Typing Shift0.CPS.
Require Import Shift0.Target.Syntax Shift0.Target.Typing.
Require Import Shift0.Target.TypingFacts.

Lemma trans_sub_preserves_types {T U : typ_eff} (D : T ≺: U) : sub S⟦ D ⟧ T U.
Proof.
induction D; simpl; econstructor; eassumption.
Qed.

Local Ltac typing_var :=
  match goal with
  | [ |- ?G ⊩ exp_var ?X ∷ (?T)ε ] =>
    unify (G X) T;
    change (G ⊩ exp_var X ∷ (G X)ε);
    constructor
  end.

Local Ltac typing_inc_map :=
  match goal with
  | [ |- _ ⊩ map (inc_map _) _ ∷ _ ] =>
    eapply typing_map; [ eassumption | ];
    let x := fresh "x" in intro x; destruct x; reflexivity
  end.

Local Hint Resolve trans_sub_preserves_types.
Local Hint Resolve typing_weaken.
Local Hint Resolve typing_weaken1.
Local Hint Resolve trans_sub_preserves_types.
Local Hint Constructors Target.Typing.typing.
Local Hint Constructors Target.Syntax.open_value.
Local Hint Extern 1 => typing_var.
Local Hint Extern 2 => typing_inc_map.

Lemma cps_preserves_types {V : Set}
    {Γ : env V} {e : Source.Syntax.exp V} {T : typ_eff} (D : Γ ⊢ e ∷ T) :
  Γ ⊩ C⟦ D ⟧ ∷ T.
Proof.
induction D; simpl; 
  unfold cps_app; unfold cps_pure_dollar; unfold cps_dollar; unfold cps_succ;
  unfold cps_case; eauto 11.
Qed.
