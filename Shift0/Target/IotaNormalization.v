Require Import Utf8.
Require Import Shift0.Target.Syntax Shift0.Target.Reduction.
Require Import Shift0.Target.ReductionClassify.

Require Arith Omega.

Inductive iota_normalizing (e : exp0) : Prop :=
| IotaN_Normal : (∀ e', ¬iota e e') → iota_normalizing e
| IotaN_Step   : ∀ e', iota e e' → 
    iota_normalizing e' → iota_normalizing e
.

Module Private.

Import Arith Omega.

Reserved Notation "## c" (at level 30).
Reserved Notation "# e"  (at level 30).

Fixpoint crc_iota_measure (c : crc) : nat :=
  match c with
  | crc_id            => 1
  | crc_comp  c₁ c₂   => S (## c₁ + ## c₂)
  | crc_arrow c₁ c₂   => S (## c₁ + ## c₂)
  | crc_lift  c       => S (## c)
  | crc_cons  c c₁ c₂ => S (## c + S (## c₁ + ## c₂))
  end
where "## c" := (crc_iota_measure c).

Fixpoint exp_iota_measure {V : Set} (e : exp V) : nat :=
  match e with
  | exp_var   _      => 0
  | exp_const _      => 0
  | exp_abs   e      => # e
  | exp_app e₁ e₂    => # e₁ + # e₂
  | exp_capp c e     => ## c + # e
  | exp_fix   e      => # e
  | exp_succ  e      => # e
  | exp_case e e₁ e₂ => # e
  end
where "# e" := (@exp_iota_measure _ e).

Lemma iota_measure_0 (e e' : exp0) :
  # e = 0 → ¬iota e e'.
Proof.
intros H₁ H₂; generalize H₂ H₁; clear H₁ H₂; induction 1.
+ inversion H; intro Hm; discriminate Hm.
+ simpl; destruct (#e).
  - rewrite plus_0_r; assumption.
  - rewrite <- plus_n_Sm; intro Hm; discriminate Hm.
+ simpl; destruct (#v); [ assumption | intro Hm; discriminate Hm ].
+ destruct c; simpl; intro Hm; discriminate Hm.
+ assumption.
+ assumption.
Qed.

Lemma iota_measure_S (e e' : exp0) :
  iota e e' → # e = S (# e').
Proof.
induction 1.
+ inversion H; simpl; omega.
+ simpl; rewrite IHctx_cl; omega.
+ simpl; rewrite IHctx_cl; omega.
+ simpl; rewrite IHctx_cl; omega.
+ simpl; rewrite IHctx_cl; omega.
+ simpl; rewrite IHctx_cl; omega.
Qed.

End Private.

Import Private.

Lemma iota_is_normalizing (e : exp0) : iota_normalizing e.
Proof.
assert (Hd : ∃ d, #e = d) by (eexists; reflexivity).
destruct Hd as [ d Hd ].
generalize e Hd; clear e Hd; induction d.
+ intros; constructor 1.
  intro; apply iota_measure_0; assumption.
+ intros e He.
  destruct (iota_dec e) as [ H | H ].
  - destruct H; econstructor 2; [ eassumption | ].
    apply IHd; erewrite iota_measure_S in He; try eassumption.
    injection He; auto.
  - constructor 1; assumption.
Qed.