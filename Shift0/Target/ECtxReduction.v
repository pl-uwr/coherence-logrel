Require Import Utf8.
Require Import Shift0.Target.Syntax Shift0.Target.Reduction.
Require Import Relation_Operators.

Inductive ectx_iota E₁ E₂ : Prop :=
| ECtxIota :
    ectx E₁ → ectx E₂ →
    (∀ v, value v → iota (cplug E₁ v) (cplug E₂ v)) →
    ectx_iota E₁ E₂
.

Definition ectx_iota_rt := clos_refl_trans _ ectx_iota.

Lemma ectx_iota_rt_trans {E₁ E₃ : cctx} E₂ :
  ectx_iota_rt E₁ E₂ → ectx_iota_rt E₂ E₃ → ectx_iota_rt E₁ E₃.
Proof.
intros; econstructor 3; eassumption.
Qed.

Lemma ectx_iota_rt_capp_arrow {E₁ E₂ : cctx} {c₁ c₂ : crc} :
  ectx_iota_rt E₁ E₂ → 
  ectx_iota_rt 
    (cctx_capp (crc_arrow c₁ c₂) E₁)
    (cctx_capp (crc_arrow c₁ c₂) E₂).
Proof.
induction 1.
+ constructor 1; destruct H as [ HE₁ HE₂ Hι ]; constructor.
  - constructor; assumption.
  - constructor; assumption.
  - intros v Hv; apply Hι; constructor; assumption.
+ constructor 2.
+ econstructor 3; eauto.
Qed.

Lemma ectx_iota_rt_capp_lift {E₁ E₂ : cctx} {c : crc} :
  ectx_iota_rt E₁ E₂ → 
  ectx_iota_rt (cctx_capp (crc_lift c) E₁) (cctx_capp (crc_lift c) E₂).
Proof.
induction 1.
+ constructor 1; destruct H as [ HE₁ HE₂ Hι ]; constructor.
  - constructor; assumption.
  - constructor; assumption.
  - intros v Hv; apply Hι; constructor; assumption.
+ constructor 2.
+ econstructor 3; eauto.
Qed.

Lemma ectx_iota_rt_capp_cons {E₁ E₂ : cctx} {c c₁ c₂ : crc} :
  ectx_iota_rt E₁ E₂ → 
  ectx_iota_rt 
    (cctx_capp (crc_cons c c₁ c₂) E₁) 
    (cctx_capp (crc_cons c c₁ c₂) E₂).
Proof.
induction 1.
+ constructor 1; destruct H as [ HE₁ HE₂ Hι ]; constructor.
  - constructor; assumption.
  - constructor; assumption.
  - intros v Hv; apply Hι; constructor; assumption.
+ constructor 2.
+ econstructor 3; eauto.
Qed.