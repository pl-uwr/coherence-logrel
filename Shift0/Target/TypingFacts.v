Require Import Utf8.
Require Import Common.Metatheory Shift0.Types.
Require Import Shift0.Target.Syntax.
Require Import Shift0.Target.SyntaxValues Shift0.Target.SyntaxContext.
Require Import Shift0.Target.Typing.

Lemma wt_value_is_value {v : exp0} {T : typ_eff} :
  ⊩ᵥ v ∷ T → value v.
Proof.
induction 1; constructor; assumption.
Qed.

Lemma wt_ectx_is_ectx {E : cctx} {T T₀ : typ_eff} :
  E ⊩ T ↝ T₀ → ectx E.
Proof.
induction 1; constructor; try assumption.
eapply wt_value_is_value; eassumption.
Qed.

Lemma wt_value_is_wt {v : exp0} {T : typ_eff} :
  ⊩ᵥ v ∷ T → ø ⊩ v ∷ T.
Proof.
induction 1; repeat econstructor; eassumption.
Qed.

Local Hint Constructors typing.

Lemma T_CPlug {E : cctx} {e : exp0} {T T₀ : typ_eff} :
  ø ⊩ e ∷ T → E ⊩ T ↝ T₀ → ø ⊩ cplug E e ∷ T₀.
Proof.
intros He H; generalize e He; clear e He.
induction H; intros e₀ He₀; trivial; apply IHectx_typing; eauto.
+ econstructor 4; [ apply wt_value_is_wt | ]; eassumption.
+ econstructor 6.
  - apply value_is_open_value; eapply wt_value_is_value; eassumption.
  - eassumption.
  - apply wt_value_is_wt; assumption.
Qed.

Local Hint Constructors ectx_typing.

Lemma ET_Append {E₁ E₂ : cctx} {T₁ T₂ T₃ : typ_eff} :
  E₁ ⊩ T₂ ↝ T₃ → E₂ ⊩ T₁ ↝ T₂ → cctx_append E₁ E₂ ⊩ T₁ ↝ T₃.
Proof.
intro HE₁; induction 1; simpl; trivial; eauto.
Qed.

Lemma typing_map {V : Set} {Γ : env V} {e : exp V} {T : typ_eff} : Γ ⊩ e ∷ T →
  ∀ (V' : Set) (f : V → V') (Γ' : env V'),
  (∀ x, Γ x = Γ' (f x)) → Γ' ⊩ map f e ∷ T.
Proof.
induction 1; intros V' f Γ' HΓ; simpl.
+ rewrite HΓ; constructor.
+ constructor.
+ constructor; apply IHtyping; destruct x; simpl; auto.
+ econstructor 4; eauto.
+ constructor; apply IHtyping; destruct x; simpl; auto.
+ econstructor 6; eauto; apply open_value_map; assumption.
+ econstructor; eauto.
+ econstructor; apply IHtyping.
  destruct x as [ | x ]; simpl; trivial.
  destruct x; simpl; auto.
+ constructor; auto.
+ constructor; eauto.
  apply IHtyping3; destruct x; simpl; auto.
Qed.

Lemma typing_weaken {V : Set} {Γ : env V} {e : exp V} {T : typ_eff} {τ : typ} :
  Γ ⊩ e ∷ T → Γ / τ ⊩ weaken e ∷ T.
Proof.
intro H; eapply typing_map; eauto.
Qed.

Lemma typing_weaken1 {V : Set} {Γ : env V} {e : exp (inc V)} 
    {T : typ_eff} {τ₁ τ₂ : typ} :
  Γ / τ₂ ⊩ e ∷ T → Γ / τ₁ / τ₂ ⊩ weaken1 e ∷ T.
Proof.
intro H; eapply typing_map; eauto.
destruct x; eauto.
Qed.

Lemma typing_bind {V : Set} {Γ : env V} {e : exp V} {T : typ_eff} : Γ ⊩ e ∷ T →
  ∀ (V' : Set) (γ : V → exp V') (Γ' : env V'),
  (∀ x, Γ' ⊩ (γ x) ∷ (Γ x)ε) → (∀ x, open_value (γ x)) → Γ' ⊩ bind γ e ∷ T.
Proof.
induction 1; intros V' γ Γ' Hγ Hv; simpl.
+ apply Hγ.
+ constructor.
+ constructor; apply IHtyping; destruct x; simpl.
  - constructor.
  - apply typing_weaken; auto.
  - constructor.
  - apply open_value_map; auto.
+ econstructor 4; auto.
+ constructor; apply IHtyping; destruct x; simpl.
  - constructor.
  - apply typing_weaken; auto.
  - constructor.
  - apply open_value_map; auto.
+ econstructor 6; auto.
  apply open_value_bind; auto.
+ econstructor; eauto.
+ econstructor; apply IHtyping; 
    destruct x as [ | x ]; try destruct x; try constructor.
  - simpl; repeat apply typing_weaken; auto.
  - simpl; repeat apply open_value_map; auto.
+ constructor; auto.
+ constructor; eauto; apply IHtyping3; destruct x; try constructor.
  - simpl; apply typing_weaken; auto.
  - simpl; apply open_value_map; auto.
Qed.