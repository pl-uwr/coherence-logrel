Require Import Utf8.
Require Import Shift0.Target.Syntax Shift0.Target.Reduction.
Require Import Shift0.Target.ClosedTermInduction.

Lemma value_dec (e : exp0) : value e ∨ ¬value e.
Proof.
generalize e; clear e; apply exp_ind_cl.
+ intro m; left; constructor.
+ intro e; left; constructor.
+ intros; right; intro Hv; inversion Hv.
+ intros c e He.
  destruct He as [ He | He ].
  - destruct c; 
      try (left; constructor; assumption; fail);
      right; intro Hv; inversion Hv.
  - right; intro Hv; inversion Hv; auto.
+ intro e; left; constructor.
+ intros; right; intro Hv; inversion Hv.
+ intros; right; intro Hv; inversion Hv.
Qed.

Inductive beta_iota_class (e : exp0) : Prop :=
| BIC_value : value e → beta_iota_class e
| BIC_beta  : ∀ e', 
    (∀ E, ectx E → beta (cplug E e) (cplug E e')) → beta_iota_class e
| BIC_iota  : ∀ e', 
    (∀ E, ectx E → iota (cplug E e) (cplug E e')) → beta_iota_class e
| BIC_stuck :
    ¬value e →
    (∀ e' E, ectx E → ¬beta (cplug E e) e') →
    (∀ e' E, ectx E → ¬iota (cplug E e) e') →
      beta_iota_class e
.

Lemma beta_in_ectx' : ∀ E, ectx E → ∀ e₁ e₂, ¬value e₁ →
  beta (cplug E e₁) e₂ → ∃ e, beta e₁ e ∧ e₂ = cplug E e.
Proof.
induction 1; simpl.
+ intros; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hβ; edestruct IHectx as [ e₀ [ Hβe He₂ ]]; [ | apply Hβ | ].
    intro Hv; inversion Hv.
  inversion Hβe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
  - exfalso; auto.
+ intros e₁ e₂ Hv₁ Hβ; edestruct IHectx as [ e₀ [ Hβe He₂ ]]; [ | apply Hβ | ].
    intro Hv; inversion Hv.
  inversion Hβe.
  - inversion_reduction; exfalso; auto.
  - subst; value_normal.
  - subst; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hβ; edestruct IHectx as [ e₀ [ Hβe He₂ ]]; [ | apply Hβ | ].
    intro Hv; inversion Hv; auto.
  inversion Hβe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hβ; edestruct IHectx as [ e₀ [ Hβe He₂ ]]; [ | apply Hβ | ].
    intro Hv; inversion Hv.
  inversion Hβe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
+ intros f₁ f₂ Hv₁ Hβ; edestruct IHectx as [ f₀ [ Hβe Hf₂ ]]; [ | apply Hβ | ].
    intro Hv; inversion Hv.
  inversion Hβe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
Qed.

Lemma iota_in_ectx' : ∀ E, ectx E → ∀ e₁ e₂, ¬value e₁ →
  iota (cplug E e₁) e₂ → ∃ e, iota e₁ e ∧ e₂ = cplug E e.
Proof.
induction 1; simpl.
+ intros; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hι; edestruct IHectx as [ e₀ [ Hιe He₂ ]]; [ | apply Hι | ].
    intro Hv; inversion Hv.
  inversion Hιe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
  - exfalso; auto.
+ intros e₁ e₂ Hv₁ Hι; edestruct IHectx as [ e₀ [ Hιe He₂ ]]; [ | apply Hι | ].
    intro Hv; inversion Hv.
  inversion Hιe.
  - inversion_reduction; exfalso; auto.
  - subst; value_normal.
  - subst; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hι; edestruct IHectx as [ e₀ [ Hιe He₂ ]]; [ | apply Hι | ].
    intro Hv; inversion Hv; auto.
  inversion Hιe.
  - inversion_reduction; exfalso; apply Hv₁; auto.
  - subst; eexists; split; eauto.
+ intros e₁ e₂ Hv₁ Hι; edestruct IHectx as [ e₀ [ Hιe He₂ ]]; [ | apply Hι | ].
    intro Hv; inversion Hv.
  inversion Hιe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
+ intros f₁ f₂ Hv₁ Hι; edestruct IHectx as [ f₀ [ Hιe Hf₂ ]]; [ | apply Hι | ].
    intro Hv; inversion Hv.
  inversion Hιe.
  - inversion_reduction; exfalso; apply Hv₁; constructor; auto.
  - subst; eexists; split; eauto.
Qed.

Local Ltac bic_stuck :=
  let Hv := fresh "Hv" in
  apply BIC_stuck; [ intro Hv; inversion Hv; auto | | ].

Local Ltac stuck_term :=
  let H := fresh "H" in
  let Hv := fresh "Hv" in
  bic_stuck; intros ? ? ? H;
  [ apply beta_in_ectx' in H;
    [ destruct H as [ ? [ H ? ]]; unfold beta in H;
      repeat inversion_reduction; value_normal
    | assumption
    | intro Hv; inversion Hv
    ]
  | apply iota_in_ectx' in H;
    [ destruct H as [ ? [ H ? ]]; unfold iota in H;
      repeat inversion_reduction; value_normal
    | assumption
    | intro Hv; inversion Hv
    ]
  ].

Lemma beta_iota_classify_app_val (e₁ e₂ : exp0) :
  value e₁ → value e₂ → beta_iota_class (exp_app e₁ e₂).
Proof.
intros Hv₁ Hv₂; inversion Hv₁.
+ stuck_term.
+ eapply BIC_beta.
  intros E HE; apply beta_step_in_ectx; [ assumption | ].
  constructor; trivial.
+ eapply BIC_iota.
  intros E HE; apply iota_step_in_ectx; [ assumption | ].
  constructor; trivial.
+ eapply BIC_beta.
  intros E HE; apply beta_step_in_ectx; [ assumption | ].
  constructor; trivial.
+ eapply BIC_iota.
  intros E HE; apply iota_step_in_ectx; [ assumption | ].
  constructor; trivial.
+ eapply BIC_beta.
  intros E HE; apply beta_step_in_ectx; [ assumption | ].
  constructor; trivial.
Qed.

Lemma beta_iota_classify (e : exp0) : beta_iota_class e.
Proof. 
generalize e; clear e; apply exp_ind_cl.
+ constructor 1; constructor.
+ constructor 1; constructor.
+ intros e₁ He₁ e₂ He₂.
  destruct He₁ as [ Hv₁ | e₁' He₁ | e₁' He₁ | Hs₁ Hβ₁ Hι₁ ].
  - destruct He₂ as [ Hv₂ | e₂' He₂ | e₂' He₂ | Hs₂ Hβ₂ Hι₂ ].
    * apply beta_iota_classify_app_val; assumption.
    * apply BIC_beta with (e' := exp_app e₁ e₂').
      intros E HE; apply He₂ with (E := cctx_app2 e₁ E).
      constructor; assumption.
    * apply BIC_iota with (e' := exp_app e₁ e₂').
      intros E HE; apply He₂ with (E := cctx_app2 e₁ E).
      constructor; assumption.
    * bic_stuck; intros e' E HE.
      apply Hβ₂ with (E := cctx_app2 e₁ E); constructor; assumption.
      apply Hι₂ with (E := cctx_app2 e₁ E); constructor; assumption.
  - apply BIC_beta with (e' := exp_app e₁' e₂).
    intros E HE; apply He₁ with (E := cctx_app1 E e₂).
    constructor; assumption.
  - apply BIC_iota with (e' := exp_app e₁' e₂).
    intros E HE; apply He₁ with (E := cctx_app1 E e₂).
    constructor; assumption.
  - bic_stuck; intros e' E HE.
    apply Hβ₁ with (E := cctx_app1 E e₂); constructor; assumption.
    apply Hι₁ with (E := cctx_app1 E e₂); constructor; assumption.
+ intros c e He.
  destruct He as [ Hv | e' He | e' He | Hs Hβ Hι ].
  - destruct c.
    * eapply BIC_iota.
      intros E HE; apply iota_step_in_ectx; [ assumption | ].
      constructor; assumption.
    * eapply BIC_iota.
      intros E HE; apply iota_step_in_ectx; [ assumption | ].
      constructor; assumption.
    * apply BIC_value; constructor; assumption.
    * apply BIC_value; constructor; assumption.
    * apply BIC_value; constructor; assumption.
  - eapply BIC_beta.
    intros E HE; apply He with (E := cctx_capp c E).
    constructor; assumption.
  - eapply BIC_iota.
    intros E HE; apply He with (E := cctx_capp c E).
    constructor; assumption.
  - bic_stuck; intros e' E HE.
    apply Hβ with (E := cctx_capp c E); constructor; assumption.
    apply Hι with (E := cctx_capp c E); constructor; assumption.
+ constructor 1; constructor.
+ intros e He; destruct He as [ Hv | e' He | e' He | Hs Hβ Hι ].
  - inversion Hv.
    * eapply BIC_beta; intros E HE.
      apply beta_step_in_ectx; [ assumption | constructor ].
    * stuck_term.
    * stuck_term.
    * stuck_term.
    * stuck_term.
    * stuck_term.
  - eapply BIC_beta; intros E HE; apply He with (E := cctx_succ E).
    constructor; assumption.
  - eapply BIC_iota; intros E HE; apply He with (E := cctx_succ E).
    constructor; assumption.
  - bic_stuck; intros e' E HE.
    apply Hβ with (E := cctx_succ E); constructor; assumption.
    apply Hι with (E := cctx_succ E); constructor; assumption.
+ intros e He e₁ He₁ e₂.
  destruct He as [ Hv | e' He | e' He | Hs Hβ Hι ].
  - inversion Hv.
    * destruct m; eapply BIC_beta; intros E HE;
      apply beta_step_in_ectx; trivial; repeat constructor.
    * stuck_term.
    * stuck_term.
    * stuck_term.
    * stuck_term.
    * stuck_term.
  - eapply BIC_beta; intros E HE; apply He with (E := cctx_case E e₁ e₂).
    constructor; assumption.
  - eapply BIC_iota; intros E HE; apply He with (E := cctx_case E e₁ e₂).
    constructor; assumption.
  - bic_stuck; intros e' E HE.
    apply Hβ with (E := cctx_case E e₁ e₂); constructor; assumption.
    apply Hι with (E := cctx_case E e₁ e₂); constructor; assumption.
Qed.

Lemma iota_dec (e : exp0) :
  (∃ e', iota e e') ∨ (∀ e', ¬iota e e').
Proof.
destruct (beta_iota_classify e) as [ Hv | e' Hβ | e' Hι | Hs Hβ Hι ].
+ right; intros ? ?; value_normal.
+ right; intros ? ?; eapply beta_or_iota; try eassumption.
  apply Hβ with (E := cctx_hole); constructor.
+ left; eexists; apply Hι with (E := cctx_hole); constructor.
+ right; intro; apply Hι with (E := cctx_hole); constructor.
Qed.