Require Import Utf8.
Require Import Shift0.Target.Syntax.

Lemma fvalue_is_value {v : exp0} :
  fvalue v → value v.
Proof.
induction 1; constructor; auto.
Qed.

Lemma value_is_open_value {v : exp0} :
  value v → open_value v.
Proof.
induction 1; constructor; auto.
Qed.

Lemma open_value_map {V : Set} {e : exp V} : open_value e →
  ∀ (V' : Set) (f : V → V'), open_value (map f e).
Proof.
induction 1; intros; simpl; constructor; auto.
Qed.

Lemma open_value_bind {V : Set} {e : exp V} : open_value e →
  ∀ (V' : Set) (γ : V → exp V'),
  (∀ x, open_value (γ x)) → open_value (bind γ e).
Proof.
induction 1; intros V' γ Hγ; simpl; try constructor; auto.
Qed.