Require Import Utf8.
Require Import Shift0.Target.Syntax.

Module Private.

Reserved Notation "# e"  (at level 30).

Fixpoint exp_measure {V : Set} (e : exp V) : nat :=
  match e with
  | exp_var   _      => 0
  | exp_const _      => 0
  | exp_abs   _      => 0
  | exp_app e₁ e₂    => S (#e₁ + #e₂)
  | exp_capp c e     => S (#e)
  | exp_fix   _      => 0
  | exp_succ  e      => S (#e)
  | exp_case e e₁ e₂ => S (#e + #e₁)
  end
where "# e" := (@exp_measure _ e).

End Private.
Import Private.

Require Import Arith.
Require Import Omega.

Lemma exp_ind_cl (P : exp0 → Prop) :
  (∀ m, P (exp_const m)) →
  (∀ e, P (exp_abs e)) →
  (∀ e₁, P e₁ → ∀ e₂, P e₂ → P (exp_app e₁ e₂)) →
  (∀ c e, P e → P (exp_capp c e)) →
  (∀ e, P (exp_fix e)) →
  (∀ e, P e → P (exp_succ e)) →
  (∀ e, P e → ∀ e₁, P e₁ → ∀ e₂, P (exp_case e e₁ e₂)) →
  ∀ e, P e.
Proof.
intros Hconst Habs Happ Hcapp Hfix Hsucc Hcase.
intro e; assert (Hd : ∃ d, #e = d) by (eexists; reflexivity).
destruct Hd as [ d Hd ].
generalize Hd; generalize e; generalize d; clear e d Hd.
apply (well_founded_ind lt_wf (λ d, ∀ e : exp0, #e = d → P e)).
intros d IHd e; dependent inversion e; intro He; simpl in He.
+ destruct e0.
+ apply Hconst.
+ apply Habs.
+ apply Happ; eapply IHd; try reflexivity; omega.
+ apply Hcapp; eapply IHd; try reflexivity; omega.
+ apply Hfix.
+ apply Hsucc; eapply IHd; try reflexivity; omega.
+ apply Hcase; eapply IHd; try reflexivity; omega.
Qed.