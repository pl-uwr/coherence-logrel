Require Import Utf8.
Require Import Shift0.Target.Syntax.

Fixpoint cctx_append (E₁ E₂ : cctx) : cctx :=
  match E₂ with
  | cctx_hole          => E₁
  | cctx_app1 E₂ e     => cctx_app1 (cctx_append E₁ E₂) e
  | cctx_app2 v E₂     => cctx_app2 v (cctx_append E₁ E₂)
  | cctx_capp c E₂     => cctx_capp c (cctx_append E₁ E₂)
  | cctx_succ E₂       => cctx_succ (cctx_append E₁ E₂)
  | cctx_case E₂ e₁ e₂ => cctx_case (cctx_append E₁ E₂) e₁ e₂
  end.

Lemma cplug_cctx_append (E₁ E₂ : cctx) : ∀ e,
  cplug (cctx_append E₁ E₂) e = cplug E₁ (cplug E₂ e).
Proof.
induction E₂; intro e₀; simpl; congruence.
Qed.

Lemma ectx_append (E₁ E₂ : cctx) :
  ectx E₁ → ectx E₂ → ectx (cctx_append E₁ E₂).
Proof.
intro HE₁; induction 1; simpl; trivial; constructor; assumption.
Qed.