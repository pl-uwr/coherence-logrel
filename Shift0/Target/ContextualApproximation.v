Require Import Utf8.
Require Import Shift0.Types.
Require Import Shift0.Target.Syntax Shift0.Target.Typing.
Require Import Shift0.Target.Reduction.

Definition obs (e₁ e₂ : exp0) : Prop :=
  stops e₁ → stops e₂.
Notation "A ~ B" := (obs A B) (at level 70).

Definition ctx_approx {V : Set} 
    (Γ : env V) (e₁ e₂ : exp V) (T : typ_eff) :=
  ∀ (C : ctx V) ρ, C ∷[ Γ ⊢ T ]↝ (ρ)ε → plug C e₁ ~ plug C e₂.