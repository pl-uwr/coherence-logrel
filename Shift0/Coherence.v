Require Import Utf8.
Require Import IxFree.Lib.
Require Import Shift0.Types.
Require Import Shift0.Source.Typing Shift0.Source.TypingInduction.
Require Import Shift0.CPS Shift0.CPS_Facts.
Require Import Shift0.Target.Syntax Shift0.Target.ContextualApproximation.
Require Import Shift0.LogicalRelation.Main.
Require Import Shift0.LogicalRelation.Soundness.
Require Import Shift0.LogicalRelation.CPS_Compatibility.

Local Hint Resolve trans_sub_preserves_types.

Lemma coherence_lemma {V : Set} {Γ₁ Γ₂ : env V} 
      {e : Source.Syntax.exp V} {T₁ T₂ : typ_eff} :
  ∀ (D₁ : Γ₁ ⊢ e ∷ T₁) (D₂ : Γ₂ ⊢ e ∷ T₂),
    ⊨ rel_e_open Γ₁ Γ₂ C⟦ D₁ ⟧ C⟦ D₂ ⟧ T₁ T₂.
Proof.
intro D₁; generalize Γ₂ T₂; clear Γ₂ T₂.
induction D₁; intros Γ₂ D₂ T₂ n; simpl.
+ apply typing_var_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - apply compat_var.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_const_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - apply compat_const.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_abs_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; apply compat_abs; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_app_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_app; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply compat_cps_app_r; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_app_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_app_l; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply compat_cps_app; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_shift0_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_kabs; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_reset0_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_reset0; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_dollar_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_pure_dollar; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply compat_cps_dollar_r; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_dollar_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_dollar_l; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply compat_cps_dollar; [ apply IHD₁1 | apply IHD₁2 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_fix_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_fix; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_succ_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_succ; apply IHD₁.
  - intros; eapply compat_cps_succ_r; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_succ_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_succ_l; apply IHD₁.
  - intros; eapply compat_cps_succ; apply IHD₁.
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_case_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_case; [ apply IHD₁1 | apply IHD₁2 | apply IHD₁3 ].
  - intros; eapply compat_cps_case_r; 
      [ apply IHD₁1 | apply IHD₁2 | apply IHD₁3 ].
  - intros; eapply rel_coercion_r; eauto.
+ apply typing_case_ind with
    (P := λ _ T D, n ⊨ rel_e_open _ _ _ C⟦ D ⟧ _ T); simpl.
  - intros; eapply compat_cps_case_l;
      [ apply IHD₁1 | apply IHD₁2 | apply IHD₁3 ].
  - intros; eapply compat_cps_case;
      [ apply IHD₁1 | apply IHD₁2 | apply IHD₁3 ].
  - intros; eapply rel_coercion_r; eauto.
+ eapply rel_coercion_l; eauto.
  apply IHD₁.
Qed.

Theorem coherence {V : Set} (Γ : env V) e T :
  ∀ (D₁ D₂ : Γ ⊢ e ∷ T), ctx_approx Γ C⟦ D₁ ⟧ C⟦ D₂ ⟧ T.
Proof.
intros D₁ D₂; apply relation_soundness; apply coherence_lemma.
Qed.